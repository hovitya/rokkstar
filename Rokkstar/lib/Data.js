﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./Base"], function(require, exports, __core__) {
    ///<reference path='Base.ts'/>
    var core = __core__;
    var FieldBase = (function (_super) {
        __extends(FieldBase, _super);
        function FieldBase() {
            _super.call(this);
            this._superGlobal = "";
            this.isStorageChange = false;
            this._defaultValue = undefined;
            var that = this;
            this.storageEventHandler = function (event) {
                if (that.superGlobal !== "" && that.superGlobal === event.key) {
                    that.isStorageChange = true;
                    that.setSerializedValue(event.newValue);
                    that.isStorageChange = false;
                }
            };
        }
        Object.defineProperty(FieldBase.prototype, "defaultValue", {
            get: function () {
                return this._defaultValue;
            },
            set: function (val) {
                if (this._defaultValue == val)
                    return;
                var old = this._defaultValue;
                this._defaultValue = val;
                if (this.superGlobal !== "") {
                    var storedValue = window.localStorage.getItem(this.superGlobal);
                    if (storedValue === null) {
                        this.setSerializedValue(val);
                    }
                }
                this.notifyPropertyChanged("defaultValue", old, val);
            },
            enumerable: true,
            configurable: true
        });



        Object.defineProperty(FieldBase.prototype, "superGlobal", {
            get: function () {
                return this._superGlobal;
            },
            set: function (value) {
                if (value == this._superGlobal)
                    return;
                var old = this._superGlobal;
                this._superGlobal = value;
                window.removeEventListener("storage", this.storageEventHandler);
                if (value !== "") {
                    window.addEventListener("storage", this.storageEventHandler);
                    var storedValue = window.localStorage.getItem(value);
                    if (storedValue !== null) {
                        this.isStorageChange = true;
                        this.setSerializedValue(storedValue);
                        this.isStorageChange = false;
                    } else if (this.defaultValue !== undefined) {
                        this.setSerializedValue(this.defaultValue);
                    }
                }
                this.notifyPropertyChanged("superGlobal", old, value);
            },
            enumerable: true,
            configurable: true
        });

        FieldBase.prototype.storeValue = function () {
            if (this.superGlobal !== "" && !this.isStorageChange) {
                window.localStorage.setItem(this.superGlobal, this.getSerializedValue());
            }
        };

        FieldBase.prototype.getSerializedValue = function () {
            return this['value'];
        };
        FieldBase.prototype.setSerializedValue = function (val) {
            this['value'] = val;
        };
        return FieldBase;
    })(core.PropertyChangeNotifier);
    exports.FieldBase = FieldBase;

    var BooleanField = (function (_super) {
        __extends(BooleanField, _super);
        function BooleanField() {
            _super.apply(this, arguments);
        }
        Object.defineProperty(BooleanField.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureBool(val);
                if (val === this._value)
                    return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            },
            enumerable: true,
            configurable: true
        });
        return BooleanField;
    })(FieldBase);
    exports.BooleanField = BooleanField;

    var IntegerField = (function (_super) {
        __extends(IntegerField, _super);
        function IntegerField() {
            _super.apply(this, arguments);
        }
        Object.defineProperty(IntegerField.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureInteger(val);
                if (val === this._value)
                    return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            },
            enumerable: true,
            configurable: true
        });
        return IntegerField;
    })(FieldBase);
    exports.IntegerField = IntegerField;

    var FloatField = (function (_super) {
        __extends(FloatField, _super);
        function FloatField() {
            _super.apply(this, arguments);
        }
        Object.defineProperty(FloatField.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureFloat(val);
                if (val === this._value)
                    return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            },
            enumerable: true,
            configurable: true
        });
        return FloatField;
    })(FieldBase);
    exports.FloatField = FloatField;

    var StringField = (function (_super) {
        __extends(StringField, _super);
        function StringField() {
            _super.apply(this, arguments);
        }
        Object.defineProperty(StringField.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (val) {
                if (val == this._value)
                    return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            },
            enumerable: true,
            configurable: true
        });
        return StringField;
    })(FieldBase);
    exports.StringField = StringField;
});
//# sourceMappingURL=Data.js.map
