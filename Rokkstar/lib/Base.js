define(["require", "exports"], function(require, exports) {
    var IIPropertyObserver = (function () {
        function IIPropertyObserver() {
            this.className = "IIPropertyObserver";
            this.methodNames = ["update"];
            this.inheritsFrom = [];
        }
        return IIPropertyObserver;
    })();
    exports.IIPropertyObserver = IIPropertyObserver;

    var DevConsole = (function () {
        function DevConsole() {
        }
        DevConsole.warn = function (message) {
            if (!window['_rokkSilentMode'])
                console.warn(message);
        };
        DevConsole.log = function (message) {
            if (!window['_rokkSilentMode'])
                console.log(message);
        };
        return DevConsole;
    })();
    exports.DevConsole = DevConsole;

    var DataBindingManager = (function () {
        function DataBindingManager() {
            this.bindings = [];
        }
        DataBindingManager.GetInstance = function () {
            if (DataBindingManager._instance === undefined)
                DataBindingManager._instance = new DataBindingManager();
            return DataBindingManager._instance;
        };

        DataBindingManager.prototype.bind = function (source, property, target, targetProperty, filter, context) {
            if (typeof filter === "undefined") { filter = []; }
            if (typeof context === "undefined") { context = {}; }
            var parsedFilters = [];
            for (var i in filter) {
                var matches = filter[i].match(/^([^(]+)\(([^)]+)+\)$/);
                var func = ReflectionUtil.ObjectFactory(matches[1], context);
                var params = matches[2].split(',');
                parsedFilters.push({ f: func, p: params });
            }
            if (InterfaceChecker.implementsInterface(source, new InterfaceChecker(new IIPropertyChangeNotifier()))) {
                if (source.objectId === undefined) {
                    this.bindings.push({});
                    source.objectId = this.bindings.length - 1;
                }
                if (this.bindings[source.objectId][property] === undefined)
                    this.bindings[source.objectId][property] = [];

                DevConsole.log(parsedFilters);
                this.bindings[source.objectId][property].push({ t: target, tp: targetProperty, f: parsedFilters });
                source.attach(this);
            } else {
                DevConsole.warn("The following object is not an IPropertyChangeNotifier, so property binding won't work:");
                DevConsole.log(source);
            }
            target[targetProperty] = this.applyFilters(source[property], parsedFilters);
        };

        DataBindingManager.prototype.update = function (observable, property, oldValue, newValue) {
            if (observable.objectId === undefined || this.bindings[observable.objectId] === undefined || this.bindings[observable.objectId][property] === undefined)
                return;
            for (var i in this.bindings[observable.objectId][property]) {
                this.bindings[observable.objectId][property][i].t[this.bindings[observable.objectId][property][i].tp] = this.applyFilters(newValue, this.bindings[observable.objectId][property][i].f);
            }
        };

        DataBindingManager.prototype.applyFilters = function (value, filters) {
            for (var i in filters) {
                value = filters[i].f.apply({}, [value].concat(filters[i].p));
            }
            return value;
        };
        return DataBindingManager;
    })();
    exports.DataBindingManager = DataBindingManager;

    var MetaPropertyTypes = (function () {
        function MetaPropertyTypes() {
        }
        MetaPropertyTypes.INTEGER = "integer";
        MetaPropertyTypes.FLOAT = "float";
        MetaPropertyTypes.STRING = "string";
        MetaPropertyTypes.ARRAY = "array";
        return MetaPropertyTypes;
    })();
    exports.MetaPropertyTypes = MetaPropertyTypes;

    var MetaProperty = (function () {
        function MetaProperty(type, name, listType) {
            this.type = type;
            this.name = name;
            if (listType !== null && listType !== undefined) {
                this.listType = listType;
            }
        }
        return MetaProperty;
    })();
    exports.MetaProperty = MetaProperty;

    var PropertyChangeNotifier = (function () {
        function PropertyChangeNotifier() {
            this.observers = [];
        }
        PropertyChangeNotifier.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        PropertyChangeNotifier.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        PropertyChangeNotifier.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };
        return PropertyChangeNotifier;
    })();
    exports.PropertyChangeNotifier = PropertyChangeNotifier;

    var IIPropertyChangeNotifier = (function () {
        function IIPropertyChangeNotifier() {
            this.className = "IIPropertyChangeNotifier";
            this.methodNames = ["attach", "detach", "notifyPropertyChanged"];
            this.inheritsFrom = [];
        }
        return IIPropertyChangeNotifier;
    })();
    exports.IIPropertyChangeNotifier = IIPropertyChangeNotifier;

    var ReflectionUtil = (function () {
        function ReflectionUtil() {
        }
        ReflectionUtil.ClassFactoryFromString = function (className, currentScope, parameter) {
            if (typeof parameter === "undefined") { parameter = undefined; }
            var currentObject = ReflectionUtil.ObjectFactory(className, currentScope);
            return new currentObject(parameter);
        };

        ReflectionUtil.ObjectFactory = function (className, currentScope) {
            if (currentScope === null || currentScope === undefined) {
                currentScope = window;
            }
            var parts = className.split(".");
            var currentObject = currentScope;
            for (var i in parts) {
                if (currentObject[parts[i]] === undefined || currentObject[parts[i]] === undefined)
                    throw new Error("Class reflection error: " + className + " not found.");
                currentObject = currentObject[parts[i]];
            }
            return currentObject;
        };

        ReflectionUtil.ClassExists = function (className, currentScope) {
            if (currentScope === null || currentScope === undefined) {
                currentScope = window;
            }
            var parts = className.split(".");
            var currentObject = currentScope;
            for (var i in parts) {
                if (currentObject[parts[i]] === undefined || currentObject[parts[i]] === undefined)
                    return false;
                currentObject = currentObject[parts[i]];
            }
            return true;
        };
        return ReflectionUtil;
    })();
    exports.ReflectionUtil = ReflectionUtil;

    var InterfaceChecker = (function () {
        function InterfaceChecker(object) {
            this.name = object.className;
            this.methods = [];
            var i, len;
            for (i = 0, len = object.methodNames.length; i < len; i++) {
                this.methods.push(object.methodNames[i]);
            }
            ;
        }
        InterfaceChecker.ensureImplements = function (object, targetInterface) {
            var i, len;
            for (i = 0, len = targetInterface.methods.length; i < len; i++) {
                var method = targetInterface.methods[i];
                if (!object[method] || typeof object[method] !== 'function') {
                    throw new Error("Function InterfaceChecker.ensureImplements: ' + ' object does not implement the " + targetInterface.name + " interface. Method " + method + " was not found");
                }
            }
        };
        InterfaceChecker.implementsInterface = function (object, targetInterface) {
            var i, len;
            for (i = 0, len = targetInterface.methods.length; i < len; i++) {
                var method = targetInterface.methods[i];
                if (!object[method] || typeof object[method] !== 'function') {
                    return false;
                }
            }
            return true;
        };
        return InterfaceChecker;
    })();
    exports.InterfaceChecker = InterfaceChecker;

    var PrimitiveTypeTools = (function () {
        function PrimitiveTypeTools() {
        }
        PrimitiveTypeTools.ensureInteger = function (value) {
            var ensured = parseInt(value);
            if (ensured === NaN) {
                throw new Error("Given value is not an integer.");
            }
            return ensured;
        };

        PrimitiveTypeTools.ensureFloat = function (value) {
            var ensured = parseFloat(value);
            if (ensured === NaN) {
                throw new Error("Given value is not a float.");
            }
            return ensured;
        };

        PrimitiveTypeTools.ensureBool = function (value) {
            if (value === "true" || value === true || value == 1) {
                return true;
            } else if (value === "false" || value === false || value == 0) {
                return false;
            }
            throw new Error("Given value is not a boolean.");
        };
        return PrimitiveTypeTools;
    })();
    exports.PrimitiveTypeTools = PrimitiveTypeTools;

    var Point = (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        Point.prototype.toJSON = function () {
            return { x: this.x, y: this.y };
        };

        Point.fromJSON = function (json) {
            return new Point(json.x, json.y);
        };
        return Point;
    })();
    exports.Point = Point;

    var MathTools = (function () {
        function MathTools() {
        }
        MathTools.toDegree = function (radians) {
            var pi = Math.PI;
            var deg = (radians) * (180 / pi);
            return Number(deg);
        };

        MathTools.toRadians = function (degree) {
            var pi = Math.PI;
            var rad = degree * (pi / 180);
            return Number(rad);
        };
        return MathTools;
    })();
    exports.MathTools = MathTools;
});
//# sourceMappingURL=Base.js.map
