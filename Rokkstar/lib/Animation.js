﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./Base", "./Events"], function(require, exports, __core__, __events__) {
    ///<reference path='Base.ts'/>
    ///<reference path='Events.ts'/>
    ///<reference path='ComponentBase.ts'/>
    ///<reference path='../third-party/definitions/greensock.d.ts'/>
    var core = __core__;
    
    var events = __events__;

    var AnimationAbstract = (function (_super) {
        __extends(AnimationAbstract, _super);
        function AnimationAbstract() {
            _super.apply(this, arguments);
            this.observers = [];
            this._position = "+=0";
        }
        AnimationAbstract.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        AnimationAbstract.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        AnimationAbstract.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };


        Object.defineProperty(AnimationAbstract.prototype, "position", {
            get: function () {
                return this._position;
            },
            set: function (val) {
                if (this._position === val)
                    return;
                var old = this._position;
                this._position = val;
                this.notifyPropertyChanged("position", old, val);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(AnimationAbstract.prototype, "delay", {
            get: function () {
                return this.toAnimation().delay();
            },
            set: function (val) {
                this.toAnimation().delay(core.PrimitiveTypeTools.ensureFloat(val));
            },
            enumerable: true,
            configurable: true
        });

        AnimationAbstract.prototype.toGSAP = function () {
            return [];
        };

        AnimationAbstract.prototype.toAnimation = function () {
            return this.toGSAP();
        };


        Object.defineProperty(AnimationAbstract.prototype, "duration", {
            get: function () {
                return this.toAnimation().duration();
            },
            set: function (val) {
                this.toAnimation().duration(core.PrimitiveTypeTools.ensureFloat(val));
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(AnimationAbstract.prototype, "yoyo", {
            get: function () {
                return this.toGSAP().yoyo();
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureBool(val);
                this.toGSAP().yoyo(val);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(AnimationAbstract.prototype, "timeScale", {
            get: function () {
                return this.toGSAP().timeScale();
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureFloat(val);
                this.toGSAP().timeScale(val);
            },
            enumerable: true,
            configurable: true
        });

        AnimationAbstract.prototype.play = function (from) {
            if (from === undefined) {
                this.toAnimation().play();
            } else {
                this.toAnimation().play(from);
            }
        };

        AnimationAbstract.prototype.seek = function (time) {
            this.toAnimation().seek(time);
        };

        AnimationAbstract.prototype.reverse = function (from) {
            if (from === undefined) {
                this.toAnimation().reverse();
            } else {
                this.toAnimation().reverse(from);
            }
        };

        AnimationAbstract.prototype.resume = function (from) {
            if (from === undefined) {
                this.toAnimation().resume();
            } else {
                this.toAnimation().resume(from);
            }
        };

        AnimationAbstract.prototype.pause = function (atTime) {
            if (atTime === undefined) {
                this.toAnimation().pause();
            } else {
                this.toAnimation().pause(atTime);
            }
        };


        Object.defineProperty(AnimationAbstract.prototype, "repeat", {
            get: function () {
                return this.toGSAP().repeat();
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                this.toGSAP().repeat(value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(AnimationAbstract.prototype, "repeatDelay", {
            get: function () {
                return this.toGSAP().repeatDelay();
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                this.toGSAP().repeatDelay(value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(AnimationAbstract.prototype, "reversed", {
            get: function () {
                return this.toAnimation().reversed();
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureBool(val);
                this.toAnimation().reversed(val);
            },
            enumerable: true,
            configurable: true
        });
        return AnimationAbstract;
    })(events.EventDispatcher);
    exports.AnimationAbstract = AnimationAbstract;

    var Tween = (function (_super) {
        __extends(Tween, _super);
        function Tween() {
            _super.call(this);
            this.tween = new TweenMax({}, 1, { paused: true, ease: "Linear.easeNone" });
            this._easing = "Linear.easeNone";
        }

        Object.defineProperty(Tween.prototype, "target", {
            get: function () {
                return this._target;
            },
            set: function (val) {
                if (val === this._target)
                    return;
                var old = this._target;
                this._target = val;
                var oldTween = this.tween;
                this.tween = new TweenMax(val, oldTween.duration(), { paused: true, ease: this.easing });
                this.tween.delay(oldTween.delay());
                this.tween.yoyo(oldTween.yoyo());
                this.tween.timeScale(oldTween.timeScale());
                this.tween.updateTo(oldTween.vars);
                this.tween.repeat(oldTween.repeat());
                this.tween.repeatDelay(oldTween.repeatDelay());
                this.tween.reversed(oldTween.reversed());
                this.notifyPropertyChanged("target", old, val);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Tween.prototype, "easing", {
            get: function () {
                return this._easing;
            },
            set: function (val) {
                if (val === this._easing)
                    return;
                var old = this._easing;
                this._easing = val;

                /*var oldTween = this.tween;
                this.tween = new TweenMax(val, oldTween.duration(), { paused: true, ease: val });
                this.tween.delay(oldTween.delay());
                this.tween.yoyo(oldTween.yoyo());
                this.tween.timeScale(oldTween.timeScale());
                var vars = oldTween.vars =
                this.tween.updateTo(oldTween.vars);
                this.tween.repeat(oldTween.repeat());
                this.tween.repeatDelay(oldTween.repeatDelay());
                this.tween.reversed(oldTween.reversed());*/
                this.tween.updateTo({ ease: val });
                this.notifyPropertyChanged("easing", old, val);
            },
            enumerable: true,
            configurable: true
        });


        Tween.prototype.toGSAP = function () {
            return this.tween;
        };


        Object.defineProperty(Tween.prototype, "vars", {
            get: function () {
                return this.tween.vars;
            },
            set: function (val) {
                if (typeof val === "string") {
                    val = (val).replace(new RegExp("'", 'g'), '"');
                    val = JSON.parse(val);
                }
                this.tween.updateTo(val);
            },
            enumerable: true,
            configurable: true
        });
        return Tween;
    })(AnimationAbstract);
    exports.Tween = Tween;

    var Timeline = (function (_super) {
        __extends(Timeline, _super);
        function Timeline() {
            _super.call(this);
            this._tweens = [];
            if (this.timeline === undefined) {
                this.timeline = new TimelineMax({ paused: true });
            }
        }

        Object.defineProperty(Timeline.prototype, "tweens", {
            get: function () {
                return this._tweens;
            },
            set: function (val) {
                for (var i in this._tweens) {
                    this._tweens[i].detach(this);
                }
                for (var i in val) {
                    val[i].attach(this);
                }
                this._tweens = val;
                this.refresh();
            },
            enumerable: true,
            configurable: true
        });

        Timeline.prototype.update = function (item, property, newValue, oldValue) {
            //if (property === "target" || property === "position") {
            this.refresh();
            //}
        };

        Timeline.prototype.refresh = function () {
            this.timeline.clear();
            for (var i in this.tweens) {
                if (this.tweens[i] instanceof Tween) {
                    var tween = this.tweens[i];
                    if (tween.target) {
                        var item = tween.toGSAP();
                        item.paused(false);
                        this.timeline.add(item, tween.position);
                    }
                } else {
                    this.timeline.add(this.tweens[i].toGSAP(), this.tweens[i].position);
                }
            }
        };

        Timeline.prototype.toGSAP = function () {
            return this.timeline;
        };
        return Timeline;
    })(AnimationAbstract);
    exports.Timeline = Timeline;

    var Parallel = (function () {
        function Parallel() {
            this.observers = [];
            this._position = "+=0";
            this._tweens = [];
        }
        Parallel.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        Parallel.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        Parallel.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };


        Object.defineProperty(Parallel.prototype, "position", {
            get: function () {
                return this._position;
            },
            set: function (val) {
                if (this._position === val)
                    return;
                var old = this._position;
                this._position = val;
                this.notifyPropertyChanged("position", old, val);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Parallel.prototype, "tweens", {
            get: function () {
                return this._tweens;
            },
            set: function (val) {
                var old = this.tweens;
                for (var i in this._tweens) {
                    this._tweens[i].detach(this);
                }
                for (var i in val) {
                    val[i].attach(this);
                }
                this._tweens = val;
                this.notifyPropertyChanged("tweens", old, this._tweens);
            },
            enumerable: true,
            configurable: true
        });

        Parallel.prototype.update = function (item, property, newValue, oldValue) {
            //if (property === "target" || property === "position") {
            this.notifyPropertyChanged("tweensInner", 0, 1);
            //}
        };

        Parallel.prototype.toGSAP = function () {
            var tweens = [];
            for (var i in this.tweens) {
                var item = this.tweens[i].toGSAP();
                if (this.tweens[i] instanceof Tween) {
                    item.paused(false);
                }
                tweens.push(item);
            }
            return tweens;
        };
        return Parallel;
    })();
    exports.Parallel = Parallel;
});
//# sourceMappingURL=Animation.js.map
