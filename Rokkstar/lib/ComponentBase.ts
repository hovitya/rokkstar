///<reference path='Events.ts'/>
///<reference path='Collections.ts'/>
///<reference path='../third-party/definitions/modernizr.d.ts'/>
import core = require("./Base");
import events = require("./Events");
import collections = require("./Collections");
    export module globals {
        export var DOMEvents: String[] = ['mouseover', 'mouseout', 'mousemove', 'blur', 'mouseup', 'mousedown', 'touchend', 'touchstart', 'keyup', 'keydown', 'focus', 'onload', 'click'];
        export var namedColors: Object = {
            'transparent': 'rgba(0, 0, 0, 0)', 'aliceblue': '#F0F8FF', 'antiquewhite': '#FAEBD7', 'aqua': '#00FFFF', 'aquamarine': '#7FFFD4',
            'azure': '#F0FFFF', 'beige': '#F5F5DC', 'bisque': '#FFE4C4', 'black': '#000000', 'blanchedalmond': '#FFEBCD', 'blue': '#0000FF', 'blueviolet': '#8A2BE2',
            'brown': '#A52A2A', 'burlywood': '#DEB887', 'cadetblue': '#5F9EA0', 'chartreuse': '#7FFF00', 'chocolate': '#D2691E', 'coral': '#FF7F50',
            'cornflowerblue': '#6495ED', 'cornsilk': '#FFF8DC', 'crimson': '#DC143C', 'cyan': '#00FFFF', 'darkblue': '#00008B', 'darkcyan': '#008B8B', 'darkgoldenrod': '#B8860B',
            'darkgray': '#A9A9A9', 'darkgrey': '#A9A9A9', 'darkgreen': '#006400', 'darkkhaki': '#BDB76B', 'darkmagenta': '#8B008B', 'darkolivegreen': '#556B2F',
            'darkorange': '#FF8C00', 'darkorchid': '#9932CC', 'darkred': '#8B0000', 'darksalmon': '#E9967A', 'darkseagreen': '#8FBC8F', 'darkslateblue': '#483D8B',
            'darkslategray': '#2F4F4F', 'darkslategrey': '#2F4F4F', 'darkturquoise': '#00CED1', 'darkviolet': '#9400D3', 'deeppink': '#FF1493', 'deepskyblue': '#00BFFF',
            'dimgray': '#696969', 'dimgrey': '#696969', 'dodgerblue': '#1E90FF', 'firebrick': '#B22222', 'floralwhite': '#FFFAF0', 'forestgreen': '#228B22',
            'fuchsia': '#FF00FF', 'gainsboro': '#DCDCDC', 'ghostwhite': '#F8F8FF', 'gold': '#FFD700', 'goldenrod': '#DAA520', 'gray': '#808080', 'grey': '#808080',
            'green': '#008000', 'greenyellow': '#ADFF2F', 'honeydew': '#F0FFF0', 'hotpink': '#FF69B4', 'indianred': '#CD5C5C', 'indigo': '#4B0082', 'ivory': '#FFFFF0',
            'khaki': '#F0E68C', 'lavender': '#E6E6FA', 'lavenderblush': '#FFF0F5', 'lawngreen': '#7CFC00', 'lemonchiffon': '#FFFACD', 'lightblue': '#ADD8E6',
            'lightcoral': '#F08080', 'lightcyan': '#E0FFFF', 'lightgoldenrodyellow': '#FAFAD2', 'lightgray': '#D3D3D3', 'lightgrey': '#D3D3D3', 'lightgreen': '#90EE90',
            'lightpink': '#FFB6C1', 'lightsalmon': '#FFA07A', 'lightseagreen': '#20B2AA', 'lightskyblue': '#87CEFA', 'lightslategray': '#778899',
            'lightslategrey': '#778899', 'lightsteelblue': '#B0C4DE', 'lightyellow': '#FFFFE0', 'lime': '#00FF00', 'limegreen': '#32CD32', 'linen': '#FAF0E6',
            'magenta': '#FF00FF', 'maroon': '#800000', 'mediumaquamarine': '#66CDAA', 'mediumblue': '#0000CD', 'mediumorchid': '#BA55D3', 'mediumpurple': '#9370D8',
            'mediumseagreen': '#3CB371', 'mediumslateblue': '#7B68EE', 'mediumspringgreen': '#00FA9A', 'mediumturquoise': '#48D1CC', 'mediumvioletred': '#C71585',
            'midnightblue': '#191970', 'mintcream': '#F5FFFA', 'mistyrose': '#FFE4E1', 'moccasin': '#FFE4B5', 'navajowhite': '#FFDEAD', 'navy': '#000080', 'oldlace': '#FDF5E6',
            'olive': '#808000', 'olivedrab': '#6B8E23', 'orange': '#FFA500', 'orangered': '#FF4500', 'orchid': '#DA70D6', 'palegoldenrod': '#EEE8AA',
            'palegreen': '#98FB98', 'paleturquoise': '#AFEEEE', 'palevioletred': '#D87093', 'papayawhip': '#FFEFD5', 'peachpuff': '#FFDAB9', 'peru': '#CD853F',
            'pink': '#FFC0CB', 'plum': '#DDA0DD', 'powderblue': '#B0E0E6', 'purple': '#800080', 'red': '#FF0000', 'rosybrown': '#BC8F8F', 'royalblue': '#4169E1',
            'saddlebrown': '#8B4513', 'salmon': '#FA8072', 'sandybrown': '#F4A460', 'seagreen': '#2E8B57', 'seashell': '#FFF5EE', 'sienna': '#A0522D', 'silver': '#C0C0C0',
            'skyblue': '#87CEEB', 'slateblue': '#6A5ACD', 'slategray': '#708090', 'slategrey': '#708090', 'snow': '#FFFAFA', 'springgreen': '#00FF7F',
            'steelblue': '#4682B4', 'tan': '#D2B48C', 'teal': '#008080', 'thistle': '#D8BFD8', 'tomato': '#FF6347', 'turquoise': '#40E0D0', 'violet': '#EE82EE'
        };
    }

    export interface IBackgroundBrush extends events.IEventDispatcher {
        toBackgroundCSS(): string;
        setFillColor(ctx: CanvasRenderingContext2D, x0: number, y0: number, x1: number, y1: number): void;
        toStringValue(): string;
    }

    export class BackgroundBrushEvent extends events.RokkEvent {
        static UPDATED = "updated";
    }

    export class IIBackgroundBrush implements core.IInterfaceChecker {
        className: string = "IIBackgroundBrush";
        methodNames: string[] = ["toBackgroundCSS"];
        inheritsFrom: core.IInterfaceChecker[] = [];
    }

    export interface IBorderBrush {
        toBorderCSS(): string;
    }

    export class IIBorderBrush implements core.IInterfaceChecker {
        className: string = "IIBoderBrush";
        methodNames: string[] = ["toBorderCSS"];
        inheritsFrom: core.IInterfaceChecker[] = [];
    }

    export class ColorEvent extends events.RokkEvent {
        static RGB_UPDATED: string = 'RGBUpdated';
        static HSL_UPDATED: string = 'HSLUpdated';
        static HSV_UPDATED: string = 'HSVUpdated';
        static HEX_UPDATED: string = 'HexUpdated';
        static INT_UPDATED: string = 'IntUpdated';
        static UPDATED: string = 'updated';
    }

    //#region Color

    /*
     * Ported from https://github.com/moagrius/Color-1
     * License Unknown
     */
    export class Color extends events.EventDispatcher implements core.IPropertyChangeNotifier, IBackgroundBrush {
        private propertyChangeNotifier: core.PropertyChangeNotifier = new core.PropertyChangeNotifier();
        public objectId:number;
        attach(observer: core.IPropertyObserver) {
            this.propertyChangeNotifier.attach(observer);
        }

        detach(observer: core.IPropertyObserver) {
            this.propertyChangeNotifier.detach(observer);
        }

        notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
            this.propertyChangeNotifier.notifyPropertyChanged(property, oldValue, newValue);
        }

        toBackgroundCSS(): string {
            return this.RGBA;
        }

        private isHex: RegExp = /^#?([0-9a-f]{3}|[0-9a-f]{6})$/i;
        private isHSL: RegExp = /^hsla?\((\d{1,3}?),\s*(\d{1,3}%),\s*(\d{1,3}%)(,\s*[01]?\.?\d*)?\)$/;
        private isRGB: RegExp = /^rgba?\((\d{1,3}%?),\s*(\d{1,3}%?),\s*(\d{1,3}%?)(,\s*[01]?\.?\d*)?\)$/;
        private isPercent: RegExp = /^\d+(\.\d+)*%$/;

        private hexBit: RegExp = /([0-9a-f])/gi;
        private leadHex: RegExp = /^#/;

        private matchHSL: RegExp = /^hsla?\((\d{1,3}),\s*(\d{1,3})%,\s*(\d{1,3})%(,\s*([01]?\.?\d*))?\)$/;
        private matchRGB: RegExp = /^rgba?\((\d{1,3}%?),\s*(\d{1,3}%?),\s*(\d{1,3}%?)(,\s*([01]?\.?\d*))?\)$/;

        private absround(num: number): any {
            return (0.5 + num) << 0;
        }

        private hue2rgb(a: number, b: number, c: number) {
            if (c < 0) c += 1;
            if (c > 1) c -= 1;
            if (c < 1 / 6) return a + (b - a) * 6 * c;
            if (c < 1 / 2) return b;
            if (c < 2 / 3) return a + (b - a) * (2 / 3 - c) * 6;
            return a;
        }

        private p2v(p: string): number {
            return this.isPercent.test(p) ? this.absround(parseInt(p) * 2.55) : p;
        }

        private isNamedColor(key): string {
            var lc = ('' + key).toLowerCase();
            return globals.namedColors.hasOwnProperty(lc)
                ? globals.namedColors[lc]
                : null;
        }

        constructor(value?: any) {
            super();
            var that = this;
            this.addEventListener(ColorEvent.RGB_UPDATED, function (evt: events.RokkEvent) {
                that._RGBUpdated();
            });
            this.addEventListener(ColorEvent.HEX_UPDATED, function (evt: events.RokkEvent) {
                that._HEXUpdated();
            });
            this.addEventListener(ColorEvent.HSL_UPDATED, function (evt: events.RokkEvent) {
                that._HSLUpdated();
            });
            this.addEventListener(ColorEvent.HSV_UPDATED, function (evt: events.RokkEvent) {
                that._HSVUpdated();
            });
            this.addEventListener(ColorEvent.INT_UPDATED, function (evt: events.RokkEvent) {
                that._INTUpdated();
            });
            if (value !== null) this.parse(value);
        }

        private _decimal: number = 0;  // 0 - 16777215
        private _hex: string = '#000000';  // #000000 - #FFFFFF
        private _red: number = 0;  // 0 - 255
        private _green: number = 0;  // 0 - 255
        private _blue: number = 0;  // 0 - 255
        private _hue: number = 0;  // 0 - 360
        private _saturation: number = 0;  // 0 - 100
        private _lightness: number = 0;  // 0 - 100
        private _brightness: number = 0;  // 0 - 100
        private _alpha: number = 1;  // 0 - 1


        parse(value: any): Color {
            if (typeof value == 'undefined') {
                return this;
            };
            switch (true) {
                case isFinite(value):
                    this.decimal = value;
                    return this;
                case (value instanceof Color):
                    this.copy(value);
                    return this;
                default:

                    switch (true) {
                        case (globals.namedColors.hasOwnProperty(value)):
                            value = globals.namedColors[value];
                            var stripped = value.replace(this.leadHex, '');
                            this.decimal = parseInt(stripped, 16);
                            return this;
                        case this.isHex.test(value):
                            var stripped = value.replace(this.leadHex, '');
                            if (stripped.length == 3) {
                                stripped = stripped.replace(this.hexBit, '$1$1');
                            };
                            this.decimal = parseInt(stripped, 16);
                            return this;
                        case this.isRGB.test(value):
                            var parts = value.match(this.matchRGB);
                            this.red = this.p2v(parts[1]);
                            this.green = this.p2v(parts[2]);
                            this.blue = this.p2v(parts[3]);
                            this.alpha = parseFloat(parts[5]) || 1;
                            return this;
                        case this.isHSL.test(value):
                            var parts = value.match(this.matchHSL);
                            this.hue = parseInt(parts[1]);
                            this.saturation = parseInt(parts[2]);
                            this.lightness = parseInt(parts[3]);
                            this.alpha = parseFloat(parts[5]) || 1;
                            return this;
                    };


            };
            return this;
        }

        clone(): Color {
            return new Color(this.decimal);
        }

        copy(color: Color): Color {
            this.decimal = color.decimal;
            return this;
        }



        interpolate(destination, factor) {
            if (!(destination instanceof Color)) {
                destination = new Color(destination);
            };
            this._red = this.absround(+(this._red) + (destination._red - this._red) * factor);
            this._green = this.absround(+(this._green) + (destination._green - this._green) * factor);
            this._blue = this.absround(+(this._blue) + (destination._blue - this._blue) * factor);
            this._alpha = this.absround(+(this._alpha) + (destination._alpha - this._alpha) * factor);
            this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED, false, false));
            this.dispatchEvent(new ColorEvent(ColorEvent.UPDATED, false, false));
            return this;
        }

        private _RGB2HSL(): void {
            var oh = this._hue;
            var os = this._saturation;
            var ol = this._lightness;
            var ob = this._brightness;
            var r = this._red / 255;
            var g = this._green / 255;
            var b = this._blue / 255;

            var max = Math.max(r, g, b);
            var min = Math.min(r, g, b);
            var l = (max + min) / 2;
            var v = max;

            if (max == min) {
                this._hue = 0;
                this._saturation = 0;
                this._lightness = this.absround(l * 100);
                this._brightness = this.absround(v * 100);
                return;
            };

            var d = max - min;
            var s = d / ((l <= 0.5) ? (max + min) : (2 - max - min));
            var h = ((max == r)
                ? (g - b) / d + (g < b ? 6 : 0)
                : (max == g)
                ? ((b - r) / d + 2)
                : ((r - g) / d + 4)) / 6;

            this._hue = this.absround(h * 360);
            this._saturation = this.absround(s * 100);
            this._lightness = this.absround(l * 100);
            this._brightness = this.absround(v * 100);
            this.propertyChangeNotifier.notifyPropertyChanged("hue", oh, this._hue);
            this.propertyChangeNotifier.notifyPropertyChanged("saturation", os, this._saturation);
            this.propertyChangeNotifier.notifyPropertyChanged("brightness", ob, this._brightness);
            this.propertyChangeNotifier.notifyPropertyChanged("lightness", ol, this._lightness);
        }

        private _HSL2RGB = function () {
            var r = this._red;
            var g = this._green;
            var b = this._blue;
            var h = this._hue / 360;
            var s = this._saturation / 100;
            var l = this._lightness / 100;
            var q = l < 0.5 ? l * (1 + s) : (l + s - l * s);
            var p = 2 * l - q;
            this._red = this.absround(this.hue2rgb(p, q, h + 1 / 3) * 255);
            this._green = this.absround(this.hue2rgb(p, q, h) * 255);
            this._blue = this.absround(this.hue2rgb(p, q, h - 1 / 3) * 255);
            this.propertyChangeNotifier.notifyPropertyChanged("red", r, this._red);
            this.propertyChangeNotifier.notifyPropertyChanged("green", g, this._green);
            this.propertyChangeNotifier.notifyPropertyChanged("blue", b, this._blue);
        };

        private _HSV2RGB() {
            var or = this._red;
            var og = this._green;
            var ob = this._blue;
            var h = this._hue / 360;
            var s = this._saturation / 100;
            var v = this._brightness / 100;
            var r = 0;
            var g = 0;
            var b = 0;
            var i = Math.floor(h * 6);
            var f = h * 6 - i;
            var p = v * (1 - s);
            var q = v * (1 - f * s);
            var t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0:
                    r = v, g = t, b = p;
                    break;
                case 1:
                    r = q, g = v, b = p;
                    break;
                case 2:
                    r = p, g = v, b = t;
                    break;
                case 3:
                    r = p, g = q, b = v;
                    break;
                case 4:
                    r = t, g = p, b = v
                    break;
                case 5:
                    r = v, g = p, b = q;
                    break;
            }
            this._red = this.absround(r * 255);
            this._green = this.absround(g * 255);
            this._blue = this.absround(b * 255);
            this.propertyChangeNotifier.notifyPropertyChanged("red", or, this._red);
            this.propertyChangeNotifier.notifyPropertyChanged("green", og, this._green);
            this.propertyChangeNotifier.notifyPropertyChanged("blue", ob, this._blue);
        }

        private _INT2HEX() {
            var old = this._hex;
            var x = this._decimal.toString(16);
            x = '000000'.substr(0, 6 - x.length) + x;
            this._hex = '#' + x.toUpperCase();
            this.propertyChangeNotifier.notifyPropertyChanged("hex", old, this._hex);
        }

        private _INT2RGB() {
            var r = this._red;
            var g = this._green;
            var b = this._blue;
            this._red = this._decimal >> 16;
            this._green = (this._decimal >> 8) & 0xFF;
            this._blue = this._decimal & 0xFF;
            this.propertyChangeNotifier.notifyPropertyChanged("red", r, this._red);
            this.propertyChangeNotifier.notifyPropertyChanged("green", g, this._green);
            this.propertyChangeNotifier.notifyPropertyChanged("blue", b, this._blue);
        }

        private _HEX2INT() {
            var old = this._decimal;
            this._decimal = parseInt(this._hex, 16);
            this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, this._decimal);
        }

        private _RGB2INT() {
            var old = this._decimal;
            this._decimal = (this._red << 16 | (this._green << 8) & 0xffff | this._blue);
            this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, this._decimal);
        }

        private _RGBUpdated() {
            this._prepareUpdate();
            this._RGB2INT();  // populate INT values
            this._RGB2HSL();  // populate HSL values
            this._INT2HEX();  // populate HEX values
            this._broadcastUpdate();
        }
        private _HSLUpdated() {
            this._prepareUpdate();
            this._HSL2RGB();  // populate RGB values
            this._RGB2INT();  // populate INT values
            this._INT2HEX();  // populate HEX values
            this._broadcastUpdate();
        }
        private _HSVUpdated() {
            this._prepareUpdate();
            this._HSV2RGB();  // populate RGB values
            this._RGB2INT();  // populate INT values
            this._INT2HEX();  // populate HEX values
            this._broadcastUpdate();
        }
        private _HEXUpdated = function () {
            this._prepareUpdate();
            this._HEX2INT();  // populate INT values
            this._INT2RGB();  // populate RGB values
            this._RGB2HSL();  // populate HSL values
            this._broadcastUpdate();
        }
        private _INTUpdated = function () {
            this._prepareUpdate();
            this._INT2RGB();  // populate RGB values
            this._RGB2HSL();  // populate HSL values
            this._INT2HEX();  // populate HEX values
            this._broadcastUpdate();
        }

        private oldRGB: string;
        private oldPRGB: string;
        private oldRGBA: string;
        private oldHSL: string;
        private oldHSLA: string;

        private _prepareUpdate() {
            this.oldRGB = this.RGB;
            this.oldPRGB = this.PRGB;
            this.oldRGBA = this.RGBA;
            this.oldHSL = this.HSL;
            this.oldHSLA = this.HSLA;
        }

        private _broadcastUpdate() {
            this.dispatchEvent(new ColorEvent(ColorEvent.UPDATED, false, false));

            this.propertyChangeNotifier.notifyPropertyChanged("RGB", this.oldRGB, this.RGB);
            this.propertyChangeNotifier.notifyPropertyChanged("PRGB", this.oldPRGB, this.PRGB);
            this.propertyChangeNotifier.notifyPropertyChanged("RGBA", this.oldRGBA, this.RGBA);
            this.propertyChangeNotifier.notifyPropertyChanged("HSL", this.oldHSL, this.HSL);
            this.propertyChangeNotifier.notifyPropertyChanged("HSLA", this.oldHSLA, this.HSLA);
        }

        get decimal(): number {
            return this._decimal;
        }

        set decimal(value: number) {
            if (this._decimal === value) return;
            var old = this._decimal;
            this._decimal = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.INT_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, value);
        }

        get hex(): string {
            return this._hex;
        }

        set hex(value: string) {
            var old = this._hex;
            this._hex = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.HEX_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("hex", old, value);
        }

        get red(): number {
            return this._red;
        }

        set red(value: number) {
            var old = this._red;
            this._red = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("red", old, value);
        }


        get green(): number {
            return this._green;
        }

        set green(value: number) {
            var old = this._green;
            this._green = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("green", old, value);
        }

        get blue(): number {
            return this._blue;
        }

        set blue(value: number) {
            var old = this._blue;
            this._blue = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("blue", old, value);
        }

        get hue(): number {
            return this._hue;
        }

        set hue(value: number) {
            var old = this._hue;
            this._hue = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("hue", old, value);
        }

        get saturation(): number {
            return this._saturation;
        }

        set saturation(value: number) {
            var old = this._saturation;
            this._saturation = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("saturation", old, value);
        }

        get lightness(): number {
            return this._lightness;
        }

        set lightness(value: number) {
            var old = this._lightness;
            this._lightness = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("lightness", old, value);
        }

        get brightness(): number {
            return this._brightness;
        }

        set brightness(value: number) {
            var old = this._brightness;
            this._brightness = value;
            this.dispatchEvent(new ColorEvent(ColorEvent.HSV_UPDATED));
            this.propertyChangeNotifier.notifyPropertyChanged("brightness", old, value);
        }

        get alpha(): number {
            return this._alpha;
        }

        set alpha(value: number) {
            this._prepareUpdate();
            var old = this._alpha;
            this._alpha = value;
            this._broadcastUpdate();
            this.propertyChangeNotifier.notifyPropertyChanged("alpha", old, value);
        }

        get RGB(): string {
            var components = [this.absround(this._red), this.absround(this._green), this.absround(this._blue)];
            return 'rgb(' + components.join(', ') + ')';
        }

        get PRGB(): string {
            var components = [this.absround(100 * this._red / 255) + '%', this.absround(100 * this._green / 255) + '%', this.absround(100 * this._blue / 255) + '%'];
            return 'rgb(' + components.join(', ') + ')';
        }

        get RGBA(): string {
            var components = [this.absround(this._red), this.absround(this._green), this.absround(this._blue), this._alpha];
            return 'rgba(' + components.join(', ') + ')';
        }

        get HSL(): string {
            var components = [this.absround(this._hue), this.absround(this._saturation) + '%', this.absround(this._lightness) + '%'];
            return 'hsl(' + components.join(', ') + ')';
        }

        get HSLA(): string {
            var components = [this.absround(this._hue), this.absround(this._saturation) + '%', this.absround(this._lightness) + '%', this._alpha];
            return 'hsla(' + components.join(', ') + ')';
        }

        setFillColor(ctx: CanvasRenderingContext2D, x0: number, y0: number, x1: number, y1: number): void {
            ctx.fillStyle = this.toBackgroundCSS();
        }

        toStringValue(): string {
            return this.hex;
        }
    }

    //#endregion

    export class GradientStep {
        private _color: Color;

        set color(value: Color) {
            if (typeof value !== "object") {
                this._color = new Color(value);
            } else {
                this._color = value;
            }

        }

        get color(): Color {
            return this._color;
        }


        percent: number;
        constructor(color?: any, percent?: any) {
            if (color !== null) {
                if (!(color instanceof Color)) {
                    this.color = new Color(color);
                } else {
                    this.color = color;
                }
            }

            if (percent !== null) {
                this.percent = parseInt(percent);
            }
        }
    }

    export class LinearGradientDirection {
        static LEFT = "left";
        static RIGHT = "right";
        static TOP = "top";
        static BOTTOM = "bottom";
    }

export class LinearGradientBrush extends events.EventDispatcher implements IBackgroundBrush, core.IPropertyChangeNotifier {
    objectId: number;
        private notifier: core.PropertyChangeNotifier = new core.PropertyChangeNotifier();
        private _steps: collections.List = null;
        private _direction: string = "top";

        private _updateTrigger: (evt: events.RokkEvent) => void;

        get steps(): collections.List { 
            return this._steps;
        }

        get direction(): string {
            return this._direction;
        }

        set direction(value: string) {
            var old = this._direction;
            if (value !== LinearGradientDirection.LEFT &&
                value !== LinearGradientDirection.BOTTOM &&
                value !== LinearGradientDirection.RIGHT &&
                value !== LinearGradientDirection.TOP)
                throw new Error("Incorrect direction value. Correct values are: left, right, top, bottom.");
            this._direction = value;
            this.notifier.notifyPropertyChanged("direction", old, value);

            this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
        }

        set steps(value: collections.List) {
            if (this._steps !== null && this._steps !== undefined) {
                this._steps.removeEventListener(collections.CollectionEvent.ITEM_ADDED, this._updateTrigger);
                this._steps.removeEventListener(collections.CollectionEvent.ITEM_REMOVED, this._updateTrigger);
                this._steps.removeEventListener(collections.CollectionEvent.REFRESHED, this._updateTrigger);
            }
            this._steps = value;
            this._steps.addEventListener(collections.CollectionEvent.ITEM_ADDED, this._updateTrigger);
            this._steps.addEventListener(collections.CollectionEvent.ITEM_REMOVED, this._updateTrigger);
            this._steps.addEventListener(collections.CollectionEvent.REFRESHED, this._updateTrigger);
            this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
        }

        constructor() {
            super();
            var that = this;
            this._updateTrigger = (evt: events.RokkEvent): void => {
                that.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
            };
            this.steps = new collections.List();
        }

        toCSS(): string {
            var css: string = CrossBrowserCompatibility.FirstSupportedFunctionName("backgroundImage", ["linear-gradient", "-webkit-linear-gradient", "-moz-linear-gradient", "-o-linear-gradient", "-ms-linear-gradient"], "(left,#FFFFFF 0%,#000000 100%)");
            css = css + "(" + this.direction;
            for (var i: number = 0; i < this.steps.length; i++) {
                var step: GradientStep = this.steps.getItemAt(i);
                css = css + "," + step.color.RGBA + " " + step.percent + "%";
            }
            css = css + ")";
            return css;
        }

        toStringValue(): string {
            var css: string = 'linear-gradient';
            css = css + "(" + this.direction;
            for (var i: number = 0; i < this.steps.length; i++) {
                var step: GradientStep = this.steps.getItemAt(i);
                css = css + "," + step.color.RGBA + " " + step.percent + "%";
            }
            css = css + ")";
            return css;
        }

        toBackgroundCSS(): string {
            return this.toCSS();
        }

        attach(observer: core.IPropertyObserver): void {
            this.notifier.attach(observer);
        }

        detach(observer: core.IPropertyObserver) {
            this.notifier.detach(observer);
        }

        notifyPropertyChanged(property: string, oldValue: any, newValue: any): void {
            this.notifier.notifyPropertyChanged(property, oldValue, newValue);
        }

        setFillColor(ctx: CanvasRenderingContext2D, x0: number, y0: number, x1: number, y1: number): void {
            var grad: CanvasGradient;
            switch (this.direction) {
                case LinearGradientDirection.TOP:
                    grad = ctx.createLinearGradient(0, y0, 0, y1);
                    break;
                case LinearGradientDirection.BOTTOM:
                    grad = ctx.createLinearGradient(0, y1, 0, y0);
                    break;
                case LinearGradientDirection.LEFT:
                    grad = ctx.createLinearGradient(x0, 0, x1, 0);
                    break;
                case LinearGradientDirection.RIGHT:
                    grad = ctx.createLinearGradient(x1, 0, x0, 0);
                    break;
                default:
                    throw new Error("Invalid gradient direction: " + this.direction);
            }

            for (var i: number = 0; i < this.steps.length; i++) {
                grad.addColorStop(this.steps.getItemAt(i).percent / 100.0, this.steps.getItemAt(i).color.RGBA);
                console.log(this.steps.getItemAt(i).percent / 100.0, this.steps.getItemAt(i).color.RGBA);
            }
            ctx.fillStyle = grad;
        }



    }

    export class ImageBrush extends events.EventDispatcher implements IBackgroundBrush, core.IPropertyChangeNotifier {
        private propertyChangeNotifier: core.PropertyChangeNotifier = new core.PropertyChangeNotifier();
        public objectId: number;
        attach(observer: core.IPropertyObserver) {
            this.propertyChangeNotifier.attach(observer);
        }

        detach(observer: core.IPropertyObserver) {
            this.propertyChangeNotifier.detach(observer);
        }

        notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
            this.propertyChangeNotifier.notifyPropertyChanged(property, oldValue, newValue);
        }

        private _image: HTMLImageElement = null;

        set source(val: HTMLImageElement) {
            var old: HTMLImageElement = this._image;
            if (!val.complete) {
                this._image = val;
                var that = this;
                val.onload = function () {
                    that.propertyChangeNotifier.notifyPropertyChanged("source", old, val);
                    that.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
                }
            } else {
                this._image = val;
                this.propertyChangeNotifier.notifyPropertyChanged("source", old, val);
                this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
            }
        }

        get source(): HTMLImageElement {
            return this._image;
        }

        toBackgroundCSS(): string {
            if (this._image != null && this._image.complete) {
                // Create the canvas element.
                var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.createElement('canvas');
                canvas.width = this._image.width;
                canvas.height = this._image.height;
                // Get '2d' context and draw the image.
                var ctx = canvas.getContext("2d");
                ctx.drawImage(this._image, 0, 0);
                // Get canvas data URL
                try {
                    var data = canvas.toDataURL();
                } catch (e) {
                    return "none";
                }
                return "url('" + data + "')";
            } else if (this._image.src != "") {
                return "url('" + this._image.src + "')";
            }
            return "none";
        }

        toStringValue(): string {
            return this.toBackgroundCSS();
        }

        setFillColor(ctx: CanvasRenderingContext2D, x0: number, y0: number, x1: number, y1: number): void {
            if (this._image.complete) {
                ctx.drawImage(this._image, 0, 0, this._image.width, this._image.height, x0, y0, x1 - x0, y1 - y0);
            }
        }

        isComplete(): boolean {
            if (this.source !== null && this.source !== undefined)
                return false;
            return this.source.complete;
        }
    }
    export class BackgroundParser {
        public static parseBackgroundString(str: string): IBackgroundBrush {

            var gradientReg = /^linear-gradient\((left|right|top|bottom),(.*)\)$/,
                colorPairReg = /([^ ]+) ([0-9]{1,3})%/,
                urlReg = /^url\(('(.*)'|"(.*)")\)$/;
            if (gradientReg.test(str)) {
                //Linear gradient
                var matches: string[] = str.match(gradientReg);
                var gradient = new LinearGradientBrush();
                gradient.direction = matches[1];
                var colors: string[] = matches[2].split(',');
                for (var j = 0; j < colors.length; j++) {
                    var cMatches: string[] = colors[j].match(colorPairReg);
                    var step: GradientStep = new GradientStep(new Color(cMatches[1]), cMatches[2]);
                    gradient.steps.push(step);

                }
                return gradient;
            } else if (urlReg.test(str)) {
                //Image
                var iMatches: string[] = str.match(urlReg);
                var url: string;
                if (iMatches[2] !== undefined) {
                    url = iMatches[2];
                } else {
                    url = iMatches[3];
                }
                var image: HTMLImageElement = <HTMLImageElement>document.createElement("img");
                image.src = url;
                var imageBrush: ImageBrush = new ImageBrush();
                imageBrush.source = image;
                return imageBrush;
            }
            //Color
            return new Color(str);

        }

        public static parseImageString(str: string): ImageBrush {
            var urlReg = /^url\(('(.*)'|"(.*)")\)$/;
            if (urlReg.test(str)) {
                //Image
                var iMatches: string[] = str.match(urlReg);
                var url: string;
                if (iMatches[2] !== undefined) {
                    url = iMatches[2];
                } else {
                    url = iMatches[3];
                }
                var image: HTMLImageElement = <HTMLImageElement>document.createElement("img");
                image.src = url;
                var imageBrush: ImageBrush = new ImageBrush();
                imageBrush.source = image;
                return imageBrush;
            } else {
                throw new Error("Cannot parse image string :" + str);
            }
        }
    }

    export class CrossBrowserCompatibility {
        public static FirstSupportedFunctionName(property, prefixedFunctionNames, argString): string {
            var tempDiv = document.createElement("div");
            for (var i = 0; i < prefixedFunctionNames.length; ++i) {
                tempDiv.style[property] = prefixedFunctionNames[i] + argString;
                if (tempDiv.style[property] != "")
                    return prefixedFunctionNames[i];
            }
            return null;
        }
    }

    //#region ScreenRedraw

    class RedrawCallback {
        callable: () => void;
        cancelled: boolean = false;
        scope: any;
        callbackId: number = 0;
    }

    class ScreenRedrawManager {
        private static callbacks: RedrawCallback[] = [];
        private static currentCallbackId: number = 0;
        private static actualCallbackMap: Object = {};
        private static redrawScheduled: boolean = false;

        static AddCallback(callback: () => void, scope: any): number {
            var redrawCallback: RedrawCallback = new RedrawCallback();
            ScreenRedrawManager.currentCallbackId++;
            var cid: number = ScreenRedrawManager.currentCallbackId;
            redrawCallback.callable = callback;
            redrawCallback.cancelled = false;
            redrawCallback.scope = scope;
            redrawCallback.callbackId = cid;
            ScreenRedrawManager.actualCallbackMap[cid.toString()] = redrawCallback;
            ScreenRedrawManager.callbacks.push(redrawCallback);
            ScreenRedrawManager.ScheduleRedraw();
            return cid;
        }

        static CancelCallback(callbackId: number): boolean {
            if (ScreenRedrawManager.actualCallbackMap[callbackId.toString()] !== undefined) {
                ScreenRedrawManager.actualCallbackMap[callbackId.toString()].cancelled = true;
                return true;
            }
            return false;
        }

        static DoRedraw() {
            var callbacksCopy: RedrawCallback[] = ScreenRedrawManager.callbacks.reverse();
            ScreenRedrawManager.callbacks = [];
            ScreenRedrawManager.redrawScheduled = false;
            while (callbacksCopy.length > 0) {
                var item: RedrawCallback = callbacksCopy.pop();
                if (!item.cancelled) {
                    item.callable.apply(item.scope, []);
                }
                ScreenRedrawManager.actualCallbackMap[item.callbackId.toString()] = undefined;
            }
        }

        static ScheduleRedraw() {
            if (!ScreenRedrawManager.redrawScheduled) {
                //if(ScreenRedrawManager.DoRedraw == undefined) throw new Error("ScreenRedrawManager not found");
                if (Modernizr.prefixed("requestAnimationFrame", window) !== undefined) {
                    try {
                        Modernizr.prefixed("requestAnimationFrame", window).apply({}, [ScreenRedrawManager.DoRedraw]);

                    } catch (error) {
                        setTimeout(ScreenRedrawManager.DoRedraw, 1000 / 60);
                    }

                } else {
                    setTimeout(ScreenRedrawManager.DoRedraw, 1000 / 60);
                }

                ScreenRedrawManager.redrawScheduled = true;
            }
        }
    }

    export class InteractiveObject extends events.EventDispatcher {


        public _domElement: HTMLElement = null;
        get domElement(): HTMLElement {
            if (this._domElement === null || this._domElement === undefined) {
                this._domElement = this.createDOMElement();
            }
            return this._domElement;
        }

        private registeredDOMEvents: string[] = [];
        private registerDOMEvent(event: string) {
            if (this.registeredDOMEvents.indexOf(event) === -1) {
                var that: InteractiveObject = this;
                this.domElement.addEventListener(event, function (event: Event) {
                    if (event.preventDefault) event.preventDefault();
                    var revent = new events.RokkEvent(event.type, event.bubbles, event.cancelable);
                    revent.originalEvent = event;
                    event.stopImmediatePropagation();
                    that.dispatchEvent(revent);
                    return false;
                });
                this.registeredDOMEvents.push(event);
            }
        }

        constructor() {
            super();
        }

        createDOMElement(): HTMLElement {
            var div: HTMLDivElement = <HTMLDivElement>window.document.createElement('div');
            div.style[Modernizr.prefixed('boxSizing')] = 'border-box';
            div.style.position = 'absolute';
            div.className = "noSelect";
            return div;
        }

        addEventListener(eventType: string, listener: (evt: events.RokkEvent) => void, once: boolean = false, useCapture: boolean = false) {

            if (globals.DOMEvents.indexOf(eventType) !== -1) {
                this.registerDOMEvent(eventType);
            }
            super.addEventListener(eventType, listener, once, useCapture);
        }




    }

    //#endregion

    export class Matrix {
        public a: number = 1;
        public b: number = 0;
        public c: number = 0;
        public d: number = 1;
        public e: number = 0;
        public f: number = 0;

        constructor(a?, b?, c?, d?, e?, f?) {
            if (a !== null && a !== undefined) {
                this.a = +a;
                this.b = +b;
                this.c = +c;
                this.d = +d;
                this.e = +e;
                this.f = +f;
            }
        }

        add(a, b, c, d, e, f) {
            var out = [
                [],
                [],
                []
            ],
                m = [
                    [this.a, this.c, this.e],
                    [this.b, this.d, this.f],
                    [0, 0, 1]
                ],
                matrix = [
                    [a, c, e],
                    [b, d, f],
                    [0, 0, 1]
                ],
                x,
                y,
                z,
                res;

            if (a && a instanceof Matrix) {
                matrix = [
                    [a.a, a.c, a.e],
                    [a.b, a.d, a.f],
                    [0, 0, 1]
                ];
            }

            for (x = 0; x < 3; x++) {
                for (y = 0; y < 3; y++) {
                    res = 0;
                    for (z = 0; z < 3; z++) {
                        res += m[x][z] * matrix[z][y];
                    }
                    out[x][y] = res;
                }
            }
            this.a = out[0][0];
            this.b = out[1][0];
            this.c = out[0][1];
            this.d = out[1][1];
            this.e = out[0][2];
            this.f = out[1][2];
        }

        invert() {
            var me = this,
                x = me.a * me.d - me.b * me.c;
            return new Matrix(me.d / x, -me.b / x, -me.c / x, me.a / x, (me.c * me.f - me.d * me.e) / x, (me.b * me.e - me.a * me.f) / x);
        }

        clone() {
            return new Matrix(this.a, this.b, this.c, this.d, this.e, this.f);
        }

        translate(x, y) {
            this.add(1, 0, 0, 1, x, y);
        }

        shear(x, y, cx, cy) {
            (y === null || y === undefined) && (y = x);
            (cx || cy) && this.add(1, 0, 0, 1, cx, cy);
            this.add(1, x, y, 1, 0, 0);
            (cx || cy) && this.add(1, 0, 0, 1, -cx, -cy);

        }

        scale(x, y, cx, cy) {
            (y === null || y === undefined) && (y = x);
            (cx || cy) && this.add(1, 0, 0, 1, cx, cy);
            this.add(x, 0, 0, y, 0, 0);
            (cx || cy) && this.add(1, 0, 0, 1, -cx, -cy);
        }

        rotate(a, x?, y?) {
            a = core.MathTools.toRadians(a);
            x = x || 0;
            y = y || 0;
            var cos = +Math.cos(a).toFixed(9),
                sin = +Math.sin(a).toFixed(9);
            this.add(cos, sin, -sin, cos, x, y);
            this.add(1, 0, 0, 1, -x, -y);
        }

        x(x, y) {
            return x * this.a + y * this.c + this.e;
        }

        y(x, y) {
            return x * this.b + y * this.d + this.f;
        }

        toString() {
            if (Modernizr.webgl) {
                return "matrix3d(" + [this.a, this.b, 0, 0, this.c, this.d, 0, 0, 0, 0, 1, 0, this.e, this.f, 0, 1].join(',') + ")";
            } else {
                return "matrix(" + [this.a, this.b, this.c, this.d, this.e, this.f].join(',') + ")";
            }
        }

        toFilter() {
            return "progid:DXImageTransform.Microsoft.Matrix(M11=" + this.a +
                ", M12=" + this.c + ", M21=" + this.b + ", M22=" + this.d +
                ", Dx=" + this.e + ", Dy=" + this.f + ", sizingmethod='auto expand')";
        }

        offset() {
            return [this.e.toFixed(4), this.f.toFixed(4)];
        }

        normalize(a) {
            var mag = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
            a[0] && (a[0] /= mag);
            a[1] && (a[1] /= mag);
        }

        extract = function () {
            var out = { dx: 0, dy: 0, scalex: 0, scaley: 0, shear: 0, rotate: 0, isSimple: false, isSuperSimple: false, noRotation: false };
            // translation
            out.dx = this.e;
            out.dy = this.f;

            // scale and shear
            var row = [
                [this.a, this.c],
                [this.b, this.d]
            ];
            var a = this.a;
            out.scalex = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
            this.normalize(row[0]);

            out.shear = row[0][0] * row[1][0] + row[0][1] * row[1][1];
            row[1] = [row[1][0] - row[0][0] * out.shear, row[1][1] - row[0][1] * out.shear];

            out.scaley = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
            this.normalize(row[1]);
            out.shear /= out.scaley;

            // rotation
            var sin = -row[0][1],
                cos = row[1][1];
            if (cos < 0) {
                out.rotate = core.MathTools.toDegree(Math.acos(cos));
                if (sin < 0) {
                    out.rotate = 360 - out.rotate;
                }
            } else {
                out.rotate = core.MathTools.toDegree(Math.asin(sin));
            }

            out.isSimple = !+out.shear.toFixed(9) && (out.scalex.toFixed(9) == out.scaley.toFixed(9) || !out.rotate);
            out.isSuperSimple = !+out.shear.toFixed(9) && out.scalex.toFixed(9) == out.scaley.toFixed(9) && !out.rotate;
            out.noRotation = !+out.shear.toFixed(9) && !out.rotate;
            return out;
        }
    }



    //#region RenderTransform
    export class RenderTransform extends core.PropertyChangeNotifier {
        applyTo(matrix: Matrix) {

        }
    }

    export class RotateTransform extends RenderTransform {
        private _angle: number = 0;
        private _x: number = 0;
        private _y: number = 0;

        set angle(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._angle;
            if (value === old) return;
            this._angle = value;
            this.notifyPropertyChanged("angle", old, value);
        }

        get angle(): number {
            return this._angle;
        }

        set x(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._x;
            if (value === old) return;
            this._x = value;
            this.notifyPropertyChanged("x", old, value);
        }

        get x(): number {
            return this._x;
        }

        set y(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._y;
            if (value === old) return;
            this._y = value;
            this.notifyPropertyChanged("y", old, value);
        }

        get y(): number {
            return this._y;
        }

        applyTo(matrix: Matrix) {
            matrix.rotate(this.angle,this.x,this.y);
        }
    }

    export class ScaleTransform extends RenderTransform {
        private _scaleX: number = 1.0;
        private _scaleY: number = 1.0;
        private _x: number = 0;
        private _y: number = 0;

        set scaleX(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._scaleX;
            if (value === old) return;
            this._scaleX = value;
            this.notifyPropertyChanged("scaleX", old, value);
        }

        get scaleX(): number {
            return this._scaleX;
        }

        set scaleY(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._scaleY;
            if (value === old) return;
            this._scaleY = value;
            this.notifyPropertyChanged("scaleY", old, value);
        }

        get scaleY(): number {
            return this._scaleY;
        }

        set x(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._x;
            if (value === old) return;
            this._x = value;
            this.notifyPropertyChanged("x", old, value);
        }

        get x(): number {
            return this._x;
        }

        set y(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._y;
            if (value === old) return;
            this._y = value;
            this.notifyPropertyChanged("y", old, value);
        }

        get y(): number {
            return this._y;
        }


        applyTo(matrix: Matrix) {
            matrix.scale(this.scaleX, this.scaleY, this.x, this.y);
        }

    }

    export class SkewTransform extends RenderTransform {
        private _angleX: number = 0.0;
        private _angleY: number = 0.0;

        set angleX(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._angleX;
            if (value === old) return;
            this._angleX = value;
            this.notifyPropertyChanged("angleX", old, value);
        }

        get angleX(): number {
            return this._angleX;
        }

        set angleY(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._angleY;
            if (value !== old) return;
            this._angleY = value;
            this.notifyPropertyChanged("angleY", old, value);
        }

        get angleY(): number {
            return this._angleY;
        }


        applyTo(matrix: Matrix) {
            matrix.shear(0, 0, this.angleX, this.angleY);
        }

    }

    export class TranslateTransform extends RenderTransform {
        private _x: number = 0.0;
        private _y: number = 0.0;

        set x(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._x;
            if (value === old) return;
            this._x = value;
            this.notifyPropertyChanged("x", old, value);
        }

        get x(): number {
            return this._x;
        }

        set y(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            var old: number = this._y;
            if (value === old) return;
            this._y = value;
            this.notifyPropertyChanged("y", old, value);
        }

        get y(): number {
            return this._y;
        }

        applyTo(matrix: Matrix) {
            matrix.translate(this.x, this.y);
        }
    }
    //#endregion

    export class HorizontalAlign {
        static LEFT: string = "left";
        static RIGHT: string = "right";
        static CENTER: string = "center";
        static STRETCH: string = "stretch";
    }

    export class VerticalAlign {
        static TOP: string = "top";
        static BOTTOM: string = "bottom";
        static CENTER: string = "center";
        static STRETCH: string = "stretch";
    }

    export interface IDisplayObject extends core.IPropertyChangeNotifier, events.IEventDispatcher {
        top: number;
        left: number;
        right: number;
        bottom: number;
        width: number;
        height: number;
        gridColumn: number;
        gridRow: number;
        gridRowSpan: number;
        gridColumnSpan: number;
        visible: boolean;
        verticalAlign: string;
        horizontalAlign: string;
        htmlClass: string;
        parent: IDisplayContainer;
        domElement: HTMLElement;
        measuredWidth: number;
        measuredHeight: number;
        checkSize(): void;
        addToDisplayList();
        removeFromDisplayList();
        renderTransformations: RenderTransform[];
        alpha: number;
        addClass(string): void;
        removeClass(string): void;
        getClasses(): string[];
        globalToLocal(point:core.Point):core.Point;
    }

    export class IIDisplayObject implements core.IInterfaceChecker {
        className: string = "IIDisplayObject";
        methodNames: string[] = ["checkSize"];
        inheritsFrom: core.IInterfaceChecker[] = [new core.IIPropertyChangeNotifier(), new events.IIEventDispatcher()];
    }

    export class DisplayListEvent extends events.RokkEvent {
        static ADDED_TO_DISPLAY_LIST: string = "addedToDisplayList";
        static REMOVED_FROM_DISPLAY_LIST: string = "removedFromDisplayList";
    }

    export class DisplayObject extends InteractiveObject implements IDisplayObject, core.IPropertyObserver {
        private _renderTransformations: RenderTransform[] = new Array<RenderTransform>();
        public objectId: number;
        private renderMatrix: Matrix;

        private visibleChanged: boolean = false;

        private _htmlClass: string;

        public set htmlClass(val: string) {
            if (this._domElement.className === val) return;
            var old = this._domElement.className;
            this._domElement.className = val;
            this.notifyPropertyChanged("htmlClass", old, val);
        }

        public get htmlClass(): string {
            return this._domElement.className;
        }

        public getClasses(): string[]{
            return this._domElement.className.split(" ");
        }

        public addClass(className: string) {
            var classes = this.getClasses();
            if (classes.indexOf(className) !== -1) return;
            classes.push(className);
            this.htmlClass = classes.join(" ");
        }

        public removeClass(className: string) {
            var classes = this.getClasses();
            if (classes.indexOf(className) === -1) return;
            classes.splice(classes.indexOf(className),1);
            this.htmlClass = classes.join(" ");
        }


        private _visible: boolean = true;

        public get visible(): boolean {
            return this._visible;
        }

        public set visible(val: boolean) {
            val = core.PrimitiveTypeTools.ensureBool(val);
            if (this._visible === val) return;
            this._visible = val;
            this.visibleChanged = true;
            this.invalidateProperties();
            this.notifyPropertyChanged("visible", !val, val);
        }

        private _alpha: number = 1.0;

        private alphaChanged = false;

        public set alpha(value: number) {
            value = core.PrimitiveTypeTools.ensureFloat(value);
            if (value === this._alpha) return;
            var old = this._alpha;
            this.alphaChanged = true;
            this._alpha = value;
            this.notifyPropertyChanged("alpha", old, value);
            this.invalidateProperties();
        }

        public get alpha(): number {
            return this._alpha;
        }

        public addedToDisplayList: boolean = false;

        public globalToLocal(point: core.Point): core.Point {
            var localPoint: core.Point = new core.Point(
                point.x - this.domElement.getBoundingClientRect().left - this.domElement.clientLeft + this.domElement.scrollLeft,
                point.y - this.domElement.getBoundingClientRect().top - this.domElement.clientTop + this.domElement.scrollTop
                );
            return localPoint;

        }

        public addToDisplayList() {
            this.addedToDisplayList = true;
            this.invalidateSize();
            this.dispatchEvent(new DisplayListEvent(DisplayListEvent.ADDED_TO_DISPLAY_LIST));
        }

        public removeFromDisplayList() {
            this.addedToDisplayList = false;
            this.dispatchEvent(new DisplayListEvent(DisplayListEvent.REMOVED_FROM_DISPLAY_LIST));
        }

        get renderTransformations(): RenderTransform[] {
            return this._renderTransformations;
        }

        set renderTransformations(list: RenderTransform[]) {
            var old = this._renderTransformations;
            if (!(list instanceof Array)) {
                var item: any = list;
                list = new Array<RenderTransform>();
                list.push(item);
            }
            if (old !== null && old !== undefined) {
                for (var i in old) {
                    old[i].detach(this);
                }
            }
            for (var i in list) {
                if (!(list[i] instanceof RenderTransform))
                    throw new Error("You can only assign core.RenderTransformation objects to Interactive object renderTransformations property.");
                list[i].attach(this);
            }
            this._renderTransformations = list;
            this.updateRenderMatrix();
            this.notifyPropertyChanged("renderTransformations", old, this.renderTransformations);
        }

        private renderMatrixUpdated: boolean = false;

        private updateRenderMatrix() {
            var m: Matrix = new Matrix();
            for (var i in this.renderTransformations) {
                this.renderTransformations[i].applyTo(m);
            }
            this.renderMatrix = m;
            this.renderMatrixUpdated = true;
            this.invalidateProperties();
        }

        update(observable: core.IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
            if (this.renderTransformations.indexOf(<RenderTransform>observable) !== -1) {
                this.updateRenderMatrix();
            }
        }

        private _top: number = 0;
        private _left: number = 0;
        private _right: number = 0;
        private _bottom: number = 0;
        private _width: number = 100;
        private _height: number = 100;
        private _gridColumn: number = 0;
        private _gridRow: number = 0;
        private _gridRowSpan: number = 1;
        private _gridColumnSpan: number = 1;
        private _parent: IDisplayContainer = null;
        private observers: core.IPropertyObserver[] = [];
        private _horizontalAlign: string = "left";
        private _verticalAlign: string = "top";
        private _measuredWidth: number = -1;
        private _measuredHeight: number = -1;
        private _wrapper: HTMLElement = null;
        private originalDomElement: HTMLElement;

        public _domElement: HTMLElement;

        constructor(element?: HTMLElement) {
            super();
            if (element === null || element === undefined) {
                element = document.createElement('div');
            }
            this._domElement = <HTMLElement>element;
        }

        get domElement(): HTMLElement {
            return this._domElement;
        }



        get measuredWidth(): number {
            return this._measuredWidth;
        }

        get measuredHeight(): number {
            return this._measuredHeight;
        }

        /*get wrapper(): HTMLElement {
            if (this._wrapper === null) {
                this._wrapper = document.createElement('div');
                this._wrapper.appendChild(this.domElement);
            }
            return this._wrapper;
        }*/

        private lastPropertyInvalidation: number = -1;

        invalidateProperties() {
            if (this.lastPropertyInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastPropertyInvalidation);
            }
            this.lastPropertyInvalidation = ScreenRedrawManager.AddCallback(this.commitProperties, this);
        }

        commitProperties() {
            //Stub for custom property updates
            if (this.renderMatrixUpdated) {
                this.domElement.style[Modernizr.prefixed("transform")] = this.renderMatrix.toString();
                this.renderMatrixUpdated = false;
            }
            if (this.alphaChanged) {
                this.alphaChanged = false;
                this.domElement.style.opacity = this.alpha.toString();
                if (this.alpha <= 0.0) {
                    this.domElement.style.display = "none";
                } else if (this.visible && this.domElement.style.display === "none") {
                    this.domElement.style.display = "block";
                    if (this.measuredHeight === -1) this.invalidateSize();
                }
            }

            if (this.visibleChanged) {
                this.visibleChanged = false;
                if (this.visible) {
                    if (this.alpha <= 0.0) {
                        this.domElement.style.display = "none";
                    } else {
                        this.domElement.style.display = "block";
                        if (this.measuredHeight === -1) this.invalidateSize();
                    }
                    
                } else {
                    this.domElement.style.display = "none";
                }
            }
        }

        private lastDisplayListInvalidation: number = -1;

        invalidateDisplayList() {
            if (this.lastDisplayListInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastDisplayListInvalidation);
            }
            this.lastDisplayListInvalidation = ScreenRedrawManager.AddCallback(this.updateDisplayList, this);
        }

        private lastSizeInvalidation: number = -1;

        invalidateSize() {
            if (this.lastSizeInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastSizeInvalidation);
            }
            this.lastSizeInvalidation = ScreenRedrawManager.AddCallback(this.checkSize, this);
        }

        updateDisplayList() {
            this.invalidateSize();
        }


        updateSize(newWidth: number, newHeight: number) {
            var oldMWidth = this.measuredWidth;
            var oldMHeight = this.measuredHeight;
            this._measuredWidth = newWidth;
            this._measuredHeight = newHeight;
            if (oldMWidth !== newWidth) this.notifyPropertyChanged("measuredWidth", oldMWidth, newWidth);
            if (oldMHeight !== newHeight) this.notifyPropertyChanged("measuredHeight", oldMHeight, newHeight);
        }

        checkSize() {
            if (this.domElement.style.display === "none") return;
            if (this.measuredWidth !== this.domElement.clientWidth ||
                this.measuredHeight !== this.domElement.clientHeight) {
                this.updateSize(this.domElement.clientWidth, this.domElement.clientHeight);
            }
        }

        get top(): number {
            return this._top;
        }

        get left(): number {
            return this._left;
        }

        get right(): number {
            return this._right;
        }

        get bottom(): number {
            return this._bottom;
        }

        get width(): number {
            return this._width;
        }

        get height(): number {
            return this._height;
        }

        get gridColumn(): number {
            return this._gridColumn;
        }

        get gridRow(): number {
            return this._gridRow;
        }

        get gridColumnSpan(): number {
            return this._gridColumnSpan;
        }

        get gridRowSpan(): number {
            return this._gridRowSpan;
        }

        get parent(): IDisplayContainer {
            return this._parent;
        }

        get verticalAlign(): string {
            return this._verticalAlign;
        }

        get horizontalAlign(): string {
            return this._horizontalAlign;
        }

        set parent(value: IDisplayContainer) {
            this._parent = value;
        }

        set top(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.top === value) return;
            var old = this.top;
            this._top = value;
            this.notifyPropertyChanged("top", old, value);
        }

        set left(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.left === value) return;
            var old = this.left;
            this._left = value;
            this.notifyPropertyChanged("left", old, value);
        }

        set right(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.right === value) return;
            var old = this.right;
            this._right = value;
            this.notifyPropertyChanged("right", old, value);
        }

        set bottom(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.bottom === value) return;
            var old = this.bottom;
            this._bottom = value;
            this.notifyPropertyChanged("bottom", old, value);
        }

        set width(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.width === value) return;
            var old = this.width;
            this._width = value;
            this.notifyPropertyChanged("width", old, value);
        }

        set height(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (this.height === value) return;
            var old = this.height;
            this._height = value;
            this.notifyPropertyChanged("height", old, value);
        }

        set horizontalAlign(value: string) {
            if (value !== HorizontalAlign.LEFT &&
                value !== HorizontalAlign.RIGHT &&
                value !== HorizontalAlign.CENTER &&
                value !== HorizontalAlign.STRETCH)
                throw new Error("Incorrect horizontalAlign value. Correct values are: left, right, center, stretch.");
            if (this.horizontalAlign === value) return;
            var old = this.horizontalAlign;
            this._horizontalAlign = value;
            this.notifyPropertyChanged("horizontalAlign", old, value);
        }

        set verticalAlign(value: string) {
            if (value !== VerticalAlign.TOP &&
                value !== VerticalAlign.BOTTOM &&
                value !== VerticalAlign.CENTER &&
                value !== VerticalAlign.STRETCH)
                throw new Error("Incorrect verticalAlign value. Correct values are: top, bottom, center, stretch.");
            if (this.verticalAlign === value) return;
            var old = this.verticalAlign;
            this._verticalAlign = value;
            this.notifyPropertyChanged("verticalAlign", old, value);
        }

        attach(observer: core.IPropertyObserver) {
            this.observers.push(observer);
        }

        detach(observer: core.IPropertyObserver) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        }

        notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        }

        getPropagationParent(): events.IEventDispatcher {
            return this.parent;
        }


    }




    export interface IDisplayContainer extends IDisplayObject {
        addChild(child: IDisplayObject);
        addChildAt(child: IDisplayObject, index: number);
        getChildIndex(child: IDisplayObject);
        getChildAt(index: number): IDisplayObject;
        childrenNum: number;
        removeChild(child: IDisplayObject);
        removeChildAt(index: number);
        owned(child: IDisplayObject): boolean;
        hasChild(child: IDisplayObject): boolean;
    }

    export class IIDisplayContainer implements core.IInterfaceChecker {
        className: string = "IIDisplayContainer";
        methodNames: string[] = ["addChild", "addChildAt", "getChildAt", "removeChild", "removeChildAt", "owned", "hasChild","getChildIndex"];
        inheritsFrom: core.IInterfaceChecker[] = [new IIDisplayObject()];
    }

    export class DisplayContainerEvent extends events.RokkEvent {
        public static ITEM_ADDED = "item_added";
        public static ITEM_REMOVED = "item_removed";
        public index: number;
        public item: IDisplayObject;
    }

    export class DisplayContainer extends DisplayObject implements IDisplayContainer, core.IPropertyObserver {
        private children: collections.List;

        constructor(node: HTMLElement) {
            super(node);
            this.children = new collections.List();
        }

        addChild(child: IDisplayObject) {
            this.takeOwnership(child);
            this.children.push(child);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_ADDED, false, false);
            event.index = this.children.length - 1;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        }

        getChildIndex(child: IDisplayObject) {
            return this.children.indexOf(child);
        }

        addChildAt(child: IDisplayObject, index: number) {
            this.takeOwnership(child);
            this.children.addItemAt(index, child);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_ADDED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        }

        getChildAt(index: number): IDisplayObject {
            return this.children.getItemAt(index);
        }

        get childrenNum(): number {
            return this.children.length;
        }

        removeChild(child: IDisplayObject) {
            this.releaseOwnership(child);
            var index = this.children.indexOf(child);
            this.children.removeItemAt(index);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_REMOVED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);

            this.invalidateDisplayList();
        }

        removeChildAt(index: number) {
            var child = this.children.getItemAt(index);
            this.releaseOwnership(child);
            this.children.removeItemAt(index);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_REMOVED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        }

        hasChild(child: IDisplayObject): boolean {
            return this.children.indexOf(child) !== -1;
        }

        owned(child: IDisplayObject): boolean {
            return child.parent === this;
        }

        private takeOwnership(child: IDisplayObject) {
            if (this.owned(child)) return;
            child.attach(this);
            if (child.parent !== null) {
                child.parent.removeChild(child);
            }
            child.parent = this;
        }

        private releaseOwnership(child: IDisplayObject) {
            if (!this.owned(child)) return;
            child.detach(this);
            child.parent = null;
            child.removeFromDisplayList();
        }

        updateSize(newWidth: number, newHeight: number) {
            super.updateSize(newWidth, newHeight);
            for (var i: number = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).checkSize();
            }
        }

        appendChildren() {
            //this.domElement.innerHTML = '';
            for (var i: number = 0; i < this.children.length; i++) {
                this.domElement.appendChild(this.children.getItemAt(i).domElement);
                if (this.addedToDisplayList)
                    this.children.getItemAt(i).addToDisplayList();
            }
        }

        updateDisplayList() {
            this.appendChildren();
            this.fixWrappers();
            super.updateDisplayList();
        }

        fixWrappers() {
            //Stub for item placement
        }

        update(observable: core.IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
            //Stub for item repositioning
            super.update(observable, property, oldValue, newValue);
        }

        addToDisplayList() {
            super.addToDisplayList();
            for (var i: number = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).addToDisplayList();
            }
        }

        removeFromDisplayList() {
            super.removeFromDisplayList();
            for (var i: number = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).removeFromDisplayList();
            }
        }

    }

    export class HTMLElementWrapper extends DisplayContainer {

    }

    export class StockContainerAlign {
        static VERTICAL: string = "vertical";
        static HORIZONTAL: string = "horizontal";
    }


    export class Group extends DisplayContainer {
        private _inner: HTMLElement = null;

        get inner(): HTMLElement {
            if (this._inner === null) {
                this._inner = document.createElement('div');
                this._inner.style.position = "relative";
                this._inner.style.width = "100%";
                this._inner.style.height = "100%";
            }
            return this._inner;
        }

        appendChildren() {
            this.domElement.appendChild(this.inner);
            for (var i: number = 0; i < this.childrenNum; i++) {
                this.inner.appendChild(this.getChildAt(i).domElement);
                if (this.addedToDisplayList)
                    this.getChildAt(i).addToDisplayList();
            }
        }

        private positioning(child: IDisplayObject) {
            child.domElement.style.position = "absolute";
            var topMarginModifier: number = 0;
            var leftMarginModifier: number = 0;
            if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                child.domElement.style.left = "0";
                child.domElement.style.right = "0";
            } else if (child.horizontalAlign === HorizontalAlign.LEFT) {
                child.domElement.style.left = "0";
                child.domElement.style.right = "";
            } else if (child.horizontalAlign === HorizontalAlign.RIGHT) {
                child.domElement.style.right = "0";
                child.domElement.style.left = "";
            } else if (child.horizontalAlign === HorizontalAlign.CENTER) {
                child.domElement.style.right = "";
                child.domElement.style.left = "50%";
                leftMarginModifier = (-1) * Math.round(child.measuredWidth / 2);
            }

            if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                child.domElement.style.width = "";
            } else {
                child.domElement.style.width = child.width.toString() + "px";
            }

            if (child.verticalAlign === VerticalAlign.STRETCH) {
                child.domElement.style.top = "0";
                child.domElement.style.bottom = "0";
            } else if (child.verticalAlign === VerticalAlign.TOP) {
                child.domElement.style.top = "0";
                child.domElement.style.bottom = "";
            } else if (child.verticalAlign === VerticalAlign.BOTTOM) {
                child.domElement.style.top = "";
                child.domElement.style.bottom = "0";
            } else if (child.verticalAlign === VerticalAlign.CENTER) {
                child.domElement.style.top = "50%";
                child.domElement.style.bottom = "";
                topMarginModifier = (-1) * Math.round(child.measuredHeight / 2);
            }

            if (child.verticalAlign === VerticalAlign.STRETCH) {
                child.domElement.style.height = "";
            } else {
                child.domElement.style.height = child.height.toString() + "px";
            }

            child.domElement.style.marginTop = (child.top + topMarginModifier).toString() + "px";
            child.domElement.style.marginBottom = child.bottom.toString() + "px";
            child.domElement.style.marginLeft = (child.left + leftMarginModifier).toString() + "px";
            child.domElement.style.marginRight = child.right.toString() + "px";
        }

        fixWrappers() {
            for (var i: number = 0; i < this.childrenNum; i++) {
                var child: IDisplayObject = this.getChildAt(i);
                this.positioning(child);

            }
        }

        update(observable: core.IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
            super.update(observable, property, oldValue, newValue);
            if (core.InterfaceChecker.implementsInterface(observable, new core.InterfaceChecker(new IIDisplayObject())) && this.owned(<IDisplayObject>observable)) {
                if (property === "width" || property === "height" || property === "top" || property === "left" || property === "bottom" || property === "right" || property === "horizontalAlign" || property === "verticalAlign" || property === "measuredWidth" || property === "measuredHeight") {
                    this.invalidateDisplayList();
                }
            }
        }
    }

    export class StockContainer extends Group {

        private _align = StockContainerAlign.HORIZONTAL;

        public get align(): string {
            return this._align;
        }

        private _gap: number = 0;

        public get gap(): number {
            return this._gap;
        }

        public set gap(value:number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            if (value === this._gap) return;
            var old = this._gap;
            this.notifyPropertyChanged("gap", old, value);
            this.invalidateDisplayList();
        }

        public set align(value: string) {
            if (value !== StockContainerAlign.VERTICAL &&
                value !== StockContainerAlign.HORIZONTAL) return;
            if (value === this._align) return;
            var old = this._align;
            this._align = value;
            this.notifyPropertyChanged("align", old, value);
            this.invalidateDisplayList();
        }

        private positioningStock(child: IDisplayObject, lastValue: number): number {
            child.domElement.style.position = "absolute";
            var currentValue = lastValue;
            var topMarginModifier: number = 0;
            var leftMarginModifier: number = 0;
            if (this.align === StockContainerAlign.HORIZONTAL) {
                //Align child horizontally
                child.domElement.style.left = currentValue.toString(10) + "px";

                if (child.verticalAlign === VerticalAlign.STRETCH) {
                    child.domElement.style.top = "0";
                    child.domElement.style.bottom = "0";
                } else if (child.verticalAlign === VerticalAlign.TOP) {
                    child.domElement.style.top = "0";
                    child.domElement.style.bottom = "";
                } else if (child.verticalAlign === VerticalAlign.BOTTOM) {
                    child.domElement.style.top = "";
                    child.domElement.style.bottom = "0";
                } else if (child.verticalAlign === VerticalAlign.CENTER) {
                    child.domElement.style.top = "50%";
                    child.domElement.style.bottom = "";
                    topMarginModifier = (-1) * Math.round(child.measuredHeight / 2);
                }

                if (child.verticalAlign === VerticalAlign.STRETCH) {
                    child.domElement.style.height = "";
                } else {
                    child.domElement.style.height = child.height.toString() + "px";
                }

                currentValue += child.measuredWidth;
                    
            } else {
                //Align child vertically
                child.domElement.style.top = currentValue.toString(10) + "px";

                var leftMarginModifier: number = 0;
                if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                    child.domElement.style.left = "0";
                    child.domElement.style.right = "0";
                } else if (child.horizontalAlign === HorizontalAlign.LEFT) {
                    child.domElement.style.left = "0";
                    child.domElement.style.right = "";
                } else if (child.horizontalAlign === HorizontalAlign.RIGHT) {
                    child.domElement.style.right = "0";
                    child.domElement.style.left = "";
                } else if (child.horizontalAlign === HorizontalAlign.CENTER) {
                    child.domElement.style.right = "";
                    child.domElement.style.left = "50%";
                    leftMarginModifier = (-1) * Math.round(child.measuredWidth / 2);
                }

                if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                    child.domElement.style.width = "";
                } else {
                    child.domElement.style.width = child.width.toString() + "px";
                }

                currentValue += child.measuredHeight;
            }

            child.domElement.style.marginTop = (child.top + topMarginModifier).toString() + "px";
            child.domElement.style.marginBottom = child.bottom.toString() + "px";
            child.domElement.style.marginLeft = (child.left + leftMarginModifier).toString() + "px";
            child.domElement.style.marginRight = child.right.toString() + "px";

            return currentValue + this.gap;
        }

        fixWrappers() {
            var lastValue: number = 0;
            for (var i: number = 0; i < this.childrenNum; i++) {
                var child: IDisplayObject = this.getChildAt(i);
                lastValue = this.positioningStock(child, lastValue);

            }
        }
    }


    export interface IApplication extends IDisplayContainer {
        keyedItems: Object;
        resourceDictionary: ResourceDictionary;
    }

    export class IIAplication implements core.IInterfaceChecker {
        className: string = "IIAplication";
        methodNames: string[] = [];

        inheritsFrom: core.IInterfaceChecker[] = [new IIDisplayContainer()];
    }

 /*   export class Binding {
        source: core.IPropertyChangeNotifier;
        sourceProperty: string;
        target: Object;
        targetProperty: string;

        isSource(source: core.IPropertyChangeNotifier, sourceProperty: string) {
            return (this.source === source && this.sourceProperty === sourceProperty);
        }
    }

    export class BindingManager implements core.IPropertyObserver {
        private bindings: Binding[] = [];

        bind(source: core.IPropertyChangeNotifier, sourceProperty: string, target: Object, targetProperty: string): Binding {
            var binding: Binding = new Binding();
            binding.source = source;
            binding.sourceProperty = sourceProperty;
            binding.target = target;
            binding.targetProperty = targetProperty;
            this.bindings.push(binding);
            return binding;
        }

        unbind(binding: Binding) {
            this.bindings.splice(this.bindings.indexOf(binding), 1);
        }

        update(observable: core.IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
            for (var i: number = 0; i < this.bindings.length; i++) {
                if (this.bindings[i].isSource(observable, property)) {
                    //Apply binding
                    this.bindings[i].target[this.bindings[i].targetProperty] = newValue;
                }
            }
        }

        private static instance;

        static getInstance(): BindingManager {
            if (BindingManager.instance === undefined || BindingManager.instance === null) {
                BindingManager.instance = new BindingManager();
            }
            return BindingManager.instance;
        }
    }*/

    export class State {

    }

    export interface IStage {
        states: State[];
        transitions: any[];
        currentState: string;

    }

    export class Definition {
        private xml: HTMLElement;

        public factory(scope: any): any {
            
        }
    }

    export class Application extends HTMLElementWrapper implements IApplication {
        keyedItems: Object = {};
        resourceDictionary: ResourceDictionary = new ResourceDictionary();

        constructor(domNode: HTMLElement) {
            super(domNode);
            this.horizontalAlign = HorizontalAlign.STRETCH;
            this.verticalAlign = VerticalAlign.STRETCH;
            var that: Application = this;
            window.onresize = function () {
                that.checkSize();
            };
        }

    }

    export class ResourceDictionary {

        mergedDictionaries: ResourceDictionary[] = [];
        resources: Object = {};

        addResource(key: string, dom: any) {
            if (this.resources[key] !== undefined)
                throw new Error("Resource key conflict: " + key);
            this.resources[key] = dom;
        }

        hasResource(key: string) {
            return (this.resources[key] instanceof Object);
        }

        getResource(key: string): any {
            if (!(this.resources[key] instanceof Object))
                throw new Error("Resource key not found: " + key);
            return this.resources[key];
        }

        /*merge(resourceDictionary: ResourceDictionary): void {
            for (var i in resourceDictionary.resources) {
                if (resourceDictionary.resources.hasOwnProperty(i)) {
                    if (this.resources[i] === undefined)
                        this.addResource(i, resourceDictionary.resources[i]);
                }
            }
        }*/
    }



    export class Filter {

        private _divide: number = 1;

        private _mix: number = 1;

        get mixValue(): number {
            return this._mix;
        }

        get divide(): number {
            return this._divide;
        }

        private mix(a, b, ammount) {
            return a + (b - a) * ammount;
        }
        convolve(matrix: number[], sourceData: ImageData, output: ImageData): ImageData {

            var divide = this.divide;
            var mix = this.mixValue;

            var matrixSize = Math.sqrt(matrix.length) + 0.5 | 0;
            var halfMatrixSize = matrixSize / 2 | 0;
            var src = sourceData.data;
            var sw = sourceData.width;
            var sh = sourceData.height;
            var w = sw;
            var h = sh;
            var dst = output.data;
            //var alphaFac = opaque ? 1 : 0;
            for (var y = 1; y < h - 1; y++) {
                for (var x = 1; x < w - 1; x++) {

                    var dstOff = (y * w + x) * 4;
                    var r = 0,
                        g = 0,
                        b = 0,
                        a = 0;
                    for (var cy = 0; cy < matrixSize; cy++) {
                        for (var cx = 0; cx < matrixSize; cx++) {
                            var scy = y + cy - halfMatrixSize;
                            var scx = x + cx - halfMatrixSize;
                            if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
                                var srcOff = (scy * sw + scx) * 4;
                                var wt = matrix[cy * matrixSize + cx] / divide;
                                r += src[srcOff + 0] * wt;
                                g += src[srcOff + 1] * wt;
                                b += src[srcOff + 2] * wt;
                                a += src[srcOff + 3] * wt;
                            }
                        }
                    }
                    dst[dstOff + 0] = this.mix(src[dstOff + 0], r, mix);
                    dst[dstOff + 1] = this.mix(src[dstOff + 1], g, mix);
                    dst[dstOff + 2] = this.mix(src[dstOff + 2], b, mix);
                    dst[dstOff + 3] = this.mix(src[dstOff + 3], a, mix);
                }
            }


            return output;
        }

        applyTo(sourceData: ImageData, output: ImageData): ImageData {
            return sourceData;
        }
    }

    export class GaussianBlur extends Filter {
        applyTo(sourceData: ImageData, output: ImageData): ImageData {
            return this.convolve([0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067, 0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292, 0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117, 0.00038771, 0.01330373, 0.11098164, 0.22508352, 0.11098164, 0.01330373, 0.00038771, 0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117, 0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292, 0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067], sourceData, output);
        }
    }

    export class DrawableComponent extends DisplayObject {
        private canvas: HTMLCanvasElement;

        filters: collections.List = new collections.List();

        constructor(htmlElement: HTMLElement) {
            super(htmlElement);
            this.canvas = <HTMLCanvasElement>document.createElement('canvas');
            this.canvas.style.width = "100%";
            this.canvas.style.height = "100%";
        }

        private drawInvalid: boolean = true;

        invalidateDraw() {
            this.drawInvalid = true;
            this.invalidateProperties();
        }

        addToDisplayList() {
            super.addToDisplayList();
            
            this.invalidateDraw();
        }

        commitProperties() {
            super.commitProperties();
            if (this.drawInvalid) {
                if (this.canvas.parentElement !== this.domElement) {
                    this.domElement.appendChild(this.canvas);
                    this.invalidateProperties();
                    return;
                }
                this.drawInvalid = false;
                this.canvas.width = this.measuredWidth;
                this.canvas.height = this.measuredHeight;
                if(this.addedToDisplayList) {
                    this.draw(this.measuredWidth, this.measuredHeight, this.canvas);
                    if (this.filters.length > 0) {
                        var sourceData = this.canvas.getContext('2d').getImageData(0, 0, this.canvas.width, this.canvas.height);
                        for (var i: number = 0; i < this.filters.length; i++) {
                            var output: ImageData = this.canvas.getContext('2d').createImageData(this.canvas.width, this.canvas.height);
                            (<Filter>this.filters.getItemAt(i)).applyTo(sourceData, output);
                            sourceData = output;
                        }
                        this.canvas.getContext('2d').putImageData(sourceData, 0, 0);
                    }
                }

            }
        }

        draw(newWidth: number, newHeight: number, canvas: HTMLCanvasElement) {
            //Stub for drawing
            var ctx: CanvasRenderingContext2D = canvas.getContext("2d");
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, newWidth - 0, newHeight - 0);

        }

        updateSize(newWidth: number, newHeight: number) {
            var oldMWidth = this.measuredWidth;
            var oldMHeight = this.measuredHeight;
            super.updateSize(newWidth, newHeight);
            this.invalidateDraw();
        }


        getDrawSource(): any {
            return this.canvas;
        }
    }

    export class Label extends DisplayObject {
        private _text = "";
        private textChanged: boolean = false;
        public set text(value: string) {
            if (this._text == value) return;
            var old = this._text;
            this._text = value;
            this.textChanged = true;
            this.invalidateProperties();
            this.notifyPropertyChanged("text", old, value);
        }
        public get text(): string {
            return this._text;
        }
        public commitProperties() {
            super.commitProperties();
            if (this.textChanged === true) {
                this.textChanged = false;
                this.domElement.innerHTML = this.text;
            }
        }
    }

    export class ImageBox extends DisplayObject {
        private _src: string;
        private _size: string = "contain";
        private _positionX = "center";
        private _positionY = "center";
        private _repeat = "no-repeat";
        private _resizeStrategy = "no-resize";
        private imageSrc;
        private imageFile: HTMLImageElement;

        public set resizeStrategy(value: string) {
            if (this._resizeStrategy === value) return;
            var old = this._resizeStrategy;
            this._resizeStrategy = value;
            this.notifyPropertyChanged("resizeStrategy", old, value);
            this.renderImageSrc();
        }

        constructor(element:HTMLElement) {
            super(element);
            this.imageFile = document.createElement("img");
            var that = this;
            this.imageFile.onload = function () {
                that.renderImageSrc();
            }
            this.attach(this);
        }

        public get resizeStrategy():string {
            return this._resizeStrategy;
        }

        public get positionX(): string {
            return this._positionX;
        }

        public get positionY(): string {
            return this._positionY;
        }

        public get repeat(): string {
            return this._repeat;
        }

        public get src(): string {
            return this._src;
        }

        public get size(): string {
            return this._size;
        }

        private renderImageSrc() {
            if (this.imageFile.complete) {
                //resize image
                //Determinate width height
                var width;
                var height;
                if (this.resizeStrategy === "no-resize") {
                    width = this.imageFile.width;
                    height = this.imageFile.height;
                } else if(this.resizeStrategy === "width") {
                    width = this.measuredWidth;
                    var ratio = this.measuredWidth / this.imageFile.width;
                    height = ratio * this.imageFile.height;
                } else {
                    height = this.measuredHeight;
                    var ratio = this.measuredHeight / this.imageFile.height;
                    width = ratio * this.imageFile.width;
                }
                console.log(this.src,this.resizeStrategy,this.measuredHeight,width,height);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(this.imageFile, 0, 0, width, height);
                //document.body.appendChild(canvas);
                this.imageSrc = canvas.toDataURL();
                this.srcChanged = true;
                this.invalidateProperties();
            }
        }

        private sizeChanged = true;

        private srcChanged = true;

        private positionChanged = true;

        public set src(value: string) {
            if (this._src == value) return;
            var old = this._src;
            this._src = value;
            this.imageFile.src = value;
            this.notifyPropertyChanged("src", old, value);
        }

        public set positionX(value: string) {
            if (this._positionX == value) return;
            var old = this._positionX;
            this._positionX = value;
            this.notifyPropertyChanged("positionX", old, value);
            this.positionChanged = true;
            this.invalidateProperties();
        }

        public set positionY(value: string) {
            if (this._positionY == value) return;
            var old = this._positionY;
            this._positionY = value;
            this.notifyPropertyChanged("positionY", old, value);
            this.positionChanged = true;
            this.invalidateProperties();
        }

        public set repeat(value: string) {
            if (this._repeat == value) return;
            var old = this._repeat;
            this._repeat = value;
            this.notifyPropertyChanged("repeat", old, value);
            this.sizeChanged = true;
            this.invalidateProperties();
        }

        public set size(value: string) {
            if (this._size == value) return;
            var old = this._size;
            this._size= value;
            this.notifyPropertyChanged("size", old, value);
            this.sizeChanged = true;
            this.invalidateProperties();
        }

        public update(item: core.IPropertyChangeNotifier, property: string, old: any, newVal: any) {
            super.update(item,property,old,newVal);
            if (item === this) {
                if (property === "measuredHeight" && this.resizeStrategy === "height") this.renderImageSrc();
                if (property === "measuredWidth" && this.resizeStrategy === "width") this.renderImageSrc();
            }
        }

        public commitProperties() {
            super.commitProperties();
            if (this.srcChanged === true) {
                this.srcChanged = false;
                
                this.domElement.style.backgroundImage = "url(" + this.imageSrc + ")";
            }
            if (this.positionChanged) {
                var positionX = this.positionX;
                if (!isNaN(parseInt(positionX))) {
                    positionX = parseInt(positionX) + "px";
                }
                var positionY = this.positionY;
                if (!isNaN(parseInt(positionY))) {
                    positionY = parseInt(positionY) + "px";
                }
                this.domElement.style.backgroundPosition = positionX + " " + positionY;
            }
            if (this.sizeChanged === true) {
                this.sizeChanged = false;
                this.domElement.style.backgroundSize = this.size;
                this.domElement.style.backgroundRepeat = this.repeat;
            }
        }
    }