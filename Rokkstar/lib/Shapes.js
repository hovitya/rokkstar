﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./Base", "./ComponentBase"], function(require, exports, __core__, __components__) {
    ///<reference path='ComponentBase.ts'/>
    var core = __core__;
    
    var components = __components__;
    var Shape = (function (_super) {
        __extends(Shape, _super);
        function Shape(elem) {
            _super.call(this, elem);
            //private _borderColor: core.Color;
            //private _borderWidth: number;
            this._invalidateDraw = null;
            var that = this;
            this._invalidateDraw = function (event) {
                that.invalidateDraw();
            };
        }

        Object.defineProperty(Shape.prototype, "background", {
            get: function () {
                return this._background;
            },
            set: function (value) {
                var old = this.background;
                if (old !== null && old !== undefined) {
                    old.removeEventListener(components.BackgroundBrushEvent.UPDATED, this._invalidateDraw);
                }
                if (typeof value !== "object") {
                    this._background = components.BackgroundParser.parseBackgroundString(value);
                } else {
                    this._background = value;
                }
                this._background.addEventListener(components.BackgroundBrushEvent.UPDATED, this._invalidateDraw);
                this.notifyPropertyChanged("background", old, this.background);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });
        return Shape;
    })(components.DrawableComponent);
    exports.Shape = Shape;

    var Ellipse = (function (_super) {
        __extends(Ellipse, _super);
        function Ellipse() {
            _super.apply(this, arguments);
        }
        Ellipse.prototype.draw = function (w, h, canvas) {
            var x = 0;
            var y = 0;

            var graphics = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            var kappa = 0.5522848, ox = (w / 2) * kappa, oy = (h / 2) * kappa, xe = x + w, ye = y + h, xm = x + w / 2, ym = y + h / 2;

            graphics.beginPath();
            graphics.moveTo(x, ym);
            graphics.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
            graphics.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
            graphics.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
            graphics.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
            graphics.closePath();
            graphics.fill();
        };
        return Ellipse;
    })(Shape);
    exports.Ellipse = Ellipse;

    var Rectangle = (function (_super) {
        __extends(Rectangle, _super);
        function Rectangle() {
            _super.apply(this, arguments);
            this._corner = 0;
            this._topLeftCorner = undefined;
            this._topRightCorner = undefined;
            this._bottomLeftCorner = undefined;
            this._bottomRightCorner = undefined;
        }
        Object.defineProperty(Rectangle.prototype, "corner", {
            get: function () {
                return this._corner;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                var old = this.corner;
                this._corner = value;
                this.notifyPropertyChanged("corner", old, value);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Rectangle.prototype, "topLeftCorner", {
            get: function () {
                return this._topLeftCorner;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                var old = this.corner;
                if (value === old)
                    return;
                this._topLeftCorner = value;
                this.notifyPropertyChanged("topLeftCorner", old, value);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Rectangle.prototype, "topRightCorner", {
            get: function () {
                return this._topRightCorner;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                var old = this.corner;
                if (value === old)
                    return;
                this._topRightCorner = value;
                this.notifyPropertyChanged("topRightCorner", old, value);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Rectangle.prototype, "bottomLeftCorner", {
            get: function () {
                return this._bottomLeftCorner;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                var old = this.corner;
                if (value === old)
                    return;
                this._bottomLeftCorner = value;
                this.notifyPropertyChanged("bottomLeftCorner", old, value);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Rectangle.prototype, "bottomRightCorner", {
            get: function () {
                return this._bottomRightCorner;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                var old = this.corner;
                if (value === old)
                    return;
                this._bottomRightCorner = value;
                this.notifyPropertyChanged("bottomRightCorner", old, value);
                this.invalidateDraw();
            },
            enumerable: true,
            configurable: true
        });


        Rectangle.prototype.draw = function (w, h, canvas) {
            var x = 0;
            var y = 0;

            var topLeftCorner = this.topLeftCorner;
            var topRightCorner = this.topRightCorner;
            var bottomLeftCorner = this.bottomLeftCorner;
            var bottomRightCorner = this.bottomRightCorner;
            if (topLeftCorner === undefined)
                topLeftCorner = this.corner;
            if (topRightCorner === undefined)
                topRightCorner = this.corner;
            if (bottomLeftCorner === undefined)
                bottomLeftCorner = this.corner;
            if (bottomRightCorner === undefined)
                bottomRightCorner = this.corner;

            var graphics = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            if (topLeftCorner == 0 && topRightCorner == 0 && bottomLeftCorner == 0 && bottomLeftCorner == 0) {
                graphics.beginPath();
                graphics.rect(x, y, w, h);
                graphics.closePath();
            } else {
                graphics.beginPath();
                graphics.moveTo(x + topLeftCorner, y);
                graphics.lineTo(x + w - topRightCorner, y);
                if (topRightCorner != 0) {
                    graphics.quadraticCurveTo(x + w, y, x + w, y + topRightCorner);
                }
                graphics.lineTo(x + w, y + h - bottomRightCorner);
                if (bottomRightCorner != 0) {
                    graphics.quadraticCurveTo(x + w, y + h, x + w - bottomRightCorner, y + h);
                }
                graphics.lineTo(x + bottomLeftCorner, y + h);
                if (bottomLeftCorner != 0) {
                    graphics.quadraticCurveTo(x, y + h, x, y + h - bottomLeftCorner);
                }
                graphics.lineTo(x, y + topLeftCorner);
                if (topLeftCorner != 0) {
                    graphics.quadraticCurveTo(x, y, x + topLeftCorner, y);
                }
                graphics.closePath();
            }
            graphics.fill();
        };
        return Rectangle;
    })(Shape);
    exports.Rectangle = Rectangle;

    var Text = (function (_super) {
        __extends(Text, _super);
        function Text(elem) {
            _super.call(this, elem);
            this._fontFamily = "Calibri";
            this._fontSize = "12pt";
            this._color = null;
            this._textVerticalAlign = "center";
            this._textHorizontalAlign = "left";
            this._color = new components.Color("black");
        }
        return Text;
    })(Shape);
    exports.Text = Text;
});
//# sourceMappingURL=Shapes.js.map
