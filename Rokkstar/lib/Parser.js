define(["require", "exports", "./Base", "./ComponentBase", "./Events", "./Collections"], function(require, exports, __core__, __components__, __events__, __collections__) {
    ///<reference path='ComponentBase.ts'/>
    var core = __core__;
    var components = __components__;
    var events = __events__;
    var collections = __collections__;

    (function (filters) {
        function Calculate(value, expr) {
            var expression = "var x = " + parseInt(value, 10) + "; return " + expr + ";";
            return new Function(expression)();
        }
        filters.Calculate = Calculate;
    })(exports.filters || (exports.filters = {}));
    var filters = exports.filters;

    function run(modules) {
        return parseApplication(document.getElementsByTagName("body")[0], modules);
    }
    exports.run = run;

    var DelayedProperty = (function () {
        function DelayedProperty() {
            this.type = -1;
        }
        return DelayedProperty;
    })();

    var reservedParameterNames = ["role", "class", "property", "context", "key", "id", "type"];

    function dashToCamel(str) {
        return str.replace(/\W+(.)/g, function (x, chr) {
            return chr.toUpperCase();
        });
    }

    function parseApplication(node, modules) {
        if (typeof modules === "undefined") { modules = {}; }
        var app;

        if (node.getAttribute("data-type") === undefined || node.getAttribute("data-type") === null) {
            //Using default class
            app = new components.Application(node);
        } else {
            var className = node.getAttribute("data-class");
            app = core.ReflectionUtil.ClassFactoryFromString(className);
            core.InterfaceChecker.ensureImplements(app, new core.InterfaceChecker(new components.IIAplication()));
        }

        var delayedProperties = [];
        var idList = {};

        parseChildren(node, app, app, delayedProperties, idList, modules);

        //Parsing properties
        parseProperties(node, app, app, delayedProperties, idList);

        //Clear definition content
        //node.innerHTML = '';
        //Add real content
        //node.appendChild(app.domElement);
        //Apply delayed properties
        applyProperties(delayedProperties, app.resourceDictionary, modules);

        app.addToDisplayList();
        app.dispatchEvent(new events.RokkEvent("parsed"));
        return app;
    }

    function parseResource(dom, context, activeResourceDictionary, modules) {
        var delayedProperties = [];
        var instance = parseInstance(dom, context, delayedProperties, {}, modules, false);
        applyProperties(delayedProperties, activeResourceDictionary, modules);
        return instance;
    }
    exports.parseResource = parseResource;

    function parseInstance(node, context, delayedProperties, idList, modules, parseId) {
        if (typeof parseId === "undefined") { parseId = true; }
        var instance;
        if (node.getAttribute("data-type") === undefined || node.getAttribute("data-type") === null) {
            //Using default class
            instance = new components.HTMLElementWrapper(node);
        } else {
            var className = node.getAttribute("data-type");
            instance = core.ReflectionUtil.ClassFactoryFromString(className, modules, node);
        }
        if (parseId) {
            var key = node.getAttribute("data-id");

            if (key !== undefined && key !== null && key !== "") {
                if (idList[key] !== undefined)
                    throw new Error("Duplicated id: " + key);
                idList[key] = instance;
                context[key] = instance;
            }
        }

        //Parsing properties
        parseProperties(node, instance, context, delayedProperties, idList);

        //Parsing children
        parseChildren(node, instance, context, delayedProperties, idList, modules);

        return instance;
    }

    function applyProperties(properties, activeResourceDictionary, modules) {
        properties = properties.reverse();

        for (var i = 0; i < properties.length; i++) {
            var target = properties[i].target;
            var propertyName = dashToCamel(properties[i].propertyName);
            console.log("apply" + propertyName);

            if (properties[i].type === 0) {
                if (properties[i].stringValue[0] !== "$") {
                    target[propertyName] = properties[i].stringValue;
                } else {
                    var property = properties[i].stringValue.substr(1);
                    var filter = property.split('|');
                    var parts = filter[0].split('.');
                    if (parts.length === 1) {
                        target[propertyName] = properties[i].context[parts[0]];
                    } else {
                        core.DataBindingManager.GetInstance().bind(properties[i].context[parts[0]], parts[1], target, propertyName, filter.slice(1), modules);
                    }
                }
            } else {
                if (target[propertyName] instanceof Array) {
                    //Assign as array
                    target[propertyName] = properties[i].listValue;
                } else if (target[propertyName] !== undefined && target[propertyName] !== null && core.InterfaceChecker.implementsInterface(target[propertyName], new core.InterfaceChecker(new collections.IIList()))) {
                    //Assign list
                    target[propertyName] = new collections.List(properties[i].listValue);
                    /*for (var j: number = 0; j < properties[i].listValue.length; j++) {
                    target[propertyName].push(properties[i].listValue[j]);
                    }*/
                } else {
                    if (properties[i].listValue.length > 1)
                        throw new Error("Trying to add multiple value to " + target + " property " + propertyName + " but it supports only one.");
                    target[propertyName] = properties[i].listValue[0];
                }
            }
        }
    }

    function parseResources(node, context, delayedProperties, idList, modules) {
        var dict = new components.ResourceDictionary();
        for (var i = 0; i < node.childElementCount; i++) {
            var node = node.children[i];
            var key = node.getAttribute("data-key");
            if (key === "" || key === undefined || key === null) {
                throw new Error("Every resource item requires key.");
            }

            dict.addResource(key, parseInstance(node, context, delayedProperties, idList, modules));
        }
        return dict;
    }

    function parseProperties(node, object, context, delayedProperties, idList) {
        for (var i = 0; i < node.attributes.length; i++) {
            if (node.attributes[i].name.search("data-when-") === 0) {
                var eventName = node.attributes[i].name.replace("data-when-", "");
                eventName = dashToCamel(eventName);
                var func = core.ReflectionUtil.ObjectFactory(node.attributes[i].value, context);
                if ((object).addEventListener !== undefined) {
                    (object).addEventListener(eventName, function (e) {
                        func.apply(context, [e]);
                    });
                } else {
                    core.DevConsole.warn("Trying to register event (" + eventName + ") listener for a non event dispatcher.");
                }
            } else if (node.attributes[i].name.search("data-") === 0) {
                var parameterName = node.attributes[i].name.substring(5);
                var parameterName = node.attributes[i].name.replace("data-", "");
                if (reservedParameterNames.indexOf(parameterName) === -1) {
                    var delayedProperty = new DelayedProperty();
                    delayedProperty.context = context;
                    delayedProperty.idList = idList;
                    delayedProperty.target = object;
                    delayedProperty.type = 0;
                    delayedProperty.propertyName = parameterName;
                    delayedProperty.stringValue = node.attributes[i].value;
                    delayedProperties.push(delayedProperty);
                }
            }
        }
    }

    function parseSetter(node, object, context, delayedProperties, idList, modules) {
        var delayedProperty = new DelayedProperty();
        delayedProperty.context = context;
        delayedProperty.idList = idList;
        delayedProperty.target = object;
        delayedProperty.propertyName = node.getAttribute("data-property");
        if (delayedProperty.propertyName === null || delayedProperty.propertyName === undefined || delayedProperty.propertyName === "") {
            throw new Error("Invalid property name.");
        }
        if (node.children.length === 0) {
            delayedProperty.type = 0;
            delayedProperty.stringValue = node.textContent;
        } else if (node.children.length === 1) {
            delayedProperty.type = 1;
            delayedProperty.listValue = [];
            delayedProperty.listValue.push(parseInstance(node.children[0], context, delayedProperties, idList, modules));
        } else if (node.children.length > 1) {
            delayedProperty.type = 2;
            delayedProperty.listValue = [];
            for (var i = 0; i < node.childElementCount; i++) {
                delayedProperty.listValue.push(parseInstance(node.children[i], context, delayedProperties, idList, modules));
            }
        }
        delayedProperties.push(delayedProperty);
    }

    function parseScript(script, context, modules) {
        var moduleNames = "";
        for (var i in modules) {
            if (modules.hasOwnProperty(i)) {
                moduleNames += "var " + i + " = modules['" + i + "']; ";
            }
        }
        var func = eval("(function() { var func = function(modules) { " + moduleNames + script + "}; return func; }())");
        func.apply(context, [modules]);
    }

    function parseChildren(node, parentObject, context, delayedProperties, idList, modules) {
        for (var i = 0; i < node.children.length; i++) {
            var childNode = node.children[i];
            var role = childNode.getAttribute("data-role");
            if (role === undefined || role === null || role === "") {
                role = "instance";
                if (childNode.nodeName.toLowerCase() === "script" && childNode.getAttribute("type") === "text/rokk") {
                    role = "script";
                }
            }

            switch (role) {
                case "script":
                    parseScript(childNode.innerHTML, context, modules);
                    break;
                case "declaration":
                    var instance = parseInstance(childNode, context, delayedProperties, idList, modules);
                    break;
                case "instance":
                    var instance = parseInstance(childNode, context, delayedProperties, idList, modules);
                    core.InterfaceChecker.ensureImplements(parentObject, new core.InterfaceChecker(new components.IIDisplayContainer()));
                    core.InterfaceChecker.ensureImplements(instance, new core.InterfaceChecker(new components.IIDisplayObject()));
                    (parentObject).addChild(instance);
                    break;
                case "setter":
                    parseSetter(childNode, parentObject, context, delayedProperties, idList, modules);
                    break;
                case "resources":
                    if (core.InterfaceChecker.implementsInterface(parentObject, new core.InterfaceChecker(new components.IIAplication()))) {
                        var resourceDict = parseResources(childNode, context, delayedProperties, idList, modules);
                        var app = parentObject;
                        app.resourceDictionary = resourceDict;
                    } else {
                        throw new Error("Resource tag is not allowed here.");
                    }

                    break;
                case "template":
                    throw new Error("Template tag is not allowed here.");
                    break;
            }
        }
    }
});
//# sourceMappingURL=Parser.js.map
