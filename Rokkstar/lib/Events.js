define(["require", "exports"], function(require, exports) {
    
    var EventPhase = (function () {
        function EventPhase() {
        }
        EventPhase.CAPTURE_PHASE = 0;

        EventPhase.AT_TARGET = 1;

        EventPhase.BUBBLING_PHASE = 2;
        return EventPhase;
    })();
    exports.EventPhase = EventPhase;
    ;

    var RokkEvent = (function () {
        function RokkEvent(type, bubbles, cancelable) {
            if (typeof bubbles === "undefined") { bubbles = false; }
            if (typeof cancelable === "undefined") { cancelable = false; }
            this.eventPhase = 0;
            this.isPropagating = true;
            this.isImmediatePropagating = true;
            this.isCancelled = true;
            this.type = type;
            this.bubbles = bubbles;
            this.cancelable = cancelable;
        }
        RokkEvent.prototype.stopPropagation = function () {
            this.isPropagating = false;
        };

        RokkEvent.prototype.stopImmediatePropagation = function () {
            this.isImmediatePropagating = false;
        };

        RokkEvent.prototype.preventDefault = function () {
            if (this.cancelable) {
                this.isCancelled = true;
            }
        };

        RokkEvent.prototype.isDefaultPrevented = function () {
            if (!this.cancelable) {
                return false;
            }
            return this.isCancelled;
        };

        RokkEvent.prototype.serialize = function () {
            return { type: this.type, bubbles: this.bubbles, cancelable: this.cancelable, data: this.data };
        };

        RokkEvent.unSerialize = function (data) {
            var event = new RokkEvent(data.type, data.bubbles, data.cancelable);
            for (var i in data) {
                if (data.hasOwnProperty(i) && i !== "type" && i !== "bubbles" && i !== "cancelable") {
                    event[i] = data[i];
                }
            }
            return event;
        };
        return RokkEvent;
    })();
    exports.RokkEvent = RokkEvent;

    var IIEventDispatcher = (function () {
        function IIEventDispatcher() {
            this.className = "IIEventDispatcher";
            this.methodNames = ["dispatchEvent", "addEventListener", "removeEventListener", "getPropagationParent", "executeHandlers"];
            this.inheritsFrom = [];
        }
        return IIEventDispatcher;
    })();
    exports.IIEventDispatcher = IIEventDispatcher;

    var EventListenerDescription = (function () {
        function EventListenerDescription() {
            this.once = false;
            this.useCapture = false;
        }
        return EventListenerDescription;
    })();
    exports.EventListenerDescription = EventListenerDescription;

    var EventDispatcher = (function () {
        function EventDispatcher() {
            this.handlers = {};
        }
        EventDispatcher.prototype.dispatchEvent = function (event) {
            event.target = this;

            //Collect activation list
            var lastParent = this.getPropagationParent(), activationList = [], j = 0;
            while (lastParent !== null && lastParent !== undefined) {
                activationList.push(lastParent);
                lastParent = lastParent.getPropagationParent();
            }

            //Capture phase
            event.eventPhase = EventPhase.CAPTURE_PHASE;
            j = activationList.length;
            while (j > 0) {
                j = j - 1;
                activationList[j].executeHandlers(event);
                if (!event.isImmediatePropagating || !event.isPropagating) {
                    break;
                }
            }

            if (event.isImmediatePropagating && event.isPropagating) {
                event.eventPhase = EventPhase.AT_TARGET;
                this.executeHandlers(event);
            }

            if (event.bubbles && event.isImmediatePropagating && event.isPropagating) {
                event.eventPhase = EventPhase.BUBBLING_PHASE;
                j = 0;
                while (j < activationList.length) {
                    activationList[j].executeHandlers(event);
                    if (!event.isImmediatePropagating || !event.isPropagating) {
                        break;
                    }
                    j = j + 1;
                }
            }
        };

        EventDispatcher.prototype.getPropagationParent = function () {
            return null;
        };

        EventDispatcher.prototype.addEventListener = function (eventType, listener, once, useCapture) {
            if (typeof once === "undefined") { once = false; }
            if (typeof useCapture === "undefined") { useCapture = false; }
            if (this.handlers[eventType] == undefined) {
                this.handlers[eventType] = new Array();
            }
            var handler = new EventListenerDescription();
            handler.listenerFunction = listener;
            handler.once = once;
            handler.useCapture = useCapture;
            this.handlers[eventType].push(handler);
        };

        EventDispatcher.prototype.removeEventListener = function (eventType, listener) {
            var needToRemove = new Array();
            for (var i in this.handlers[eventType]) {
                if (this.handlers[eventType][i].listenerFunction === listener) {
                    needToRemove.push(i);
                }
            }
            var reversed = needToRemove.reverse();
            for (var i in reversed) {
                this.handlers[eventType].splice(reversed[i], 1);
            }
        };

        EventDispatcher.prototype.executeHandlers = function (event) {
            var handlers = this.handlers[event.type], remove = [], i;
            event.currentTarget = this;
            if (handlers !== undefined && handlers !== null) {
                i = handlers.length;
                while (--i >= 0) {
                    if (event.eventPhase === EventPhase.CAPTURE_PHASE && handlers[i].useCapture) {
                        handlers[i].listenerFunction(event);
                    } else if (!handlers[i].useCapture && (event.eventPhase === EventPhase.BUBBLING_PHASE || event.eventPhase === EventPhase.AT_TARGET)) {
                        handlers[i].listenerFunction(event);
                    }
                    if (handlers[i].once) {
                        remove.push(handlers[i]);
                    }
                    if (!event.isImmediatePropagating) {
                        break;
                    }
                }
                i = remove.length;
                while (--i >= 0) {
                    this.handlers[event.type].splice(this.handlers[event.type].indexOf(remove[i]), 1);
                }
            }
        };
        return EventDispatcher;
    })();
    exports.EventDispatcher = EventDispatcher;
});
//# sourceMappingURL=Events.js.map
