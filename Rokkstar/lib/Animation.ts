﻿///<reference path='Base.ts'/>
///<reference path='Events.ts'/>
///<reference path='ComponentBase.ts'/>
///<reference path='../third-party/definitions/greensock.d.ts'/>
import core = require("./Base");
import components = require("./ComponentBase");
import events = require("./Events");

export interface TimelineItem extends core.IPropertyChangeNotifier {
    toGSAP(): any;
    position: string;
}


export class AnimationAbstract extends events.EventDispatcher implements core.IPropertyChangeNotifier, TimelineItem {
    objectId: number;

    private observers: core.IPropertyObserver[] = [];

    attach(observer: core.IPropertyObserver) {  
        this.observers.push(observer);
    }

    detach(observer: core.IPropertyObserver) { 
        if (this.observers.indexOf(observer) !== -1) {
            this.observers.splice(this.observers.indexOf(observer), 1);
        }
    }

    notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
        for (var i = 0; i < this.observers.length; i++) { 
            this.observers[i].update(this, property, oldValue, newValue);
        }
    }

    private _position: string = "+=0";

    public set position(val: string) {
        if (this._position === val) return;
        var old = this._position;
        this._position = val;
        this.notifyPropertyChanged("position", old, val);
    }

    public get position(): string {
        return this._position;
    }


    public set delay(val: number) {
        this.toAnimation().delay(core.PrimitiveTypeTools.ensureFloat(val));
    }

    public get delay(): number {
        return this.toAnimation().delay();
    }

    public toGSAP():any {
        return [];
    }

    public toAnimation():Animation {
        return this.toGSAP(); 
    }

    public set duration(val: number) {
        this.toAnimation().duration(core.PrimitiveTypeTools.ensureFloat(val));
    }
     
    public get duration(): number {
        return this.toAnimation().duration();
    }

    public set yoyo(val:boolean) {
        val = core.PrimitiveTypeTools.ensureBool(val);
        this.toGSAP().yoyo(val);
    }

    public get yoyo(): boolean {
        return this.toGSAP().yoyo();
    }

    public set timeScale(val: number) {
        val = core.PrimitiveTypeTools.ensureFloat(val);
        this.toGSAP().timeScale(val);
    }

    public get timeScale(): number {
        return this.toGSAP().timeScale();
    }

    public play(from?: number) {
        if (from === undefined) {
            this.toAnimation().play();
        } else {
            this.toAnimation().play(from);
        }
        
    }

    public seek(time: number) {
            this.toAnimation().seek(time);
    }

    public reverse(from?: number) {
        if (from === undefined) {
            this.toAnimation().reverse();
        } else {
            this.toAnimation().reverse(from);
        }     
    }

    public resume(from?: number) {
        if (from === undefined) {
            this.toAnimation().resume();
        } else {
            this.toAnimation().resume(from);
        }
    }

    public pause(atTime?: number) {
        if (atTime === undefined) {
            this.toAnimation().pause();
        } else {
            this.toAnimation().pause(atTime);
        } 
    } 

    public set repeat(value: number) {
        value = core.PrimitiveTypeTools.ensureInteger(value);
        this.toGSAP().repeat(value);
    }

    public get repeat():number {
        return this.toGSAP().repeat();
    }

    public set repeatDelay(value: number) {
        value = core.PrimitiveTypeTools.ensureFloat(value);
        this.toGSAP().repeatDelay(value);
    }

    public get repeatDelay(): number {
        return this.toGSAP().repeatDelay();
    }

    public set reversed(val: boolean) {
        val = core.PrimitiveTypeTools.ensureBool(val);
        this.toAnimation().reversed(val);
    }

    public get reversed(): boolean {
        return this.toAnimation().reversed();
    }


}



export class Tween extends AnimationAbstract {
    private tween = new TweenMax({}, 1, { paused: true, ease: "Linear.easeNone"});

    private _target: any;

    constructor() {
        super();
    }

    public set target(val: any) {
        if (val === this._target) return;
        var old = this._target;
        this._target = val;
        var oldTween = this.tween;
        this.tween = new TweenMax(val, oldTween.duration(), {paused: true, ease:this.easing});
        this.tween.delay(oldTween.delay());  
        this.tween.yoyo(oldTween.yoyo());
        this.tween.timeScale(oldTween.timeScale());
        this.tween.updateTo(oldTween.vars);
        this.tween.repeat(oldTween.repeat());
        this.tween.repeatDelay(oldTween.repeatDelay());
        this.tween.reversed(oldTween.reversed());
        this.notifyPropertyChanged("target", old, val);
    }

    public get target(): any {
        return this._target;
    }

    private _easing: string = "Linear.easeNone";

    public get easing(): string {
        return this._easing;
    }

    public set easing(val: string) {
        if (val === this._easing) return;
        var old = this._easing;
        this._easing = val;
        /*var oldTween = this.tween;
        this.tween = new TweenMax(val, oldTween.duration(), { paused: true, ease: val });
        this.tween.delay(oldTween.delay());
        this.tween.yoyo(oldTween.yoyo());
        this.tween.timeScale(oldTween.timeScale());
        var vars = oldTween.vars =
        this.tween.updateTo(oldTween.vars);
        this.tween.repeat(oldTween.repeat());
        this.tween.repeatDelay(oldTween.repeatDelay());
        this.tween.reversed(oldTween.reversed());*/
        this.tween.updateTo({ease: val});
        this.notifyPropertyChanged("easing", old, val);

    }

    public toGSAP() {
        return this.tween;
    }

    public set vars(val: any) {
        if (typeof val === "string") {
            val = (<string>val).replace(new RegExp("'", 'g'), '"');
            val = JSON.parse(val);
        }
        this.tween.updateTo(val);
    }

    public get vars(): any {
        return this.tween.vars;
    }
    
}

export class Timeline extends AnimationAbstract implements core.IPropertyObserver {
    public timeline: TimelineMax;


    constructor() {
        super();
        if (this.timeline === undefined) {
            this.timeline = new TimelineMax({paused: true})
        }
    }


    private _tweens: TimelineItem[] = [];

    public set tweens(val: TimelineItem[]) {
        for (var i in this._tweens) {
            this._tweens[i].detach(this);
        }
        for (var i in val) {
            val[i].attach(this);
        }
        this._tweens = val; 
        this.refresh();
    }

    public get tweens(): TimelineItem[]{
        return this._tweens;
    }


    public update(item: core.IPropertyChangeNotifier, property: string, newValue: any, oldValue: any) {
        //if (property === "target" || property === "position") {
            this.refresh();
        //}
    }

    public refresh() {
        this.timeline.clear();
        for (var i in this.tweens) {
            //this.timeline.add(this.tweens[i].toGSAP(), this.tweens[i].position);
            if (this.tweens[i] instanceof Tween) {
                var tween: Tween = <Tween>this.tweens[i];
                if (tween.target) {
                    var item = tween.toGSAP();
                    item.paused(false);
                    this.timeline.add(item, tween.position);
                }
            } else {
                this.timeline.add(this.tweens[i].toGSAP(), this.tweens[i].position);
            }

        }
    }

    toGSAP() {
        return this.timeline;
    }


}

export class Parallel implements TimelineItem, core.IPropertyObserver {
    objectId: number;

    private observers: core.IPropertyObserver[] = [];

    attach(observer: core.IPropertyObserver) {
        this.observers.push(observer);
    }

    detach(observer: core.IPropertyObserver) {
        if (this.observers.indexOf(observer) !== -1) {
            this.observers.splice(this.observers.indexOf(observer), 1);
        }
    }

    notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
        for (var i = 0; i < this.observers.length; i++) {
            this.observers[i].update(this, property, oldValue, newValue);
        }
    }

    private _position: string = "+=0";

    public set position(val: string) {
        if (this._position === val) return;
        var old = this._position;
        this._position = val;
        this.notifyPropertyChanged("position", old, val);
    }

    public get position(): string {
        return this._position;
    }

    private _tweens: TimelineItem[] = [];

    public set tweens(val: TimelineItem[]) {
        var old = this.tweens;
        for (var i in this._tweens) {
            this._tweens[i].detach(this);
        }
        for (var i in val) {
            val[i].attach(this);
        }
        this._tweens = val;
        this.notifyPropertyChanged("tweens", old, this._tweens);
    }

    public get tweens(): TimelineItem[] {
        return this._tweens;
    }

    public update(item: core.IPropertyChangeNotifier, property: string, newValue: any, oldValue: any) {
        //if (property === "target" || property === "position") {
        this.notifyPropertyChanged("tweensInner", 0, 1);
        //}
    }


    public toGSAP(): any {
        var tweens = [];
        for (var i in this.tweens) {
            var item = this.tweens[i].toGSAP();
            if (this.tweens[i] instanceof Tween) {
                item.paused(false);
            }
            tweens.push(item);
        }
        return tweens;
    }

}