var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./Base", "./Events", "./Collections"], function(require, exports, __core__, __events__, __collections__) {
    ///<reference path='Events.ts'/>
    ///<reference path='Collections.ts'/>
    ///<reference path='../third-party/definitions/modernizr.d.ts'/>
    var core = __core__;
    var events = __events__;
    var collections = __collections__;
    (function (globals) {
        globals.DOMEvents = ['mouseover', 'mouseout', 'mousemove', 'blur', 'mouseup', 'mousedown', 'touchend', 'touchstart', 'keyup', 'keydown', 'focus', 'onload', 'click'];
        globals.namedColors = {
            'transparent': 'rgba(0, 0, 0, 0)',
            'aliceblue': '#F0F8FF',
            'antiquewhite': '#FAEBD7',
            'aqua': '#00FFFF',
            'aquamarine': '#7FFFD4',
            'azure': '#F0FFFF',
            'beige': '#F5F5DC',
            'bisque': '#FFE4C4',
            'black': '#000000',
            'blanchedalmond': '#FFEBCD',
            'blue': '#0000FF',
            'blueviolet': '#8A2BE2',
            'brown': '#A52A2A',
            'burlywood': '#DEB887',
            'cadetblue': '#5F9EA0',
            'chartreuse': '#7FFF00',
            'chocolate': '#D2691E',
            'coral': '#FF7F50',
            'cornflowerblue': '#6495ED',
            'cornsilk': '#FFF8DC',
            'crimson': '#DC143C',
            'cyan': '#00FFFF',
            'darkblue': '#00008B',
            'darkcyan': '#008B8B',
            'darkgoldenrod': '#B8860B',
            'darkgray': '#A9A9A9',
            'darkgrey': '#A9A9A9',
            'darkgreen': '#006400',
            'darkkhaki': '#BDB76B',
            'darkmagenta': '#8B008B',
            'darkolivegreen': '#556B2F',
            'darkorange': '#FF8C00',
            'darkorchid': '#9932CC',
            'darkred': '#8B0000',
            'darksalmon': '#E9967A',
            'darkseagreen': '#8FBC8F',
            'darkslateblue': '#483D8B',
            'darkslategray': '#2F4F4F',
            'darkslategrey': '#2F4F4F',
            'darkturquoise': '#00CED1',
            'darkviolet': '#9400D3',
            'deeppink': '#FF1493',
            'deepskyblue': '#00BFFF',
            'dimgray': '#696969',
            'dimgrey': '#696969',
            'dodgerblue': '#1E90FF',
            'firebrick': '#B22222',
            'floralwhite': '#FFFAF0',
            'forestgreen': '#228B22',
            'fuchsia': '#FF00FF',
            'gainsboro': '#DCDCDC',
            'ghostwhite': '#F8F8FF',
            'gold': '#FFD700',
            'goldenrod': '#DAA520',
            'gray': '#808080',
            'grey': '#808080',
            'green': '#008000',
            'greenyellow': '#ADFF2F',
            'honeydew': '#F0FFF0',
            'hotpink': '#FF69B4',
            'indianred': '#CD5C5C',
            'indigo': '#4B0082',
            'ivory': '#FFFFF0',
            'khaki': '#F0E68C',
            'lavender': '#E6E6FA',
            'lavenderblush': '#FFF0F5',
            'lawngreen': '#7CFC00',
            'lemonchiffon': '#FFFACD',
            'lightblue': '#ADD8E6',
            'lightcoral': '#F08080',
            'lightcyan': '#E0FFFF',
            'lightgoldenrodyellow': '#FAFAD2',
            'lightgray': '#D3D3D3',
            'lightgrey': '#D3D3D3',
            'lightgreen': '#90EE90',
            'lightpink': '#FFB6C1',
            'lightsalmon': '#FFA07A',
            'lightseagreen': '#20B2AA',
            'lightskyblue': '#87CEFA',
            'lightslategray': '#778899',
            'lightslategrey': '#778899',
            'lightsteelblue': '#B0C4DE',
            'lightyellow': '#FFFFE0',
            'lime': '#00FF00',
            'limegreen': '#32CD32',
            'linen': '#FAF0E6',
            'magenta': '#FF00FF',
            'maroon': '#800000',
            'mediumaquamarine': '#66CDAA',
            'mediumblue': '#0000CD',
            'mediumorchid': '#BA55D3',
            'mediumpurple': '#9370D8',
            'mediumseagreen': '#3CB371',
            'mediumslateblue': '#7B68EE',
            'mediumspringgreen': '#00FA9A',
            'mediumturquoise': '#48D1CC',
            'mediumvioletred': '#C71585',
            'midnightblue': '#191970',
            'mintcream': '#F5FFFA',
            'mistyrose': '#FFE4E1',
            'moccasin': '#FFE4B5',
            'navajowhite': '#FFDEAD',
            'navy': '#000080',
            'oldlace': '#FDF5E6',
            'olive': '#808000',
            'olivedrab': '#6B8E23',
            'orange': '#FFA500',
            'orangered': '#FF4500',
            'orchid': '#DA70D6',
            'palegoldenrod': '#EEE8AA',
            'palegreen': '#98FB98',
            'paleturquoise': '#AFEEEE',
            'palevioletred': '#D87093',
            'papayawhip': '#FFEFD5',
            'peachpuff': '#FFDAB9',
            'peru': '#CD853F',
            'pink': '#FFC0CB',
            'plum': '#DDA0DD',
            'powderblue': '#B0E0E6',
            'purple': '#800080',
            'red': '#FF0000',
            'rosybrown': '#BC8F8F',
            'royalblue': '#4169E1',
            'saddlebrown': '#8B4513',
            'salmon': '#FA8072',
            'sandybrown': '#F4A460',
            'seagreen': '#2E8B57',
            'seashell': '#FFF5EE',
            'sienna': '#A0522D',
            'silver': '#C0C0C0',
            'skyblue': '#87CEEB',
            'slateblue': '#6A5ACD',
            'slategray': '#708090',
            'slategrey': '#708090',
            'snow': '#FFFAFA',
            'springgreen': '#00FF7F',
            'steelblue': '#4682B4',
            'tan': '#D2B48C',
            'teal': '#008080',
            'thistle': '#D8BFD8',
            'tomato': '#FF6347',
            'turquoise': '#40E0D0',
            'violet': '#EE82EE'
        };
    })(exports.globals || (exports.globals = {}));
    var globals = exports.globals;

    var BackgroundBrushEvent = (function (_super) {
        __extends(BackgroundBrushEvent, _super);
        function BackgroundBrushEvent() {
            _super.apply(this, arguments);
        }
        BackgroundBrushEvent.UPDATED = "updated";
        return BackgroundBrushEvent;
    })(events.RokkEvent);
    exports.BackgroundBrushEvent = BackgroundBrushEvent;

    var IIBackgroundBrush = (function () {
        function IIBackgroundBrush() {
            this.className = "IIBackgroundBrush";
            this.methodNames = ["toBackgroundCSS"];
            this.inheritsFrom = [];
        }
        return IIBackgroundBrush;
    })();
    exports.IIBackgroundBrush = IIBackgroundBrush;

    var IIBorderBrush = (function () {
        function IIBorderBrush() {
            this.className = "IIBoderBrush";
            this.methodNames = ["toBorderCSS"];
            this.inheritsFrom = [];
        }
        return IIBorderBrush;
    })();
    exports.IIBorderBrush = IIBorderBrush;

    var ColorEvent = (function (_super) {
        __extends(ColorEvent, _super);
        function ColorEvent() {
            _super.apply(this, arguments);
        }
        ColorEvent.RGB_UPDATED = 'RGBUpdated';
        ColorEvent.HSL_UPDATED = 'HSLUpdated';
        ColorEvent.HSV_UPDATED = 'HSVUpdated';
        ColorEvent.HEX_UPDATED = 'HexUpdated';
        ColorEvent.INT_UPDATED = 'IntUpdated';
        ColorEvent.UPDATED = 'updated';
        return ColorEvent;
    })(events.RokkEvent);
    exports.ColorEvent = ColorEvent;

    //#region Color
    /*
    * Ported from https://github.com/moagrius/Color-1
    * License Unknown
    */
    var Color = (function (_super) {
        __extends(Color, _super);
        function Color(value) {
            _super.call(this);
            this.propertyChangeNotifier = new core.PropertyChangeNotifier();
            this.isHex = /^#?([0-9a-f]{3}|[0-9a-f]{6})$/i;
            this.isHSL = /^hsla?\((\d{1,3}?),\s*(\d{1,3}%),\s*(\d{1,3}%)(,\s*[01]?\.?\d*)?\)$/;
            this.isRGB = /^rgba?\((\d{1,3}%?),\s*(\d{1,3}%?),\s*(\d{1,3}%?)(,\s*[01]?\.?\d*)?\)$/;
            this.isPercent = /^\d+(\.\d+)*%$/;
            this.hexBit = /([0-9a-f])/gi;
            this.leadHex = /^#/;
            this.matchHSL = /^hsla?\((\d{1,3}),\s*(\d{1,3})%,\s*(\d{1,3})%(,\s*([01]?\.?\d*))?\)$/;
            this.matchRGB = /^rgba?\((\d{1,3}%?),\s*(\d{1,3}%?),\s*(\d{1,3}%?)(,\s*([01]?\.?\d*))?\)$/;
            this._decimal = 0;
            this._hex = '#000000';
            this._red = 0;
            this._green = 0;
            this._blue = 0;
            this._hue = 0;
            this._saturation = 0;
            this._lightness = 0;
            this._brightness = 0;
            this._alpha = 1;
            this._HSL2RGB = function () {
                var r = this._red;
                var g = this._green;
                var b = this._blue;
                var h = this._hue / 360;
                var s = this._saturation / 100;
                var l = this._lightness / 100;
                var q = l < 0.5 ? l * (1 + s) : (l + s - l * s);
                var p = 2 * l - q;
                this._red = this.absround(this.hue2rgb(p, q, h + 1 / 3) * 255);
                this._green = this.absround(this.hue2rgb(p, q, h) * 255);
                this._blue = this.absround(this.hue2rgb(p, q, h - 1 / 3) * 255);
                this.propertyChangeNotifier.notifyPropertyChanged("red", r, this._red);
                this.propertyChangeNotifier.notifyPropertyChanged("green", g, this._green);
                this.propertyChangeNotifier.notifyPropertyChanged("blue", b, this._blue);
            };
            this._HEXUpdated = function () {
                this._prepareUpdate();
                this._HEX2INT();
                this._INT2RGB();
                this._RGB2HSL();
                this._broadcastUpdate();
            };
            this._INTUpdated = function () {
                this._prepareUpdate();
                this._INT2RGB();
                this._RGB2HSL();
                this._INT2HEX();
                this._broadcastUpdate();
            };
            var that = this;
            this.addEventListener(ColorEvent.RGB_UPDATED, function (evt) {
                that._RGBUpdated();
            });
            this.addEventListener(ColorEvent.HEX_UPDATED, function (evt) {
                that._HEXUpdated();
            });
            this.addEventListener(ColorEvent.HSL_UPDATED, function (evt) {
                that._HSLUpdated();
            });
            this.addEventListener(ColorEvent.HSV_UPDATED, function (evt) {
                that._HSVUpdated();
            });
            this.addEventListener(ColorEvent.INT_UPDATED, function (evt) {
                that._INTUpdated();
            });
            if (value !== null)
                this.parse(value);
        }
        Color.prototype.attach = function (observer) {
            this.propertyChangeNotifier.attach(observer);
        };

        Color.prototype.detach = function (observer) {
            this.propertyChangeNotifier.detach(observer);
        };

        Color.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            this.propertyChangeNotifier.notifyPropertyChanged(property, oldValue, newValue);
        };

        Color.prototype.toBackgroundCSS = function () {
            return this.RGBA;
        };

        Color.prototype.absround = function (num) {
            return (0.5 + num) << 0;
        };

        Color.prototype.hue2rgb = function (a, b, c) {
            if (c < 0)
                c += 1;
            if (c > 1)
                c -= 1;
            if (c < 1 / 6)
                return a + (b - a) * 6 * c;
            if (c < 1 / 2)
                return b;
            if (c < 2 / 3)
                return a + (b - a) * (2 / 3 - c) * 6;
            return a;
        };

        Color.prototype.p2v = function (p) {
            return this.isPercent.test(p) ? this.absround(parseInt(p) * 2.55) : p;
        };

        Color.prototype.isNamedColor = function (key) {
            var lc = ('' + key).toLowerCase();
            return globals.namedColors.hasOwnProperty(lc) ? globals.namedColors[lc] : null;
        };

        Color.prototype.parse = function (value) {
            if (typeof value == 'undefined') {
                return this;
            }
            ;
            switch (true) {
                case isFinite(value):
                    this.decimal = value;
                    return this;
                case (value instanceof Color):
                    this.copy(value);
                    return this;
                default:
                    switch (true) {
                        case (globals.namedColors.hasOwnProperty(value)):
                            value = globals.namedColors[value];
                            var stripped = value.replace(this.leadHex, '');
                            this.decimal = parseInt(stripped, 16);
                            return this;
                        case this.isHex.test(value):
                            var stripped = value.replace(this.leadHex, '');
                            if (stripped.length == 3) {
                                stripped = stripped.replace(this.hexBit, '$1$1');
                            }
                            ;
                            this.decimal = parseInt(stripped, 16);
                            return this;
                        case this.isRGB.test(value):
                            var parts = value.match(this.matchRGB);
                            this.red = this.p2v(parts[1]);
                            this.green = this.p2v(parts[2]);
                            this.blue = this.p2v(parts[3]);
                            this.alpha = parseFloat(parts[5]) || 1;
                            return this;
                        case this.isHSL.test(value):
                            var parts = value.match(this.matchHSL);
                            this.hue = parseInt(parts[1]);
                            this.saturation = parseInt(parts[2]);
                            this.lightness = parseInt(parts[3]);
                            this.alpha = parseFloat(parts[5]) || 1;
                            return this;
                    }
                    ;
            }
            ;
            return this;
        };

        Color.prototype.clone = function () {
            return new Color(this.decimal);
        };

        Color.prototype.copy = function (color) {
            this.decimal = color.decimal;
            return this;
        };

        Color.prototype.interpolate = function (destination, factor) {
            if (!(destination instanceof Color)) {
                destination = new Color(destination);
            }
            ;
            this._red = this.absround(+(this._red) + (destination._red - this._red) * factor);
            this._green = this.absround(+(this._green) + (destination._green - this._green) * factor);
            this._blue = this.absround(+(this._blue) + (destination._blue - this._blue) * factor);
            this._alpha = this.absround(+(this._alpha) + (destination._alpha - this._alpha) * factor);
            this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED, false, false));
            this.dispatchEvent(new ColorEvent(ColorEvent.UPDATED, false, false));
            return this;
        };

        Color.prototype._RGB2HSL = function () {
            var oh = this._hue;
            var os = this._saturation;
            var ol = this._lightness;
            var ob = this._brightness;
            var r = this._red / 255;
            var g = this._green / 255;
            var b = this._blue / 255;

            var max = Math.max(r, g, b);
            var min = Math.min(r, g, b);
            var l = (max + min) / 2;
            var v = max;

            if (max == min) {
                this._hue = 0;
                this._saturation = 0;
                this._lightness = this.absround(l * 100);
                this._brightness = this.absround(v * 100);
                return;
            }
            ;

            var d = max - min;
            var s = d / ((l <= 0.5) ? (max + min) : (2 - max - min));
            var h = ((max == r) ? (g - b) / d + (g < b ? 6 : 0) : (max == g) ? ((b - r) / d + 2) : ((r - g) / d + 4)) / 6;

            this._hue = this.absround(h * 360);
            this._saturation = this.absround(s * 100);
            this._lightness = this.absround(l * 100);
            this._brightness = this.absround(v * 100);
            this.propertyChangeNotifier.notifyPropertyChanged("hue", oh, this._hue);
            this.propertyChangeNotifier.notifyPropertyChanged("saturation", os, this._saturation);
            this.propertyChangeNotifier.notifyPropertyChanged("brightness", ob, this._brightness);
            this.propertyChangeNotifier.notifyPropertyChanged("lightness", ol, this._lightness);
        };

        Color.prototype._HSV2RGB = function () {
            var or = this._red;
            var og = this._green;
            var ob = this._blue;
            var h = this._hue / 360;
            var s = this._saturation / 100;
            var v = this._brightness / 100;
            var r = 0;
            var g = 0;
            var b = 0;
            var i = Math.floor(h * 6);
            var f = h * 6 - i;
            var p = v * (1 - s);
            var q = v * (1 - f * s);
            var t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0:
                    r = v, g = t, b = p;
                    break;
                case 1:
                    r = q, g = v, b = p;
                    break;
                case 2:
                    r = p, g = v, b = t;
                    break;
                case 3:
                    r = p, g = q, b = v;
                    break;
                case 4:
                    r = t, g = p, b = v;
                    break;
                case 5:
                    r = v, g = p, b = q;
                    break;
            }
            this._red = this.absround(r * 255);
            this._green = this.absround(g * 255);
            this._blue = this.absround(b * 255);
            this.propertyChangeNotifier.notifyPropertyChanged("red", or, this._red);
            this.propertyChangeNotifier.notifyPropertyChanged("green", og, this._green);
            this.propertyChangeNotifier.notifyPropertyChanged("blue", ob, this._blue);
        };

        Color.prototype._INT2HEX = function () {
            var old = this._hex;
            var x = this._decimal.toString(16);
            x = '000000'.substr(0, 6 - x.length) + x;
            this._hex = '#' + x.toUpperCase();
            this.propertyChangeNotifier.notifyPropertyChanged("hex", old, this._hex);
        };

        Color.prototype._INT2RGB = function () {
            var r = this._red;
            var g = this._green;
            var b = this._blue;
            this._red = this._decimal >> 16;
            this._green = (this._decimal >> 8) & 0xFF;
            this._blue = this._decimal & 0xFF;
            this.propertyChangeNotifier.notifyPropertyChanged("red", r, this._red);
            this.propertyChangeNotifier.notifyPropertyChanged("green", g, this._green);
            this.propertyChangeNotifier.notifyPropertyChanged("blue", b, this._blue);
        };

        Color.prototype._HEX2INT = function () {
            var old = this._decimal;
            this._decimal = parseInt(this._hex, 16);
            this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, this._decimal);
        };

        Color.prototype._RGB2INT = function () {
            var old = this._decimal;
            this._decimal = (this._red << 16 | (this._green << 8) & 0xffff | this._blue);
            this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, this._decimal);
        };

        Color.prototype._RGBUpdated = function () {
            this._prepareUpdate();
            this._RGB2INT();
            this._RGB2HSL();
            this._INT2HEX();
            this._broadcastUpdate();
        };
        Color.prototype._HSLUpdated = function () {
            this._prepareUpdate();
            this._HSL2RGB();
            this._RGB2INT();
            this._INT2HEX();
            this._broadcastUpdate();
        };
        Color.prototype._HSVUpdated = function () {
            this._prepareUpdate();
            this._HSV2RGB();
            this._RGB2INT();
            this._INT2HEX();
            this._broadcastUpdate();
        };

        Color.prototype._prepareUpdate = function () {
            this.oldRGB = this.RGB;
            this.oldPRGB = this.PRGB;
            this.oldRGBA = this.RGBA;
            this.oldHSL = this.HSL;
            this.oldHSLA = this.HSLA;
        };

        Color.prototype._broadcastUpdate = function () {
            this.dispatchEvent(new ColorEvent(ColorEvent.UPDATED, false, false));

            this.propertyChangeNotifier.notifyPropertyChanged("RGB", this.oldRGB, this.RGB);
            this.propertyChangeNotifier.notifyPropertyChanged("PRGB", this.oldPRGB, this.PRGB);
            this.propertyChangeNotifier.notifyPropertyChanged("RGBA", this.oldRGBA, this.RGBA);
            this.propertyChangeNotifier.notifyPropertyChanged("HSL", this.oldHSL, this.HSL);
            this.propertyChangeNotifier.notifyPropertyChanged("HSLA", this.oldHSLA, this.HSLA);
        };

        Object.defineProperty(Color.prototype, "decimal", {
            get: function () {
                return this._decimal;
            },
            set: function (value) {
                if (this._decimal === value)
                    return;
                var old = this._decimal;
                this._decimal = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.INT_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("decimal", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "hex", {
            get: function () {
                return this._hex;
            },
            set: function (value) {
                var old = this._hex;
                this._hex = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.HEX_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("hex", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "red", {
            get: function () {
                return this._red;
            },
            set: function (value) {
                var old = this._red;
                this._red = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("red", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "green", {
            get: function () {
                return this._green;
            },
            set: function (value) {
                var old = this._green;
                this._green = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("green", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "blue", {
            get: function () {
                return this._blue;
            },
            set: function (value) {
                var old = this._blue;
                this._blue = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.RGB_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("blue", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "hue", {
            get: function () {
                return this._hue;
            },
            set: function (value) {
                var old = this._hue;
                this._hue = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("hue", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "saturation", {
            get: function () {
                return this._saturation;
            },
            set: function (value) {
                var old = this._saturation;
                this._saturation = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("saturation", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "lightness", {
            get: function () {
                return this._lightness;
            },
            set: function (value) {
                var old = this._lightness;
                this._lightness = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.HSL_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("lightness", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "brightness", {
            get: function () {
                return this._brightness;
            },
            set: function (value) {
                var old = this._brightness;
                this._brightness = value;
                this.dispatchEvent(new ColorEvent(ColorEvent.HSV_UPDATED));
                this.propertyChangeNotifier.notifyPropertyChanged("brightness", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "alpha", {
            get: function () {
                return this._alpha;
            },
            set: function (value) {
                this._prepareUpdate();
                var old = this._alpha;
                this._alpha = value;
                this._broadcastUpdate();
                this.propertyChangeNotifier.notifyPropertyChanged("alpha", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Color.prototype, "RGB", {
            get: function () {
                var components = [this.absround(this._red), this.absround(this._green), this.absround(this._blue)];
                return 'rgb(' + components.join(', ') + ')';
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Color.prototype, "PRGB", {
            get: function () {
                var components = [this.absround(100 * this._red / 255) + '%', this.absround(100 * this._green / 255) + '%', this.absround(100 * this._blue / 255) + '%'];
                return 'rgb(' + components.join(', ') + ')';
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Color.prototype, "RGBA", {
            get: function () {
                var components = [this.absround(this._red), this.absround(this._green), this.absround(this._blue), this._alpha];
                return 'rgba(' + components.join(', ') + ')';
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Color.prototype, "HSL", {
            get: function () {
                var components = [this.absround(this._hue), this.absround(this._saturation) + '%', this.absround(this._lightness) + '%'];
                return 'hsl(' + components.join(', ') + ')';
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Color.prototype, "HSLA", {
            get: function () {
                var components = [this.absround(this._hue), this.absround(this._saturation) + '%', this.absround(this._lightness) + '%', this._alpha];
                return 'hsla(' + components.join(', ') + ')';
            },
            enumerable: true,
            configurable: true
        });

        Color.prototype.setFillColor = function (ctx, x0, y0, x1, y1) {
            ctx.fillStyle = this.toBackgroundCSS();
        };

        Color.prototype.toStringValue = function () {
            return this.hex;
        };
        return Color;
    })(events.EventDispatcher);
    exports.Color = Color;

    //#endregion
    var GradientStep = (function () {
        function GradientStep(color, percent) {
            if (color !== null) {
                if (!(color instanceof Color)) {
                    this.color = new Color(color);
                } else {
                    this.color = color;
                }
            }

            if (percent !== null) {
                this.percent = parseInt(percent);
            }
        }

        Object.defineProperty(GradientStep.prototype, "color", {
            get: function () {
                return this._color;
            },
            set: function (value) {
                if (typeof value !== "object") {
                    this._color = new Color(value);
                } else {
                    this._color = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        return GradientStep;
    })();
    exports.GradientStep = GradientStep;

    var LinearGradientDirection = (function () {
        function LinearGradientDirection() {
        }
        LinearGradientDirection.LEFT = "left";
        LinearGradientDirection.RIGHT = "right";
        LinearGradientDirection.TOP = "top";
        LinearGradientDirection.BOTTOM = "bottom";
        return LinearGradientDirection;
    })();
    exports.LinearGradientDirection = LinearGradientDirection;

    var LinearGradientBrush = (function (_super) {
        __extends(LinearGradientBrush, _super);
        function LinearGradientBrush() {
            _super.call(this);
            this.notifier = new core.PropertyChangeNotifier();
            this._steps = null;
            this._direction = "top";
            var that = this;
            this._updateTrigger = function (evt) {
                that.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
            };
            this.steps = new collections.List();
        }
        Object.defineProperty(LinearGradientBrush.prototype, "steps", {
            get: function () {
                return this._steps;
            },
            set: function (value) {
                if (this._steps !== null && this._steps !== undefined) {
                    this._steps.removeEventListener(collections.CollectionEvent.ITEM_ADDED, this._updateTrigger);
                    this._steps.removeEventListener(collections.CollectionEvent.ITEM_REMOVED, this._updateTrigger);
                    this._steps.removeEventListener(collections.CollectionEvent.REFRESHED, this._updateTrigger);
                }
                this._steps = value;
                this._steps.addEventListener(collections.CollectionEvent.ITEM_ADDED, this._updateTrigger);
                this._steps.addEventListener(collections.CollectionEvent.ITEM_REMOVED, this._updateTrigger);
                this._steps.addEventListener(collections.CollectionEvent.REFRESHED, this._updateTrigger);
                this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(LinearGradientBrush.prototype, "direction", {
            get: function () {
                return this._direction;
            },
            set: function (value) {
                var old = this._direction;
                if (value !== LinearGradientDirection.LEFT && value !== LinearGradientDirection.BOTTOM && value !== LinearGradientDirection.RIGHT && value !== LinearGradientDirection.TOP)
                    throw new Error("Incorrect direction value. Correct values are: left, right, top, bottom.");
                this._direction = value;
                this.notifier.notifyPropertyChanged("direction", old, value);

                this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
            },
            enumerable: true,
            configurable: true
        });



        LinearGradientBrush.prototype.toCSS = function () {
            var css = CrossBrowserCompatibility.FirstSupportedFunctionName("backgroundImage", ["linear-gradient", "-webkit-linear-gradient", "-moz-linear-gradient", "-o-linear-gradient", "-ms-linear-gradient"], "(left,#FFFFFF 0%,#000000 100%)");
            css = css + "(" + this.direction;
            for (var i = 0; i < this.steps.length; i++) {
                var step = this.steps.getItemAt(i);
                css = css + "," + step.color.RGBA + " " + step.percent + "%";
            }
            css = css + ")";
            return css;
        };

        LinearGradientBrush.prototype.toStringValue = function () {
            var css = 'linear-gradient';
            css = css + "(" + this.direction;
            for (var i = 0; i < this.steps.length; i++) {
                var step = this.steps.getItemAt(i);
                css = css + "," + step.color.RGBA + " " + step.percent + "%";
            }
            css = css + ")";
            return css;
        };

        LinearGradientBrush.prototype.toBackgroundCSS = function () {
            return this.toCSS();
        };

        LinearGradientBrush.prototype.attach = function (observer) {
            this.notifier.attach(observer);
        };

        LinearGradientBrush.prototype.detach = function (observer) {
            this.notifier.detach(observer);
        };

        LinearGradientBrush.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            this.notifier.notifyPropertyChanged(property, oldValue, newValue);
        };

        LinearGradientBrush.prototype.setFillColor = function (ctx, x0, y0, x1, y1) {
            var grad;
            switch (this.direction) {
                case LinearGradientDirection.TOP:
                    grad = ctx.createLinearGradient(0, y0, 0, y1);
                    break;
                case LinearGradientDirection.BOTTOM:
                    grad = ctx.createLinearGradient(0, y1, 0, y0);
                    break;
                case LinearGradientDirection.LEFT:
                    grad = ctx.createLinearGradient(x0, 0, x1, 0);
                    break;
                case LinearGradientDirection.RIGHT:
                    grad = ctx.createLinearGradient(x1, 0, x0, 0);
                    break;
                default:
                    throw new Error("Invalid gradient direction: " + this.direction);
            }

            for (var i = 0; i < this.steps.length; i++) {
                grad.addColorStop(this.steps.getItemAt(i).percent / 100.0, this.steps.getItemAt(i).color.RGBA);
                console.log(this.steps.getItemAt(i).percent / 100.0, this.steps.getItemAt(i).color.RGBA);
            }
            ctx.fillStyle = grad;
        };
        return LinearGradientBrush;
    })(events.EventDispatcher);
    exports.LinearGradientBrush = LinearGradientBrush;

    var ImageBrush = (function (_super) {
        __extends(ImageBrush, _super);
        function ImageBrush() {
            _super.apply(this, arguments);
            this.propertyChangeNotifier = new core.PropertyChangeNotifier();
            this._image = null;
        }
        ImageBrush.prototype.attach = function (observer) {
            this.propertyChangeNotifier.attach(observer);
        };

        ImageBrush.prototype.detach = function (observer) {
            this.propertyChangeNotifier.detach(observer);
        };

        ImageBrush.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            this.propertyChangeNotifier.notifyPropertyChanged(property, oldValue, newValue);
        };


        Object.defineProperty(ImageBrush.prototype, "source", {
            get: function () {
                return this._image;
            },
            set: function (val) {
                var old = this._image;
                if (!val.complete) {
                    this._image = val;
                    var that = this;
                    val.onload = function () {
                        that.propertyChangeNotifier.notifyPropertyChanged("source", old, val);
                        that.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
                    };
                } else {
                    this._image = val;
                    this.propertyChangeNotifier.notifyPropertyChanged("source", old, val);
                    this.dispatchEvent(new BackgroundBrushEvent(BackgroundBrushEvent.UPDATED));
                }
            },
            enumerable: true,
            configurable: true
        });

        ImageBrush.prototype.toBackgroundCSS = function () {
            if (this._image != null && this._image.complete) {
                // Create the canvas element.
                var canvas = document.createElement('canvas');
                canvas.width = this._image.width;
                canvas.height = this._image.height;

                // Get '2d' context and draw the image.
                var ctx = canvas.getContext("2d");
                ctx.drawImage(this._image, 0, 0);

                try  {
                    var data = canvas.toDataURL();
                } catch (e) {
                    return "none";
                }
                return "url('" + data + "')";
            } else if (this._image.src != "") {
                return "url('" + this._image.src + "')";
            }
            return "none";
        };

        ImageBrush.prototype.toStringValue = function () {
            return this.toBackgroundCSS();
        };

        ImageBrush.prototype.setFillColor = function (ctx, x0, y0, x1, y1) {
            if (this._image.complete) {
                ctx.drawImage(this._image, 0, 0, this._image.width, this._image.height, x0, y0, x1 - x0, y1 - y0);
            }
        };

        ImageBrush.prototype.isComplete = function () {
            if (this.source !== null && this.source !== undefined)
                return false;
            return this.source.complete;
        };
        return ImageBrush;
    })(events.EventDispatcher);
    exports.ImageBrush = ImageBrush;
    var BackgroundParser = (function () {
        function BackgroundParser() {
        }
        BackgroundParser.parseBackgroundString = function (str) {
            var gradientReg = /^linear-gradient\((left|right|top|bottom),(.*)\)$/, colorPairReg = /([^ ]+) ([0-9]{1,3})%/, urlReg = /^url\(('(.*)'|"(.*)")\)$/;
            if (gradientReg.test(str)) {
                //Linear gradient
                var matches = str.match(gradientReg);
                var gradient = new LinearGradientBrush();
                gradient.direction = matches[1];
                var colors = matches[2].split(',');
                for (var j = 0; j < colors.length; j++) {
                    var cMatches = colors[j].match(colorPairReg);
                    var step = new GradientStep(new Color(cMatches[1]), cMatches[2]);
                    gradient.steps.push(step);
                }
                return gradient;
            } else if (urlReg.test(str)) {
                //Image
                var iMatches = str.match(urlReg);
                var url;
                if (iMatches[2] !== undefined) {
                    url = iMatches[2];
                } else {
                    url = iMatches[3];
                }
                var image = document.createElement("img");
                image.src = url;
                var imageBrush = new ImageBrush();
                imageBrush.source = image;
                return imageBrush;
            }

            //Color
            return new Color(str);
        };

        BackgroundParser.parseImageString = function (str) {
            var urlReg = /^url\(('(.*)'|"(.*)")\)$/;
            if (urlReg.test(str)) {
                //Image
                var iMatches = str.match(urlReg);
                var url;
                if (iMatches[2] !== undefined) {
                    url = iMatches[2];
                } else {
                    url = iMatches[3];
                }
                var image = document.createElement("img");
                image.src = url;
                var imageBrush = new ImageBrush();
                imageBrush.source = image;
                return imageBrush;
            } else {
                throw new Error("Cannot parse image string :" + str);
            }
        };
        return BackgroundParser;
    })();
    exports.BackgroundParser = BackgroundParser;

    var CrossBrowserCompatibility = (function () {
        function CrossBrowserCompatibility() {
        }
        CrossBrowserCompatibility.FirstSupportedFunctionName = function (property, prefixedFunctionNames, argString) {
            var tempDiv = document.createElement("div");
            for (var i = 0; i < prefixedFunctionNames.length; ++i) {
                tempDiv.style[property] = prefixedFunctionNames[i] + argString;
                if (tempDiv.style[property] != "")
                    return prefixedFunctionNames[i];
            }
            return null;
        };
        return CrossBrowserCompatibility;
    })();
    exports.CrossBrowserCompatibility = CrossBrowserCompatibility;

    //#region ScreenRedraw
    var RedrawCallback = (function () {
        function RedrawCallback() {
            this.cancelled = false;
            this.callbackId = 0;
        }
        return RedrawCallback;
    })();

    var ScreenRedrawManager = (function () {
        function ScreenRedrawManager() {
        }
        ScreenRedrawManager.AddCallback = function (callback, scope) {
            var redrawCallback = new RedrawCallback();
            ScreenRedrawManager.currentCallbackId++;
            var cid = ScreenRedrawManager.currentCallbackId;
            redrawCallback.callable = callback;
            redrawCallback.cancelled = false;
            redrawCallback.scope = scope;
            redrawCallback.callbackId = cid;
            ScreenRedrawManager.actualCallbackMap[cid.toString()] = redrawCallback;
            ScreenRedrawManager.callbacks.push(redrawCallback);
            ScreenRedrawManager.ScheduleRedraw();
            return cid;
        };

        ScreenRedrawManager.CancelCallback = function (callbackId) {
            if (ScreenRedrawManager.actualCallbackMap[callbackId.toString()] !== undefined) {
                ScreenRedrawManager.actualCallbackMap[callbackId.toString()].cancelled = true;
                return true;
            }
            return false;
        };

        ScreenRedrawManager.DoRedraw = function () {
            var callbacksCopy = ScreenRedrawManager.callbacks.reverse();
            ScreenRedrawManager.callbacks = [];
            ScreenRedrawManager.redrawScheduled = false;
            while (callbacksCopy.length > 0) {
                var item = callbacksCopy.pop();
                if (!item.cancelled) {
                    item.callable.apply(item.scope, []);
                }
                ScreenRedrawManager.actualCallbackMap[item.callbackId.toString()] = undefined;
            }
        };

        ScreenRedrawManager.ScheduleRedraw = function () {
            if (!ScreenRedrawManager.redrawScheduled) {
                if (Modernizr.prefixed("requestAnimationFrame", window) !== undefined) {
                    try  {
                        Modernizr.prefixed("requestAnimationFrame", window).apply({}, [ScreenRedrawManager.DoRedraw]);
                    } catch (error) {
                        setTimeout(ScreenRedrawManager.DoRedraw, 1000 / 60);
                    }
                } else {
                    setTimeout(ScreenRedrawManager.DoRedraw, 1000 / 60);
                }

                ScreenRedrawManager.redrawScheduled = true;
            }
        };
        ScreenRedrawManager.callbacks = [];
        ScreenRedrawManager.currentCallbackId = 0;
        ScreenRedrawManager.actualCallbackMap = {};
        ScreenRedrawManager.redrawScheduled = false;
        return ScreenRedrawManager;
    })();

    var InteractiveObject = (function (_super) {
        __extends(InteractiveObject, _super);
        function InteractiveObject() {
            _super.call(this);
            this._domElement = null;
            this.registeredDOMEvents = [];
        }
        Object.defineProperty(InteractiveObject.prototype, "domElement", {
            get: function () {
                if (this._domElement === null || this._domElement === undefined) {
                    this._domElement = this.createDOMElement();
                }
                return this._domElement;
            },
            enumerable: true,
            configurable: true
        });

        InteractiveObject.prototype.registerDOMEvent = function (event) {
            if (this.registeredDOMEvents.indexOf(event) === -1) {
                var that = this;
                this.domElement.addEventListener(event, function (event) {
                    if (event.preventDefault)
                        event.preventDefault();
                    var revent = new events.RokkEvent(event.type, event.bubbles, event.cancelable);
                    revent.originalEvent = event;
                    event.stopImmediatePropagation();
                    that.dispatchEvent(revent);
                    return false;
                });
                this.registeredDOMEvents.push(event);
            }
        };

        InteractiveObject.prototype.createDOMElement = function () {
            var div = window.document.createElement('div');
            div.style[Modernizr.prefixed('boxSizing')] = 'border-box';
            div.style.position = 'absolute';
            div.className = "noSelect";
            return div;
        };

        InteractiveObject.prototype.addEventListener = function (eventType, listener, once, useCapture) {
            if (typeof once === "undefined") { once = false; }
            if (typeof useCapture === "undefined") { useCapture = false; }
            if (globals.DOMEvents.indexOf(eventType) !== -1) {
                this.registerDOMEvent(eventType);
            }
            _super.prototype.addEventListener.call(this, eventType, listener, once, useCapture);
        };
        return InteractiveObject;
    })(events.EventDispatcher);
    exports.InteractiveObject = InteractiveObject;

    //#endregion
    var Matrix = (function () {
        function Matrix(a, b, c, d, e, f) {
            this.a = 1;
            this.b = 0;
            this.c = 0;
            this.d = 1;
            this.e = 0;
            this.f = 0;
            this.extract = function () {
                var out = { dx: 0, dy: 0, scalex: 0, scaley: 0, shear: 0, rotate: 0, isSimple: false, isSuperSimple: false, noRotation: false };

                // translation
                out.dx = this.e;
                out.dy = this.f;

                // scale and shear
                var row = [
                    [this.a, this.c],
                    [this.b, this.d]
                ];
                var a = this.a;
                out.scalex = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
                this.normalize(row[0]);

                out.shear = row[0][0] * row[1][0] + row[0][1] * row[1][1];
                row[1] = [row[1][0] - row[0][0] * out.shear, row[1][1] - row[0][1] * out.shear];

                out.scaley = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
                this.normalize(row[1]);
                out.shear /= out.scaley;

                // rotation
                var sin = -row[0][1], cos = row[1][1];
                if (cos < 0) {
                    out.rotate = core.MathTools.toDegree(Math.acos(cos));
                    if (sin < 0) {
                        out.rotate = 360 - out.rotate;
                    }
                } else {
                    out.rotate = core.MathTools.toDegree(Math.asin(sin));
                }

                out.isSimple = !+out.shear.toFixed(9) && (out.scalex.toFixed(9) == out.scaley.toFixed(9) || !out.rotate);
                out.isSuperSimple = !+out.shear.toFixed(9) && out.scalex.toFixed(9) == out.scaley.toFixed(9) && !out.rotate;
                out.noRotation = !+out.shear.toFixed(9) && !out.rotate;
                return out;
            };
            if (a !== null && a !== undefined) {
                this.a = +a;
                this.b = +b;
                this.c = +c;
                this.d = +d;
                this.e = +e;
                this.f = +f;
            }
        }
        Matrix.prototype.add = function (a, b, c, d, e, f) {
            var out = [
                [],
                [],
                []
            ], m = [
                [this.a, this.c, this.e],
                [this.b, this.d, this.f],
                [0, 0, 1]
            ], matrix = [
                [a, c, e],
                [b, d, f],
                [0, 0, 1]
            ], x, y, z, res;

            if (a && a instanceof Matrix) {
                matrix = [
                    [a.a, a.c, a.e],
                    [a.b, a.d, a.f],
                    [0, 0, 1]
                ];
            }

            for (x = 0; x < 3; x++) {
                for (y = 0; y < 3; y++) {
                    res = 0;
                    for (z = 0; z < 3; z++) {
                        res += m[x][z] * matrix[z][y];
                    }
                    out[x][y] = res;
                }
            }
            this.a = out[0][0];
            this.b = out[1][0];
            this.c = out[0][1];
            this.d = out[1][1];
            this.e = out[0][2];
            this.f = out[1][2];
        };

        Matrix.prototype.invert = function () {
            var me = this, x = me.a * me.d - me.b * me.c;
            return new Matrix(me.d / x, -me.b / x, -me.c / x, me.a / x, (me.c * me.f - me.d * me.e) / x, (me.b * me.e - me.a * me.f) / x);
        };

        Matrix.prototype.clone = function () {
            return new Matrix(this.a, this.b, this.c, this.d, this.e, this.f);
        };

        Matrix.prototype.translate = function (x, y) {
            this.add(1, 0, 0, 1, x, y);
        };

        Matrix.prototype.shear = function (x, y, cx, cy) {
            (y === null || y === undefined) && (y = x);
            (cx || cy) && this.add(1, 0, 0, 1, cx, cy);
            this.add(1, x, y, 1, 0, 0);
            (cx || cy) && this.add(1, 0, 0, 1, -cx, -cy);
        };

        Matrix.prototype.scale = function (x, y, cx, cy) {
            (y === null || y === undefined) && (y = x);
            (cx || cy) && this.add(1, 0, 0, 1, cx, cy);
            this.add(x, 0, 0, y, 0, 0);
            (cx || cy) && this.add(1, 0, 0, 1, -cx, -cy);
        };

        Matrix.prototype.rotate = function (a, x, y) {
            a = core.MathTools.toRadians(a);
            x = x || 0;
            y = y || 0;
            var cos = +Math.cos(a).toFixed(9), sin = +Math.sin(a).toFixed(9);
            this.add(cos, sin, -sin, cos, x, y);
            this.add(1, 0, 0, 1, -x, -y);
        };

        Matrix.prototype.x = function (x, y) {
            return x * this.a + y * this.c + this.e;
        };

        Matrix.prototype.y = function (x, y) {
            return x * this.b + y * this.d + this.f;
        };

        Matrix.prototype.toString = function () {
            if (Modernizr.webgl) {
                return "matrix3d(" + [this.a, this.b, 0, 0, this.c, this.d, 0, 0, 0, 0, 1, 0, this.e, this.f, 0, 1].join(',') + ")";
            } else {
                return "matrix(" + [this.a, this.b, this.c, this.d, this.e, this.f].join(',') + ")";
            }
        };

        Matrix.prototype.toFilter = function () {
            return "progid:DXImageTransform.Microsoft.Matrix(M11=" + this.a + ", M12=" + this.c + ", M21=" + this.b + ", M22=" + this.d + ", Dx=" + this.e + ", Dy=" + this.f + ", sizingmethod='auto expand')";
        };

        Matrix.prototype.offset = function () {
            return [this.e.toFixed(4), this.f.toFixed(4)];
        };

        Matrix.prototype.normalize = function (a) {
            var mag = Math.sqrt(a[0] * a[0] + a[1] * a[1]);
            a[0] && (a[0] /= mag);
            a[1] && (a[1] /= mag);
        };
        return Matrix;
    })();
    exports.Matrix = Matrix;

    //#region RenderTransform
    var RenderTransform = (function (_super) {
        __extends(RenderTransform, _super);
        function RenderTransform() {
            _super.apply(this, arguments);
        }
        RenderTransform.prototype.applyTo = function (matrix) {
        };
        return RenderTransform;
    })(core.PropertyChangeNotifier);
    exports.RenderTransform = RenderTransform;

    var RotateTransform = (function (_super) {
        __extends(RotateTransform, _super);
        function RotateTransform() {
            _super.apply(this, arguments);
            this._angle = 0;
            this._x = 0;
            this._y = 0;
        }

        Object.defineProperty(RotateTransform.prototype, "angle", {
            get: function () {
                return this._angle;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._angle;
                if (value === old)
                    return;
                this._angle = value;
                this.notifyPropertyChanged("angle", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(RotateTransform.prototype, "x", {
            get: function () {
                return this._x;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._x;
                if (value === old)
                    return;
                this._x = value;
                this.notifyPropertyChanged("x", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(RotateTransform.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._y;
                if (value === old)
                    return;
                this._y = value;
                this.notifyPropertyChanged("y", old, value);
            },
            enumerable: true,
            configurable: true
        });

        RotateTransform.prototype.applyTo = function (matrix) {
            matrix.rotate(this.angle, this.x, this.y);
        };
        return RotateTransform;
    })(RenderTransform);
    exports.RotateTransform = RotateTransform;

    var ScaleTransform = (function (_super) {
        __extends(ScaleTransform, _super);
        function ScaleTransform() {
            _super.apply(this, arguments);
            this._scaleX = 1.0;
            this._scaleY = 1.0;
            this._x = 0;
            this._y = 0;
        }

        Object.defineProperty(ScaleTransform.prototype, "scaleX", {
            get: function () {
                return this._scaleX;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._scaleX;
                if (value === old)
                    return;
                this._scaleX = value;
                this.notifyPropertyChanged("scaleX", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(ScaleTransform.prototype, "scaleY", {
            get: function () {
                return this._scaleY;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._scaleY;
                if (value === old)
                    return;
                this._scaleY = value;
                this.notifyPropertyChanged("scaleY", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(ScaleTransform.prototype, "x", {
            get: function () {
                return this._x;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._x;
                if (value === old)
                    return;
                this._x = value;
                this.notifyPropertyChanged("x", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(ScaleTransform.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._y;
                if (value === old)
                    return;
                this._y = value;
                this.notifyPropertyChanged("y", old, value);
            },
            enumerable: true,
            configurable: true
        });

        ScaleTransform.prototype.applyTo = function (matrix) {
            matrix.scale(this.scaleX, this.scaleY, this.x, this.y);
        };
        return ScaleTransform;
    })(RenderTransform);
    exports.ScaleTransform = ScaleTransform;

    var SkewTransform = (function (_super) {
        __extends(SkewTransform, _super);
        function SkewTransform() {
            _super.apply(this, arguments);
            this._angleX = 0.0;
            this._angleY = 0.0;
        }

        Object.defineProperty(SkewTransform.prototype, "angleX", {
            get: function () {
                return this._angleX;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._angleX;
                if (value === old)
                    return;
                this._angleX = value;
                this.notifyPropertyChanged("angleX", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(SkewTransform.prototype, "angleY", {
            get: function () {
                return this._angleY;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._angleY;
                if (value !== old)
                    return;
                this._angleY = value;
                this.notifyPropertyChanged("angleY", old, value);
            },
            enumerable: true,
            configurable: true
        });

        SkewTransform.prototype.applyTo = function (matrix) {
            matrix.shear(0, 0, this.angleX, this.angleY);
        };
        return SkewTransform;
    })(RenderTransform);
    exports.SkewTransform = SkewTransform;

    var TranslateTransform = (function (_super) {
        __extends(TranslateTransform, _super);
        function TranslateTransform() {
            _super.apply(this, arguments);
            this._x = 0.0;
            this._y = 0.0;
        }

        Object.defineProperty(TranslateTransform.prototype, "x", {
            get: function () {
                return this._x;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._x;
                if (value === old)
                    return;
                this._x = value;
                this.notifyPropertyChanged("x", old, value);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(TranslateTransform.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                var old = this._y;
                if (value === old)
                    return;
                this._y = value;
                this.notifyPropertyChanged("y", old, value);
            },
            enumerable: true,
            configurable: true
        });

        TranslateTransform.prototype.applyTo = function (matrix) {
            matrix.translate(this.x, this.y);
        };
        return TranslateTransform;
    })(RenderTransform);
    exports.TranslateTransform = TranslateTransform;

    //#endregion
    var HorizontalAlign = (function () {
        function HorizontalAlign() {
        }
        HorizontalAlign.LEFT = "left";
        HorizontalAlign.RIGHT = "right";
        HorizontalAlign.CENTER = "center";
        HorizontalAlign.STRETCH = "stretch";
        return HorizontalAlign;
    })();
    exports.HorizontalAlign = HorizontalAlign;

    var VerticalAlign = (function () {
        function VerticalAlign() {
        }
        VerticalAlign.TOP = "top";
        VerticalAlign.BOTTOM = "bottom";
        VerticalAlign.CENTER = "center";
        VerticalAlign.STRETCH = "stretch";
        return VerticalAlign;
    })();
    exports.VerticalAlign = VerticalAlign;

    var IIDisplayObject = (function () {
        function IIDisplayObject() {
            this.className = "IIDisplayObject";
            this.methodNames = ["checkSize"];
            this.inheritsFrom = [new core.IIPropertyChangeNotifier(), new events.IIEventDispatcher()];
        }
        return IIDisplayObject;
    })();
    exports.IIDisplayObject = IIDisplayObject;

    var DisplayListEvent = (function (_super) {
        __extends(DisplayListEvent, _super);
        function DisplayListEvent() {
            _super.apply(this, arguments);
        }
        DisplayListEvent.ADDED_TO_DISPLAY_LIST = "addedToDisplayList";
        DisplayListEvent.REMOVED_FROM_DISPLAY_LIST = "removedFromDisplayList";
        return DisplayListEvent;
    })(events.RokkEvent);
    exports.DisplayListEvent = DisplayListEvent;

    var DisplayObject = (function (_super) {
        __extends(DisplayObject, _super);
        function DisplayObject(element) {
            _super.call(this);
            this._renderTransformations = new Array();
            this.visibleChanged = false;
            this._visible = true;
            this._alpha = 1.0;
            this.alphaChanged = false;
            this.addedToDisplayList = false;
            this.renderMatrixUpdated = false;
            this._top = 0;
            this._left = 0;
            this._right = 0;
            this._bottom = 0;
            this._width = 100;
            this._height = 100;
            this._gridColumn = 0;
            this._gridRow = 0;
            this._gridRowSpan = 1;
            this._gridColumnSpan = 1;
            this._parent = null;
            this.observers = [];
            this._horizontalAlign = "left";
            this._verticalAlign = "top";
            this._measuredWidth = -1;
            this._measuredHeight = -1;
            this._wrapper = null;
            /*get wrapper(): HTMLElement {
            if (this._wrapper === null) {
            this._wrapper = document.createElement('div');
            this._wrapper.appendChild(this.domElement);
            }
            return this._wrapper;
            }*/
            this.lastPropertyInvalidation = -1;
            this.lastDisplayListInvalidation = -1;
            this.lastSizeInvalidation = -1;
            if (element === null || element === undefined) {
                element = document.createElement('div');
            }
            this._domElement = element;
        }

        Object.defineProperty(DisplayObject.prototype, "htmlClass", {
            get: function () {
                return this._domElement.className;
            },
            set: function (val) {
                if (this._domElement.className === val)
                    return;
                var old = this._domElement.className;
                this._domElement.className = val;
                this.notifyPropertyChanged("htmlClass", old, val);
            },
            enumerable: true,
            configurable: true
        });

        DisplayObject.prototype.getClasses = function () {
            return this._domElement.className.split(" ");
        };

        DisplayObject.prototype.addClass = function (className) {
            var classes = this.getClasses();
            if (classes.indexOf(className) !== -1)
                return;
            classes.push(className);
            this.htmlClass = classes.join(" ");
        };

        DisplayObject.prototype.removeClass = function (className) {
            var classes = this.getClasses();
            if (classes.indexOf(className) === -1)
                return;
            classes.splice(classes.indexOf(className), 1);
            this.htmlClass = classes.join(" ");
        };

        Object.defineProperty(DisplayObject.prototype, "visible", {
            get: function () {
                return this._visible;
            },
            set: function (val) {
                val = core.PrimitiveTypeTools.ensureBool(val);
                if (this._visible === val)
                    return;
                this._visible = val;
                this.visibleChanged = true;
                this.invalidateProperties();
                this.notifyPropertyChanged("visible", !val, val);
            },
            enumerable: true,
            configurable: true
        });



        Object.defineProperty(DisplayObject.prototype, "alpha", {
            get: function () {
                return this._alpha;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureFloat(value);
                if (value === this._alpha)
                    return;
                var old = this._alpha;
                this.alphaChanged = true;
                this._alpha = value;
                this.notifyPropertyChanged("alpha", old, value);
                this.invalidateProperties();
            },
            enumerable: true,
            configurable: true
        });

        DisplayObject.prototype.globalToLocal = function (point) {
            var localPoint = new core.Point(point.x - this.domElement.getBoundingClientRect().left - this.domElement.clientLeft + this.domElement.scrollLeft, point.y - this.domElement.getBoundingClientRect().top - this.domElement.clientTop + this.domElement.scrollTop);
            return localPoint;
        };

        DisplayObject.prototype.addToDisplayList = function () {
            this.addedToDisplayList = true;
            this.invalidateSize();
            this.dispatchEvent(new DisplayListEvent(DisplayListEvent.ADDED_TO_DISPLAY_LIST));
        };

        DisplayObject.prototype.removeFromDisplayList = function () {
            this.addedToDisplayList = false;
            this.dispatchEvent(new DisplayListEvent(DisplayListEvent.REMOVED_FROM_DISPLAY_LIST));
        };

        Object.defineProperty(DisplayObject.prototype, "renderTransformations", {
            get: function () {
                return this._renderTransformations;
            },
            set: function (list) {
                var old = this._renderTransformations;
                if (!(list instanceof Array)) {
                    var item = list;
                    list = new Array();
                    list.push(item);
                }
                if (old !== null && old !== undefined) {
                    for (var i in old) {
                        old[i].detach(this);
                    }
                }
                for (var i in list) {
                    if (!(list[i] instanceof RenderTransform))
                        throw new Error("You can only assign core.RenderTransformation objects to Interactive object renderTransformations property.");
                    list[i].attach(this);
                }
                this._renderTransformations = list;
                this.updateRenderMatrix();
                this.notifyPropertyChanged("renderTransformations", old, this.renderTransformations);
            },
            enumerable: true,
            configurable: true
        });


        DisplayObject.prototype.updateRenderMatrix = function () {
            var m = new Matrix();
            for (var i in this.renderTransformations) {
                this.renderTransformations[i].applyTo(m);
            }
            this.renderMatrix = m;
            this.renderMatrixUpdated = true;
            this.invalidateProperties();
        };

        DisplayObject.prototype.update = function (observable, property, oldValue, newValue) {
            if (this.renderTransformations.indexOf(observable) !== -1) {
                this.updateRenderMatrix();
            }
        };

        Object.defineProperty(DisplayObject.prototype, "domElement", {
            get: function () {
                return this._domElement;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "measuredWidth", {
            get: function () {
                return this._measuredWidth;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "measuredHeight", {
            get: function () {
                return this._measuredHeight;
            },
            enumerable: true,
            configurable: true
        });

        DisplayObject.prototype.invalidateProperties = function () {
            if (this.lastPropertyInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastPropertyInvalidation);
            }
            this.lastPropertyInvalidation = ScreenRedrawManager.AddCallback(this.commitProperties, this);
        };

        DisplayObject.prototype.commitProperties = function () {
            if (this.renderMatrixUpdated) {
                this.domElement.style[Modernizr.prefixed("transform")] = this.renderMatrix.toString();
                this.renderMatrixUpdated = false;
            }
            if (this.alphaChanged) {
                this.alphaChanged = false;
                this.domElement.style.opacity = this.alpha.toString();
                if (this.alpha <= 0.0) {
                    this.domElement.style.display = "none";
                } else if (this.visible && this.domElement.style.display === "none") {
                    this.domElement.style.display = "block";
                    if (this.measuredHeight === -1)
                        this.invalidateSize();
                }
            }

            if (this.visibleChanged) {
                this.visibleChanged = false;
                if (this.visible) {
                    if (this.alpha <= 0.0) {
                        this.domElement.style.display = "none";
                    } else {
                        this.domElement.style.display = "block";
                        if (this.measuredHeight === -1)
                            this.invalidateSize();
                    }
                } else {
                    this.domElement.style.display = "none";
                }
            }
        };

        DisplayObject.prototype.invalidateDisplayList = function () {
            if (this.lastDisplayListInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastDisplayListInvalidation);
            }
            this.lastDisplayListInvalidation = ScreenRedrawManager.AddCallback(this.updateDisplayList, this);
        };

        DisplayObject.prototype.invalidateSize = function () {
            if (this.lastSizeInvalidation !== -1) {
                ScreenRedrawManager.CancelCallback(this.lastSizeInvalidation);
            }
            this.lastSizeInvalidation = ScreenRedrawManager.AddCallback(this.checkSize, this);
        };

        DisplayObject.prototype.updateDisplayList = function () {
            this.invalidateSize();
        };

        DisplayObject.prototype.updateSize = function (newWidth, newHeight) {
            var oldMWidth = this.measuredWidth;
            var oldMHeight = this.measuredHeight;
            this._measuredWidth = newWidth;
            this._measuredHeight = newHeight;
            if (oldMWidth !== newWidth)
                this.notifyPropertyChanged("measuredWidth", oldMWidth, newWidth);
            if (oldMHeight !== newHeight)
                this.notifyPropertyChanged("measuredHeight", oldMHeight, newHeight);
        };

        DisplayObject.prototype.checkSize = function () {
            if (this.domElement.style.display === "none")
                return;
            if (this.measuredWidth !== this.domElement.clientWidth || this.measuredHeight !== this.domElement.clientHeight) {
                this.updateSize(this.domElement.clientWidth, this.domElement.clientHeight);
            }
        };

        Object.defineProperty(DisplayObject.prototype, "top", {
            get: function () {
                return this._top;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.top === value)
                    return;
                var old = this.top;
                this._top = value;
                this.notifyPropertyChanged("top", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "left", {
            get: function () {
                return this._left;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.left === value)
                    return;
                var old = this.left;
                this._left = value;
                this.notifyPropertyChanged("left", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "right", {
            get: function () {
                return this._right;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.right === value)
                    return;
                var old = this.right;
                this._right = value;
                this.notifyPropertyChanged("right", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "bottom", {
            get: function () {
                return this._bottom;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.bottom === value)
                    return;
                var old = this.bottom;
                this._bottom = value;
                this.notifyPropertyChanged("bottom", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "width", {
            get: function () {
                return this._width;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.width === value)
                    return;
                var old = this.width;
                this._width = value;
                this.notifyPropertyChanged("width", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "height", {
            get: function () {
                return this._height;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (this.height === value)
                    return;
                var old = this.height;
                this._height = value;
                this.notifyPropertyChanged("height", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "gridColumn", {
            get: function () {
                return this._gridColumn;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "gridRow", {
            get: function () {
                return this._gridRow;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "gridColumnSpan", {
            get: function () {
                return this._gridColumnSpan;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "gridRowSpan", {
            get: function () {
                return this._gridRowSpan;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "parent", {
            get: function () {
                return this._parent;
            },
            set: function (value) {
                this._parent = value;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "verticalAlign", {
            get: function () {
                return this._verticalAlign;
            },
            set: function (value) {
                if (value !== VerticalAlign.TOP && value !== VerticalAlign.BOTTOM && value !== VerticalAlign.CENTER && value !== VerticalAlign.STRETCH)
                    throw new Error("Incorrect verticalAlign value. Correct values are: top, bottom, center, stretch.");
                if (this.verticalAlign === value)
                    return;
                var old = this.verticalAlign;
                this._verticalAlign = value;
                this.notifyPropertyChanged("verticalAlign", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(DisplayObject.prototype, "horizontalAlign", {
            get: function () {
                return this._horizontalAlign;
            },
            set: function (value) {
                if (value !== HorizontalAlign.LEFT && value !== HorizontalAlign.RIGHT && value !== HorizontalAlign.CENTER && value !== HorizontalAlign.STRETCH)
                    throw new Error("Incorrect horizontalAlign value. Correct values are: left, right, center, stretch.");
                if (this.horizontalAlign === value)
                    return;
                var old = this.horizontalAlign;
                this._horizontalAlign = value;
                this.notifyPropertyChanged("horizontalAlign", old, value);
            },
            enumerable: true,
            configurable: true
        });










        DisplayObject.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        DisplayObject.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        DisplayObject.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };

        DisplayObject.prototype.getPropagationParent = function () {
            return this.parent;
        };
        return DisplayObject;
    })(InteractiveObject);
    exports.DisplayObject = DisplayObject;

    var IIDisplayContainer = (function () {
        function IIDisplayContainer() {
            this.className = "IIDisplayContainer";
            this.methodNames = ["addChild", "addChildAt", "getChildAt", "removeChild", "removeChildAt", "owned", "hasChild", "getChildIndex"];
            this.inheritsFrom = [new IIDisplayObject()];
        }
        return IIDisplayContainer;
    })();
    exports.IIDisplayContainer = IIDisplayContainer;

    var DisplayContainerEvent = (function (_super) {
        __extends(DisplayContainerEvent, _super);
        function DisplayContainerEvent() {
            _super.apply(this, arguments);
        }
        DisplayContainerEvent.ITEM_ADDED = "item_added";
        DisplayContainerEvent.ITEM_REMOVED = "item_removed";
        return DisplayContainerEvent;
    })(events.RokkEvent);
    exports.DisplayContainerEvent = DisplayContainerEvent;

    var DisplayContainer = (function (_super) {
        __extends(DisplayContainer, _super);
        function DisplayContainer(node) {
            _super.call(this, node);
            this.children = new collections.List();
        }
        DisplayContainer.prototype.addChild = function (child) {
            this.takeOwnership(child);
            this.children.push(child);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_ADDED, false, false);
            event.index = this.children.length - 1;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        };

        DisplayContainer.prototype.getChildIndex = function (child) {
            return this.children.indexOf(child);
        };

        DisplayContainer.prototype.addChildAt = function (child, index) {
            this.takeOwnership(child);
            this.children.addItemAt(index, child);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_ADDED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        };

        DisplayContainer.prototype.getChildAt = function (index) {
            return this.children.getItemAt(index);
        };

        Object.defineProperty(DisplayContainer.prototype, "childrenNum", {
            get: function () {
                return this.children.length;
            },
            enumerable: true,
            configurable: true
        });

        DisplayContainer.prototype.removeChild = function (child) {
            this.releaseOwnership(child);
            var index = this.children.indexOf(child);
            this.children.removeItemAt(index);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_REMOVED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);

            this.invalidateDisplayList();
        };

        DisplayContainer.prototype.removeChildAt = function (index) {
            var child = this.children.getItemAt(index);
            this.releaseOwnership(child);
            this.children.removeItemAt(index);
            var event = new DisplayContainerEvent(DisplayContainerEvent.ITEM_REMOVED, false, false);
            event.index = index;
            event.item = child;
            this.dispatchEvent(event);
            this.invalidateDisplayList();
        };

        DisplayContainer.prototype.hasChild = function (child) {
            return this.children.indexOf(child) !== -1;
        };

        DisplayContainer.prototype.owned = function (child) {
            return child.parent === this;
        };

        DisplayContainer.prototype.takeOwnership = function (child) {
            if (this.owned(child))
                return;
            child.attach(this);
            if (child.parent !== null) {
                child.parent.removeChild(child);
            }
            child.parent = this;
        };

        DisplayContainer.prototype.releaseOwnership = function (child) {
            if (!this.owned(child))
                return;
            child.detach(this);
            child.parent = null;
            child.removeFromDisplayList();
        };

        DisplayContainer.prototype.updateSize = function (newWidth, newHeight) {
            _super.prototype.updateSize.call(this, newWidth, newHeight);
            for (var i = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).checkSize();
            }
        };

        DisplayContainer.prototype.appendChildren = function () {
            for (var i = 0; i < this.children.length; i++) {
                this.domElement.appendChild(this.children.getItemAt(i).domElement);
                if (this.addedToDisplayList)
                    this.children.getItemAt(i).addToDisplayList();
            }
        };

        DisplayContainer.prototype.updateDisplayList = function () {
            this.appendChildren();
            this.fixWrappers();
            _super.prototype.updateDisplayList.call(this);
        };

        DisplayContainer.prototype.fixWrappers = function () {
            //Stub for item placement
        };

        DisplayContainer.prototype.update = function (observable, property, oldValue, newValue) {
            //Stub for item repositioning
            _super.prototype.update.call(this, observable, property, oldValue, newValue);
        };

        DisplayContainer.prototype.addToDisplayList = function () {
            _super.prototype.addToDisplayList.call(this);
            for (var i = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).addToDisplayList();
            }
        };

        DisplayContainer.prototype.removeFromDisplayList = function () {
            _super.prototype.removeFromDisplayList.call(this);
            for (var i = 0; i < this.children.length; i++) {
                this.children.getItemAt(i).removeFromDisplayList();
            }
        };
        return DisplayContainer;
    })(DisplayObject);
    exports.DisplayContainer = DisplayContainer;

    var HTMLElementWrapper = (function (_super) {
        __extends(HTMLElementWrapper, _super);
        function HTMLElementWrapper() {
            _super.apply(this, arguments);
        }
        return HTMLElementWrapper;
    })(DisplayContainer);
    exports.HTMLElementWrapper = HTMLElementWrapper;

    var StockContainerAlign = (function () {
        function StockContainerAlign() {
        }
        StockContainerAlign.VERTICAL = "vertical";
        StockContainerAlign.HORIZONTAL = "horizontal";
        return StockContainerAlign;
    })();
    exports.StockContainerAlign = StockContainerAlign;

    var Group = (function (_super) {
        __extends(Group, _super);
        function Group() {
            _super.apply(this, arguments);
            this._inner = null;
        }
        Object.defineProperty(Group.prototype, "inner", {
            get: function () {
                if (this._inner === null) {
                    this._inner = document.createElement('div');
                    this._inner.style.position = "relative";
                    this._inner.style.width = "100%";
                    this._inner.style.height = "100%";
                }
                return this._inner;
            },
            enumerable: true,
            configurable: true
        });

        Group.prototype.appendChildren = function () {
            this.domElement.appendChild(this.inner);
            for (var i = 0; i < this.childrenNum; i++) {
                this.inner.appendChild(this.getChildAt(i).domElement);
                if (this.addedToDisplayList)
                    this.getChildAt(i).addToDisplayList();
            }
        };

        Group.prototype.positioning = function (child) {
            child.domElement.style.position = "absolute";
            var topMarginModifier = 0;
            var leftMarginModifier = 0;
            if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                child.domElement.style.left = "0";
                child.domElement.style.right = "0";
            } else if (child.horizontalAlign === HorizontalAlign.LEFT) {
                child.domElement.style.left = "0";
                child.domElement.style.right = "";
            } else if (child.horizontalAlign === HorizontalAlign.RIGHT) {
                child.domElement.style.right = "0";
                child.domElement.style.left = "";
            } else if (child.horizontalAlign === HorizontalAlign.CENTER) {
                child.domElement.style.right = "";
                child.domElement.style.left = "50%";
                leftMarginModifier = (-1) * Math.round(child.measuredWidth / 2);
            }

            if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                child.domElement.style.width = "";
            } else {
                child.domElement.style.width = child.width.toString() + "px";
            }

            if (child.verticalAlign === VerticalAlign.STRETCH) {
                child.domElement.style.top = "0";
                child.domElement.style.bottom = "0";
            } else if (child.verticalAlign === VerticalAlign.TOP) {
                child.domElement.style.top = "0";
                child.domElement.style.bottom = "";
            } else if (child.verticalAlign === VerticalAlign.BOTTOM) {
                child.domElement.style.top = "";
                child.domElement.style.bottom = "0";
            } else if (child.verticalAlign === VerticalAlign.CENTER) {
                child.domElement.style.top = "50%";
                child.domElement.style.bottom = "";
                topMarginModifier = (-1) * Math.round(child.measuredHeight / 2);
            }

            if (child.verticalAlign === VerticalAlign.STRETCH) {
                child.domElement.style.height = "";
            } else {
                child.domElement.style.height = child.height.toString() + "px";
            }

            child.domElement.style.marginTop = (child.top + topMarginModifier).toString() + "px";
            child.domElement.style.marginBottom = child.bottom.toString() + "px";
            child.domElement.style.marginLeft = (child.left + leftMarginModifier).toString() + "px";
            child.domElement.style.marginRight = child.right.toString() + "px";
        };

        Group.prototype.fixWrappers = function () {
            for (var i = 0; i < this.childrenNum; i++) {
                var child = this.getChildAt(i);
                this.positioning(child);
            }
        };

        Group.prototype.update = function (observable, property, oldValue, newValue) {
            _super.prototype.update.call(this, observable, property, oldValue, newValue);
            if (core.InterfaceChecker.implementsInterface(observable, new core.InterfaceChecker(new IIDisplayObject())) && this.owned(observable)) {
                if (property === "width" || property === "height" || property === "top" || property === "left" || property === "bottom" || property === "right" || property === "horizontalAlign" || property === "verticalAlign" || property === "measuredWidth" || property === "measuredHeight") {
                    this.invalidateDisplayList();
                }
            }
        };
        return Group;
    })(DisplayContainer);
    exports.Group = Group;

    var StockContainer = (function (_super) {
        __extends(StockContainer, _super);
        function StockContainer() {
            _super.apply(this, arguments);
            this._align = StockContainerAlign.HORIZONTAL;
            this._gap = 0;
        }
        Object.defineProperty(StockContainer.prototype, "align", {
            get: function () {
                return this._align;
            },
            set: function (value) {
                if (value !== StockContainerAlign.VERTICAL && value !== StockContainerAlign.HORIZONTAL)
                    return;
                if (value === this._align)
                    return;
                var old = this._align;
                this._align = value;
                this.notifyPropertyChanged("align", old, value);
                this.invalidateDisplayList();
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(StockContainer.prototype, "gap", {
            get: function () {
                return this._gap;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (value === this._gap)
                    return;
                var old = this._gap;
                this.notifyPropertyChanged("gap", old, value);
                this.invalidateDisplayList();
            },
            enumerable: true,
            configurable: true
        });



        StockContainer.prototype.positioningStock = function (child, lastValue) {
            child.domElement.style.position = "absolute";
            var currentValue = lastValue;
            var topMarginModifier = 0;
            var leftMarginModifier = 0;
            if (this.align === StockContainerAlign.HORIZONTAL) {
                //Align child horizontally
                child.domElement.style.left = currentValue.toString(10) + "px";

                if (child.verticalAlign === VerticalAlign.STRETCH) {
                    child.domElement.style.top = "0";
                    child.domElement.style.bottom = "0";
                } else if (child.verticalAlign === VerticalAlign.TOP) {
                    child.domElement.style.top = "0";
                    child.domElement.style.bottom = "";
                } else if (child.verticalAlign === VerticalAlign.BOTTOM) {
                    child.domElement.style.top = "";
                    child.domElement.style.bottom = "0";
                } else if (child.verticalAlign === VerticalAlign.CENTER) {
                    child.domElement.style.top = "50%";
                    child.domElement.style.bottom = "";
                    topMarginModifier = (-1) * Math.round(child.measuredHeight / 2);
                }

                if (child.verticalAlign === VerticalAlign.STRETCH) {
                    child.domElement.style.height = "";
                } else {
                    child.domElement.style.height = child.height.toString() + "px";
                }

                currentValue += child.measuredWidth;
            } else {
                //Align child vertically
                child.domElement.style.top = currentValue.toString(10) + "px";

                var leftMarginModifier = 0;
                if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                    child.domElement.style.left = "0";
                    child.domElement.style.right = "0";
                } else if (child.horizontalAlign === HorizontalAlign.LEFT) {
                    child.domElement.style.left = "0";
                    child.domElement.style.right = "";
                } else if (child.horizontalAlign === HorizontalAlign.RIGHT) {
                    child.domElement.style.right = "0";
                    child.domElement.style.left = "";
                } else if (child.horizontalAlign === HorizontalAlign.CENTER) {
                    child.domElement.style.right = "";
                    child.domElement.style.left = "50%";
                    leftMarginModifier = (-1) * Math.round(child.measuredWidth / 2);
                }

                if (child.horizontalAlign === HorizontalAlign.STRETCH) {
                    child.domElement.style.width = "";
                } else {
                    child.domElement.style.width = child.width.toString() + "px";
                }

                currentValue += child.measuredHeight;
            }

            child.domElement.style.marginTop = (child.top + topMarginModifier).toString() + "px";
            child.domElement.style.marginBottom = child.bottom.toString() + "px";
            child.domElement.style.marginLeft = (child.left + leftMarginModifier).toString() + "px";
            child.domElement.style.marginRight = child.right.toString() + "px";

            return currentValue + this.gap;
        };

        StockContainer.prototype.fixWrappers = function () {
            var lastValue = 0;
            for (var i = 0; i < this.childrenNum; i++) {
                var child = this.getChildAt(i);
                lastValue = this.positioningStock(child, lastValue);
            }
        };
        return StockContainer;
    })(Group);
    exports.StockContainer = StockContainer;

    var IIAplication = (function () {
        function IIAplication() {
            this.className = "IIAplication";
            this.methodNames = [];
            this.inheritsFrom = [new IIDisplayContainer()];
        }
        return IIAplication;
    })();
    exports.IIAplication = IIAplication;

    /*   export class Binding {
    source: core.IPropertyChangeNotifier;
    sourceProperty: string;
    target: Object;
    targetProperty: string;
    
    isSource(source: core.IPropertyChangeNotifier, sourceProperty: string) {
    return (this.source === source && this.sourceProperty === sourceProperty);
    }
    }
    
    export class BindingManager implements core.IPropertyObserver {
    private bindings: Binding[] = [];
    
    bind(source: core.IPropertyChangeNotifier, sourceProperty: string, target: Object, targetProperty: string): Binding {
    var binding: Binding = new Binding();
    binding.source = source;
    binding.sourceProperty = sourceProperty;
    binding.target = target;
    binding.targetProperty = targetProperty;
    this.bindings.push(binding);
    return binding;
    }
    
    unbind(binding: Binding) {
    this.bindings.splice(this.bindings.indexOf(binding), 1);
    }
    
    update(observable: core.IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
    for (var i: number = 0; i < this.bindings.length; i++) {
    if (this.bindings[i].isSource(observable, property)) {
    //Apply binding
    this.bindings[i].target[this.bindings[i].targetProperty] = newValue;
    }
    }
    }
    
    private static instance;
    
    static getInstance(): BindingManager {
    if (BindingManager.instance === undefined || BindingManager.instance === null) {
    BindingManager.instance = new BindingManager();
    }
    return BindingManager.instance;
    }
    }*/
    var State = (function () {
        function State() {
        }
        return State;
    })();
    exports.State = State;

    var Definition = (function () {
        function Definition() {
        }
        Definition.prototype.factory = function (scope) {
        };
        return Definition;
    })();
    exports.Definition = Definition;

    var Application = (function (_super) {
        __extends(Application, _super);
        function Application(domNode) {
            _super.call(this, domNode);
            this.keyedItems = {};
            this.resourceDictionary = new ResourceDictionary();
            this.horizontalAlign = HorizontalAlign.STRETCH;
            this.verticalAlign = VerticalAlign.STRETCH;
            var that = this;
            window.onresize = function () {
                that.checkSize();
            };
        }
        return Application;
    })(HTMLElementWrapper);
    exports.Application = Application;

    var ResourceDictionary = (function () {
        function ResourceDictionary() {
            this.mergedDictionaries = [];
            this.resources = {};
        }
        ResourceDictionary.prototype.addResource = function (key, dom) {
            if (this.resources[key] !== undefined)
                throw new Error("Resource key conflict: " + key);
            this.resources[key] = dom;
        };

        ResourceDictionary.prototype.hasResource = function (key) {
            return (this.resources[key] instanceof Object);
        };

        ResourceDictionary.prototype.getResource = function (key) {
            if (!(this.resources[key] instanceof Object))
                throw new Error("Resource key not found: " + key);
            return this.resources[key];
        };
        return ResourceDictionary;
    })();
    exports.ResourceDictionary = ResourceDictionary;

    var Filter = (function () {
        function Filter() {
            this._divide = 1;
            this._mix = 1;
        }
        Object.defineProperty(Filter.prototype, "mixValue", {
            get: function () {
                return this._mix;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Filter.prototype, "divide", {
            get: function () {
                return this._divide;
            },
            enumerable: true,
            configurable: true
        });

        Filter.prototype.mix = function (a, b, ammount) {
            return a + (b - a) * ammount;
        };
        Filter.prototype.convolve = function (matrix, sourceData, output) {
            var divide = this.divide;
            var mix = this.mixValue;

            var matrixSize = Math.sqrt(matrix.length) + 0.5 | 0;
            var halfMatrixSize = matrixSize / 2 | 0;
            var src = sourceData.data;
            var sw = sourceData.width;
            var sh = sourceData.height;
            var w = sw;
            var h = sh;
            var dst = output.data;

            for (var y = 1; y < h - 1; y++) {
                for (var x = 1; x < w - 1; x++) {
                    var dstOff = (y * w + x) * 4;
                    var r = 0, g = 0, b = 0, a = 0;
                    for (var cy = 0; cy < matrixSize; cy++) {
                        for (var cx = 0; cx < matrixSize; cx++) {
                            var scy = y + cy - halfMatrixSize;
                            var scx = x + cx - halfMatrixSize;
                            if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
                                var srcOff = (scy * sw + scx) * 4;
                                var wt = matrix[cy * matrixSize + cx] / divide;
                                r += src[srcOff + 0] * wt;
                                g += src[srcOff + 1] * wt;
                                b += src[srcOff + 2] * wt;
                                a += src[srcOff + 3] * wt;
                            }
                        }
                    }
                    dst[dstOff + 0] = this.mix(src[dstOff + 0], r, mix);
                    dst[dstOff + 1] = this.mix(src[dstOff + 1], g, mix);
                    dst[dstOff + 2] = this.mix(src[dstOff + 2], b, mix);
                    dst[dstOff + 3] = this.mix(src[dstOff + 3], a, mix);
                }
            }

            return output;
        };

        Filter.prototype.applyTo = function (sourceData, output) {
            return sourceData;
        };
        return Filter;
    })();
    exports.Filter = Filter;

    var GaussianBlur = (function (_super) {
        __extends(GaussianBlur, _super);
        function GaussianBlur() {
            _super.apply(this, arguments);
        }
        GaussianBlur.prototype.applyTo = function (sourceData, output) {
            return this.convolve([0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067, 0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292, 0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117, 0.00038771, 0.01330373, 0.11098164, 0.22508352, 0.11098164, 0.01330373, 0.00038771, 0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117, 0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292, 0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067], sourceData, output);
        };
        return GaussianBlur;
    })(Filter);
    exports.GaussianBlur = GaussianBlur;

    var DrawableComponent = (function (_super) {
        __extends(DrawableComponent, _super);
        function DrawableComponent(htmlElement) {
            _super.call(this, htmlElement);
            this.filters = new collections.List();
            this.drawInvalid = true;
            this.canvas = document.createElement('canvas');
            this.canvas.style.width = "100%";
            this.canvas.style.height = "100%";
        }
        DrawableComponent.prototype.invalidateDraw = function () {
            this.drawInvalid = true;
            this.invalidateProperties();
        };

        DrawableComponent.prototype.addToDisplayList = function () {
            _super.prototype.addToDisplayList.call(this);

            this.invalidateDraw();
        };

        DrawableComponent.prototype.commitProperties = function () {
            _super.prototype.commitProperties.call(this);
            if (this.drawInvalid) {
                if (this.canvas.parentElement !== this.domElement) {
                    this.domElement.appendChild(this.canvas);
                    this.invalidateProperties();
                    return;
                }
                this.drawInvalid = false;
                this.canvas.width = this.measuredWidth;
                this.canvas.height = this.measuredHeight;
                if (this.addedToDisplayList) {
                    this.draw(this.measuredWidth, this.measuredHeight, this.canvas);
                    if (this.filters.length > 0) {
                        var sourceData = this.canvas.getContext('2d').getImageData(0, 0, this.canvas.width, this.canvas.height);
                        for (var i = 0; i < this.filters.length; i++) {
                            var output = this.canvas.getContext('2d').createImageData(this.canvas.width, this.canvas.height);
                            (this.filters.getItemAt(i)).applyTo(sourceData, output);
                            sourceData = output;
                        }
                        this.canvas.getContext('2d').putImageData(sourceData, 0, 0);
                    }
                }
            }
        };

        DrawableComponent.prototype.draw = function (newWidth, newHeight, canvas) {
            //Stub for drawing
            var ctx = canvas.getContext("2d");
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, newWidth - 0, newHeight - 0);
        };

        DrawableComponent.prototype.updateSize = function (newWidth, newHeight) {
            var oldMWidth = this.measuredWidth;
            var oldMHeight = this.measuredHeight;
            _super.prototype.updateSize.call(this, newWidth, newHeight);
            this.invalidateDraw();
        };

        DrawableComponent.prototype.getDrawSource = function () {
            return this.canvas;
        };
        return DrawableComponent;
    })(DisplayObject);
    exports.DrawableComponent = DrawableComponent;

    var Label = (function (_super) {
        __extends(Label, _super);
        function Label() {
            _super.apply(this, arguments);
            this._text = "";
            this.textChanged = false;
        }
        Object.defineProperty(Label.prototype, "text", {
            get: function () {
                return this._text;
            },
            set: function (value) {
                if (this._text == value)
                    return;
                var old = this._text;
                this._text = value;
                this.textChanged = true;
                this.invalidateProperties();
                this.notifyPropertyChanged("text", old, value);
            },
            enumerable: true,
            configurable: true
        });
        Label.prototype.commitProperties = function () {
            _super.prototype.commitProperties.call(this);
            if (this.textChanged === true) {
                this.textChanged = false;
                this.domElement.innerHTML = this.text;
            }
        };
        return Label;
    })(DisplayObject);
    exports.Label = Label;

    var ImageBox = (function (_super) {
        __extends(ImageBox, _super);
        function ImageBox(element) {
            _super.call(this, element);
            this._size = "contain";
            this._positionX = "center";
            this._positionY = "center";
            this._repeat = "no-repeat";
            this._resizeStrategy = "no-resize";
            this.sizeChanged = true;
            this.srcChanged = true;
            this.positionChanged = true;
            this.imageFile = document.createElement("img");
            var that = this;
            this.imageFile.onload = function () {
                that.renderImageSrc();
            };
            this.attach(this);
        }

        Object.defineProperty(ImageBox.prototype, "resizeStrategy", {
            get: function () {
                return this._resizeStrategy;
            },
            set: function (value) {
                if (this._resizeStrategy === value)
                    return;
                var old = this._resizeStrategy;
                this._resizeStrategy = value;
                this.notifyPropertyChanged("resizeStrategy", old, value);
                this.renderImageSrc();
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ImageBox.prototype, "positionX", {
            get: function () {
                return this._positionX;
            },
            set: function (value) {
                if (this._positionX == value)
                    return;
                var old = this._positionX;
                this._positionX = value;
                this.notifyPropertyChanged("positionX", old, value);
                this.positionChanged = true;
                this.invalidateProperties();
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ImageBox.prototype, "positionY", {
            get: function () {
                return this._positionY;
            },
            set: function (value) {
                if (this._positionY == value)
                    return;
                var old = this._positionY;
                this._positionY = value;
                this.notifyPropertyChanged("positionY", old, value);
                this.positionChanged = true;
                this.invalidateProperties();
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ImageBox.prototype, "repeat", {
            get: function () {
                return this._repeat;
            },
            set: function (value) {
                if (this._repeat == value)
                    return;
                var old = this._repeat;
                this._repeat = value;
                this.notifyPropertyChanged("repeat", old, value);
                this.sizeChanged = true;
                this.invalidateProperties();
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ImageBox.prototype, "src", {
            get: function () {
                return this._src;
            },
            set: function (value) {
                if (this._src == value)
                    return;
                var old = this._src;
                this._src = value;
                this.imageFile.src = value;
                this.notifyPropertyChanged("src", old, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ImageBox.prototype, "size", {
            get: function () {
                return this._size;
            },
            set: function (value) {
                if (this._size == value)
                    return;
                var old = this._size;
                this._size = value;
                this.notifyPropertyChanged("size", old, value);
                this.sizeChanged = true;
                this.invalidateProperties();
            },
            enumerable: true,
            configurable: true
        });

        ImageBox.prototype.renderImageSrc = function () {
            if (this.imageFile.complete) {
                //resize image
                //Determinate width height
                var width;
                var height;
                if (this.resizeStrategy === "no-resize") {
                    width = this.imageFile.width;
                    height = this.imageFile.height;
                } else if (this.resizeStrategy === "width") {
                    width = this.measuredWidth;
                    var ratio = this.measuredWidth / this.imageFile.width;
                    height = ratio * this.imageFile.height;
                } else {
                    height = this.measuredHeight;
                    var ratio = this.measuredHeight / this.imageFile.height;
                    width = ratio * this.imageFile.width;
                }
                console.log(this.src, this.resizeStrategy, this.measuredHeight, width, height);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(this.imageFile, 0, 0, width, height);

                //document.body.appendChild(canvas);
                this.imageSrc = canvas.toDataURL();
                this.srcChanged = true;
                this.invalidateProperties();
            }
        };






        ImageBox.prototype.update = function (item, property, old, newVal) {
            _super.prototype.update.call(this, item, property, old, newVal);
            if (item === this) {
                if (property === "measuredHeight" && this.resizeStrategy === "height")
                    this.renderImageSrc();
                if (property === "measuredWidth" && this.resizeStrategy === "width")
                    this.renderImageSrc();
            }
        };

        ImageBox.prototype.commitProperties = function () {
            _super.prototype.commitProperties.call(this);
            if (this.srcChanged === true) {
                this.srcChanged = false;

                this.domElement.style.backgroundImage = "url(" + this.imageSrc + ")";
            }
            if (this.positionChanged) {
                var positionX = this.positionX;
                if (!isNaN(parseInt(positionX))) {
                    positionX = parseInt(positionX) + "px";
                }
                var positionY = this.positionY;
                if (!isNaN(parseInt(positionY))) {
                    positionY = parseInt(positionY) + "px";
                }
                this.domElement.style.backgroundPosition = positionX + " " + positionY;
            }
            if (this.sizeChanged === true) {
                this.sizeChanged = false;
                this.domElement.style.backgroundSize = this.size;
                this.domElement.style.backgroundRepeat = this.repeat;
            }
        };
        return ImageBox;
    })(DisplayObject);
    exports.ImageBox = ImageBox;
});
//# sourceMappingURL=ComponentBase.js.map
