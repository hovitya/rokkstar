﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path='Events.ts'/>
///<reference path='Collections.ts'/>
///<reference path='../third-party/definitions/Tween.d.ts'/>
var core;
(function (core) {
    (function (animations) {
        var SimplePropertyAnimation = (function (_super) {
            __extends(SimplePropertyAnimation, _super);
            //public
            function SimplePropertyAnimation(html) {
                _super.call(this);
                this.propertyName = "";
                this.from = undefined;
                this.to = 0.0;
                this.tween = new TWEEN.Tween(this);
            }

            Object.defineProperty(SimplePropertyAnimation.prototype, "duration", {
                get: function () {
                    return this._duration;
                },
                set: function (value) {
                    value = core.PrimitiveTypeTools.ensureInteger(value);
                    this._duration = value;
                },
                enumerable: true,
                configurable: true
            });

            Object.defineProperty(SimplePropertyAnimation.prototype, "target", {
                get: function () {
                    return this._target;
                },
                set: function (value) {
                    this._target = value;
                },
                enumerable: true,
                configurable: true
            });


            Object.defineProperty(SimplePropertyAnimation.prototype, "currentValue", {
                get: function () {
                    if (this.target !== undefined && this.target !== null && this.propertyName !== "") {
                        return this.target[this.propertyName];
                    }
                    return 0;
                },
                set: function (value) {
                    if (this.target !== undefined && this.target !== null && this.propertyName !== "") {
                        this.target[this.propertyName] = value;
                    }
                },
                enumerable: true,
                configurable: true
            });


            SimplePropertyAnimation.prototype.run = function () {
                if (this.from !== undefined && this.from !== null) {
                    this.currentValue = this.from;
                }
                var to = {};
                to[this.propertyName] = this.to;
                this.tween.to(to, this.duration).start();
            };

            SimplePropertyAnimation.prototype.stop = function () {
                this.tween.stop();
            };
            return SimplePropertyAnimation;
        })(core.EventDispatcher);
        animations.SimplePropertyAnimation = SimplePropertyAnimation;
    })(core.animations || (core.animations = {}));
    var animations = core.animations;
})(core || (core = {}));
