﻿///<reference path='ComponentBase.ts'/>
import core = require("./Base");
import events = require("./Events");
import components = require("./ComponentBase");
    export class Shape extends components.DrawableComponent {
        private _background: components.IBackgroundBrush;
        //private _borderColor: core.Color;
        //private _borderWidth: number;
        private _invalidateDraw: (event: events.RokkEvent) => void = null;

        constructor(elem?: HTMLElement) {
            super(elem);
            var that = this;
            this._invalidateDraw = (event: events.RokkEvent): void => {
                that.invalidateDraw();

            }
        }
            
        set background(value: components.IBackgroundBrush) {
            var old = this.background;
            if (old !== null && old !== undefined) {
                old.removeEventListener(components.BackgroundBrushEvent.UPDATED, this._invalidateDraw);
            }
            if (typeof value !== "object") {
                this._background = components.BackgroundParser.parseBackgroundString(<any>value);
            } else {
                this._background = value;
            }
            this._background.addEventListener(components.BackgroundBrushEvent.UPDATED, this._invalidateDraw);
            this.notifyPropertyChanged("background", old, this.background);
            this.invalidateDraw();
        }

        get background(): components.IBackgroundBrush {
            return this._background;
        }

       /* set borderWidth(value: number) {
            var old: number = this._borderWidth;
            value = core.PrimitiveTypeTools.ensureInteger(value);
            this._borderWidth = value;
            this.notifyPropertyChanged("borderWidth", old, value);
            this.invalidateDraw();
        }

        get borderWidth(): number {
            return this._borderWidth;
        }

        set borderColor(value: core.Color) {
            var old: core.Color = this._borderColor;
            if (typeof value !== "object") {
                value = new core.Color(value);
            }
            this._borderColor = value;
            this.notifyPropertyChanged("borderColor", old, value);
            this.invalidateDraw();
        }

        get borderColor(): core.Color {
            return this._borderColor;
        }*/

    }

    export class Ellipse extends Shape {
        draw(w: number, h: number, canvas: HTMLCanvasElement) {
            var x: number = 0;
            var y: number = 0;

            var graphics: CanvasRenderingContext2D = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            var kappa = 0.5522848,
                ox = (w / 2) * kappa,
                oy = (h / 2) * kappa,
                xe = x + w,
                ye = y + h,
                xm = x + w / 2,
                ym = y + h / 2;

            graphics.beginPath();
            graphics.moveTo(x, ym);
            graphics.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
            graphics.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
            graphics.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
            graphics.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
            graphics.closePath();
            graphics.fill();
        }
    }

    export class Rectangle extends Shape {

        private _corner: number = 0;
        private _topLeftCorner: number = undefined;
        private _topRightCorner: number = undefined;
        private _bottomLeftCorner: number = undefined;
        private _bottomRightCorner: number = undefined;

        get corner(): number {
            return this._corner;
        }

        set corner(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            var old: number = this.corner;
            this._corner = value;
            this.notifyPropertyChanged("corner", old, value);
            this.invalidateDraw();
        }

        get topLeftCorner(): number {
            return this._topLeftCorner;
        }

        set topLeftCorner(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            var old: number = this.corner;
            if (value === old) return;
            this._topLeftCorner = value;
            this.notifyPropertyChanged("topLeftCorner", old, value);
            this.invalidateDraw();
        }

        get topRightCorner(): number {
            return this._topRightCorner;
        }

        set topRightCorner(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            var old: number = this.corner;
            if (value === old) return;
            this._topRightCorner = value;
            this.notifyPropertyChanged("topRightCorner", old, value);
            this.invalidateDraw();
        }

        get bottomLeftCorner(): number {
            return this._bottomLeftCorner;
        }

        set bottomLeftCorner(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            var old: number = this.corner;
            if (value === old) return;
            this._bottomLeftCorner = value;
            this.notifyPropertyChanged("bottomLeftCorner", old, value);
            this.invalidateDraw();
        }

        get bottomRightCorner(): number {
            return this._bottomRightCorner;
        }

        set bottomRightCorner(value: number) {
            value = core.PrimitiveTypeTools.ensureInteger(value);
            var old: number = this.corner;
            if (value === old) return;
            this._bottomRightCorner = value;
            this.notifyPropertyChanged("bottomRightCorner", old, value);
            this.invalidateDraw();
        }

        draw(w: number, h: number, canvas: HTMLCanvasElement) {
            var x: number = 0;
            var y: number = 0;

            var topLeftCorner: number = this.topLeftCorner;
            var topRightCorner: number = this.topRightCorner;
            var bottomLeftCorner: number = this.bottomLeftCorner;
            var bottomRightCorner: number = this.bottomRightCorner;
            if (topLeftCorner === undefined) topLeftCorner = this.corner;
            if (topRightCorner === undefined) topRightCorner = this.corner;
            if (bottomLeftCorner === undefined) bottomLeftCorner = this.corner;
            if (bottomRightCorner === undefined) bottomRightCorner = this.corner;

            var graphics: CanvasRenderingContext2D = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            if (topLeftCorner == 0 && topRightCorner == 0 && bottomLeftCorner == 0 && bottomLeftCorner == 0) {
                graphics.beginPath();
                graphics.rect(x, y, w, h);
                graphics.closePath();
            } else {
                graphics.beginPath();
                graphics.moveTo(x + topLeftCorner, y);
                graphics.lineTo(x + w - topRightCorner, y);
                if (topRightCorner != 0) {
                    graphics.quadraticCurveTo(x + w, y, x + w, y + topRightCorner);
                }
                graphics.lineTo(x + w, y + h - bottomRightCorner);
                if (bottomRightCorner != 0) {
                    graphics.quadraticCurveTo(x + w, y + h, x + w - bottomRightCorner, y + h);
                }
                graphics.lineTo(x + bottomLeftCorner, y + h);
                if (bottomLeftCorner != 0) {
                    graphics.quadraticCurveTo(x, y + h, x, y + h - bottomLeftCorner);
                }
                graphics.lineTo(x, y + topLeftCorner);
                if (topLeftCorner != 0) {
                    graphics.quadraticCurveTo(x, y, x + topLeftCorner, y);
                }
                graphics.closePath();
            }
            graphics.fill();
        }
    }

    export class Text extends Shape {
        private _fontFamily = "Calibri";
        private _fontSize = "12pt";
        private _color = null;
        private _textVerticalAlign = "center";
        private _textHorizontalAlign = "left";
        constructor(elem?: HTMLElement) {
            super(elem);
            this._color = new components.Color("black");
        }



    }