var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./Events"], function(require, exports, __events__) {
    
    var events = __events__;

    var IICursor = (function () {
        function IICursor() {
            this.className = "IICursor";
            this.methodNames = ["next", "prev", "current", "hasNext", "hasPrev", "rewind", "seek", "remove", "add"];
            this.inheritsFrom = [];
        }
        return IICursor;
    })();
    exports.IICursor = IICursor;

    var IIList = (function () {
        function IIList() {
            this.className = "IIList";
            this.methodNames = ["indexOf", "push", "pop", "getItemAt", "removeItemAt", "addItemAt", "toArray"];
            this.inheritsFrom = [new events.IIEventDispatcher()];
        }
        return IIList;
    })();
    exports.IIList = IIList;

    var IIIterable = (function () {
        function IIIterable() {
            this.className = "IIIterable";
            this.methodNames = ["createCursor"];
            this.inheritsFrom = [];
        }
        return IIIterable;
    })();
    exports.IIIterable = IIIterable;

    var IICollectionView = (function () {
        function IICollectionView() {
            this.className = "IICollectionView";
            this.methodNames = ["filterFunction", "sortFunction", "contains", "disableAutoUpdate", "enableAutoUpdate", "refresh"];
            this.inheritsFrom = [new IIIterable(), new events.IIEventDispatcher()];
        }
        return IICollectionView;
    })();
    exports.IICollectionView = IICollectionView;

    var CollectionEvent = (function (_super) {
        __extends(CollectionEvent, _super);
        function CollectionEvent(type, bubbles, cancelable, position, affectedItem) {
            _super.call(this, type, bubbles, cancelable);
            if (position)
                this.position = position;
            if (affectedItem)
                this.affectedItem = affectedItem;
        }
        CollectionEvent.ITEM_ADDED = "item_added";
        CollectionEvent.ITEM_REMOVED = "item_removed";
        CollectionEvent.REFRESHED = "refreshed";
        return CollectionEvent;
    })(events.RokkEvent);
    exports.CollectionEvent = CollectionEvent;

    var List = (function (_super) {
        __extends(List, _super);
        function List(source) {
            _super.call(this);
            if (source) {
                var src = source.slice(0);
                this.source = src.filter(function (el, i, a) {
                    return i == a.indexOf(el);
                }, this);
            } else {
                this.source = new Array();
            }
        }
        List.prototype.indexOf = function (item, fromIndex) {
            if (fromIndex)
                return this.source.indexOf(item);
            return this.source.indexOf(item, fromIndex);
        };

        List.prototype.lastIndexOf = function (item, fromIndex) {
            if (fromIndex)
                return this.source.lastIndexOf(item);
            return this.source.lastIndexOf(item, fromIndex);
        };

        List.prototype.push = function (item) {
            if (this.source.indexOf(item) != -1) {
                //remove previous
                this.removeItemAt(this.source.indexOf(item));
            }
            this.source.push(item);
            this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, this.source.length - 1, item));
            return this;
        };

        List.prototype.pop = function () {
            var item = this.source.pop();
            this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, this.source.length, item));
            return item;
        };

        Object.defineProperty(List.prototype, "length", {
            get: function () {
                return this.source.length;
            },
            enumerable: true,
            configurable: true
        });

        List.prototype.removeItemAt = function (index) {
            if (!this.checkIndex(index))
                throw new Error("Index out of bounds.");
            var item = this.source[index];
            this.source.splice(index, 1);
            this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, index, item));
            return this;
        };

        List.prototype.getItemAt = function (index) {
            if (!this.checkIndex(index))
                throw new Error("Index out of bounds.");
            return this.source[index];
        };

        List.prototype.checkIndex = function (index) {
            if (index < 0 || index >= this.source.length) {
                return false;
            }
            return true;
        };

        List.prototype.addItemAt = function (index, item) {
            if (this.source.indexOf(item) != -1) {
                //remove previous
                this.removeItemAt(this.source.indexOf(item));
            }
            if (index === this.source.length) {
                this.source.push(item);
                this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, index, item));
                return;
            }
            if (!this.checkIndex(index))
                throw new Error("Index out of bounds.");
            this.source.splice(index, 0, item);
            this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, index, item));
            return this;
        };

        List.prototype.toArray = function () {
            return this.source.slice(0);
        };

        List.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        List.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        List.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };
        return List;
    })(events.EventDispatcher);
    exports.List = List;

    var ListItemArrayAccess = (function () {
        function ListItemArrayAccess() {
            this.items = [];
        }
        return ListItemArrayAccess;
    })();
    exports.ListItemArrayAccess = ListItemArrayAccess;

    var ListCollectionView = (function (_super) {
        __extends(ListCollectionView, _super);
        function ListCollectionView(source) {
            _super.call(this);
            this.isAutoUpdate = true;
            this.itemsAccess = new ListItemArrayAccess();
            this.itemIndexes = [];
            this.observers = [];
            this.items = [];
            if (source !== undefined) {
                this._source = source;
            } else {
                this._source = new List();
            }
            var that = this;

            this.source.addEventListener(CollectionEvent.ITEM_ADDED, function (event) {
                if (that.filterFunction && !that.filterFunction(event.affectedItem)) {
                    return;
                }
                if (!that.sortFunction) {
                    that.items.splice(event.position, 0, event.affectedItem);
                    that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, event.position, event.affectedItem));
                } else {
                    var i = 0;
                    while (i < that.items.length && that.sortFunction(event.affectedItem, that.items[i]) > -1) {
                        i++;
                    }
                    that.items.splice(i, 0, event.affectedItem);
                    that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, i, event.affectedItem));
                }
                that.notifyPropertyChanged('length', that.items.length - 1, that.items.length);
            });
            this.source.addEventListener(CollectionEvent.ITEM_REMOVED, function (event) {
                if (that.filterFunction && !that.filterFunction(event.affectedItem)) {
                    return;
                }
                for (var i = 0; i < that.items.length; i++) {
                    if (that.items[i] === event.affectedItem) {
                        that.items.splice(i, 1);
                        that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, i, event.affectedItem));
                        that.notifyPropertyChanged('length', that.items.length + 1, that.items.length);
                        return;
                    }
                }
            });
            this.refresh();
        }
        Object.defineProperty(ListCollectionView.prototype, "items", {
            get: function () {
                return this._items;
            },
            set: function (value) {
                this.itemsAccess.items = value;
                this._items = value;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ListCollectionView.prototype, "length", {
            get: function () {
                return this.items.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ListCollectionView.prototype, "source", {
            get: function () {
                return this._source;
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(ListCollectionView.prototype, "filterFunction", {
            get: function () {
                return this._filterFunction;
            },
            set: function (value) {
                this._filterFunction = value;
                if (this.isAutoUpdate)
                    this.refresh();
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(ListCollectionView.prototype, "sortFunction", {
            get: function () {
                return this._sortFunction;
            },
            set: function (value) {
                this._sortFunction = value;
                if (this.isAutoUpdate)
                    this.refresh();
            },
            enumerable: true,
            configurable: true
        });


        ListCollectionView.prototype.refresh = function () {
            var oldItemsLength = this.items.length;

            this.items = [];

            for (var i = 0; i < this.source.length; i++) {
                if (!this.filterFunction || this.filterFunction(this.source.getItemAt(i))) {
                    this.items.push(this.source.getItemAt(i));
                }
            }

            if (this.sortFunction) {
                this.items.sort(this.sortFunction);
            }
            this.dispatchEvent(new CollectionEvent(CollectionEvent.REFRESHED, false, true, -1, null));
            if (oldItemsLength !== this.items.length) {
                this.notifyPropertyChanged("length", oldItemsLength, this.items.length);
            }
        };

        ListCollectionView.prototype.contains = function (item) {
            if (this.items.indexOf(item) === -1)
                return false;
            return true;
        };

        ListCollectionView.prototype.disableAutoUpdate = function () {
            this.isAutoUpdate = false;
        };

        ListCollectionView.prototype.enableAutoUpdate = function () {
            this.isAutoUpdate = true;
        };

        ListCollectionView.prototype.attach = function (observer) {
            this.observers.push(observer);
        };

        ListCollectionView.prototype.detach = function (observer) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        };

        ListCollectionView.prototype.notifyPropertyChanged = function (property, oldValue, newValue) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        };

        ListCollectionView.prototype.createCursor = function () {
            return new ListCollectionViewCursor(this, this.itemsAccess);
        };
        return ListCollectionView;
    })(events.EventDispatcher);
    exports.ListCollectionView = ListCollectionView;

    var ListCollectionViewCursor = (function () {
        function ListCollectionViewCursor(list, access) {
            this.view = list;
            this.access = access;
        }
        ListCollectionViewCursor.prototype.next = function () {
            this.currentPosition++;
            if (this.currentPosition < this.length) {
                return true;
            }
            return false;
        };

        ListCollectionViewCursor.prototype.prev = function () {
            this.currentPosition--;
            if (this.currentPosition >= 0) {
                return true;
            }
            return false;
        };

        ListCollectionViewCursor.prototype.seek = function (position) {
            if (position >= 0 && position < this.length) {
                this.currentPosition = position;
                return true;
            }
            return false;
        };

        Object.defineProperty(ListCollectionViewCursor.prototype, "length", {
            get: function () {
                return this.view.length;
            },
            enumerable: true,
            configurable: true
        });

        ListCollectionViewCursor.prototype.current = function () {
            return this.access.items[this.currentPosition];
        };

        ListCollectionViewCursor.prototype.hasNext = function () {
            return (this.currentPosition < this.length - 1);
        };

        ListCollectionViewCursor.prototype.hasPrev = function () {
            return (this.currentPosition > 0);
        };

        ListCollectionViewCursor.prototype.rewind = function () {
            this.currentPosition = 0;
        };

        ListCollectionViewCursor.prototype.remove = function () {
            this.view.source.removeItemAt(this.currentPosition);
            return this;
        };

        ListCollectionViewCursor.prototype.add = function (item) {
            this.view.source.addItemAt(this.currentPosition, item);
            return this;
        };
        return ListCollectionViewCursor;
    })();
    exports.ListCollectionViewCursor = ListCollectionViewCursor;
});
//# sourceMappingURL=Collections.js.map
