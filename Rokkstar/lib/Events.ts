///<reference path='Base.ts'/>
import core = require("./Base");
export class EventPhase {
    static CAPTURE_PHASE: number = 0;

    static AT_TARGET: number = 1;

    static BUBBLING_PHASE: number = 2;
};

export class RokkEvent {
    type: string;
    cancelable: boolean;
    bubbles: boolean;
    eventPhase: number = 0;
    target: IEventDispatcher;
    currentTarget: IEventDispatcher;
    isPropagating: boolean = true;
    isImmediatePropagating: boolean = true;
    isCancelled: boolean = true;
    originalEvent: any;
    data: any;

    constructor(type: string, bubbles: boolean = false, cancelable: boolean = false) {
        this.type = type;
        this.bubbles = bubbles;
        this.cancelable = cancelable;
    }

    stopPropagation() {
        this.isPropagating = false;
    }

    stopImmediatePropagation() {
        this.isImmediatePropagating = false;
    }

    preventDefault() {
        if (this.cancelable) {
            this.isCancelled = true;
        }
    }

    isDefaultPrevented() {
        if (!this.cancelable) {
            return false;
        }
        return this.isCancelled;
    }


    serialize(): any {
        return { type: this.type, bubbles: this.bubbles, cancelable: this.cancelable, data: this.data};
    }

    static unSerialize(data: any): RokkEvent {
        var event = new RokkEvent(data.type, data.bubbles, data.cancelable);
        for (var i in data) {
            if (data.hasOwnProperty(i) && i !== "type" && i !== "bubbles" && i !== "cancelable") {
                event[i] = data[i];
            }
        }
        return event;
    }
}

export interface IEventDispatcher {
    dispatchEvent(evt: RokkEvent);
    addEventListener(eventType: string, listener: (evt: RokkEvent) => void);
    removeEventListener(eventType: string, listener: (evt: RokkEvent) => void);
    getPropagationParent(): IEventDispatcher;
    executeHandlers(event: RokkEvent): void;
}

export class IIEventDispatcher implements core.IInterfaceChecker {
    className: string = "IIEventDispatcher";
    methodNames: string[] = ["dispatchEvent", "addEventListener", "removeEventListener", "getPropagationParent", "executeHandlers"];
    inheritsFrom: core.IInterfaceChecker[] = [];
}

export class EventListenerDescription {
    listenerFunction: (event: RokkEvent) => void;
    once: boolean = false;
    useCapture: boolean = false;
}

export class EventDispatcher implements IEventDispatcher {
    private eventRoot: HTMLElement;
    private handlers: { [index: string]: EventListenerDescription[]; } = {};

    dispatchEvent(event: RokkEvent) {

        event.target = this;

        //Collect activation list
        var lastParent: IEventDispatcher = this.getPropagationParent(),
            activationList: IEventDispatcher[] = [],
            j: number = 0;
        while (lastParent !== null && lastParent !== undefined) {
            activationList.push(lastParent);
            lastParent = lastParent.getPropagationParent();
        }

        //Capture phase
        event.eventPhase = EventPhase.CAPTURE_PHASE;
        j = activationList.length;
        while (j > 0) {
            j = j - 1;
            activationList[j].executeHandlers(event);
            if (!event.isImmediatePropagating || !event.isPropagating) {
                break;
            }
        }

        //At target
        if (event.isImmediatePropagating && event.isPropagating) {
            event.eventPhase = EventPhase.AT_TARGET;
            this.executeHandlers(event);
        }

        //Bubble phase
        if (event.bubbles && event.isImmediatePropagating && event.isPropagating) {
            event.eventPhase = EventPhase.BUBBLING_PHASE;
            j = 0;
            while (j < activationList.length) {
                activationList[j].executeHandlers(event);
                if (!event.isImmediatePropagating || !event.isPropagating) {
                    break;
                }
                j = j + 1;
            }
        }
    }

    getPropagationParent(): IEventDispatcher {
        return null;
    }

    addEventListener(eventType: string, listener: (evt: RokkEvent) => void, once: boolean = false, useCapture: boolean = false) {
        if (this.handlers[eventType] == undefined) {
            this.handlers[eventType] = new Array<EventListenerDescription>();
        }
        var handler: EventListenerDescription = new EventListenerDescription();
        handler.listenerFunction = listener;
        handler.once = once;
        handler.useCapture = useCapture;
        this.handlers[eventType].push(handler);
    }

    removeEventListener(eventType: string, listener: (evt: RokkEvent) => void) {
        var needToRemove: any[] = new Array();
        for (var i in this.handlers[eventType]) {
            if (this.handlers[eventType][i].listenerFunction === listener) {
                needToRemove.push(i);
            }
        }
        var reversed = needToRemove.reverse();
        for (var i in reversed) {
            this.handlers[eventType].splice(reversed[i], 1);
        }
    }

    executeHandlers(event: RokkEvent) {
        var handlers = this.handlers[event.type],
            remove = [],
            i;
        event.currentTarget = this;
        if (handlers !== undefined && handlers !== null) {
            i = handlers.length;
            while (--i >= 0) {
                if (event.eventPhase === EventPhase.CAPTURE_PHASE && handlers[i].useCapture) {
                    handlers[i].listenerFunction(event);
                } else if (!handlers[i].useCapture && (event.eventPhase === EventPhase.BUBBLING_PHASE || event.eventPhase === EventPhase.AT_TARGET)) {
                    handlers[i].listenerFunction(event);
                }
                if (handlers[i].once) {
                    remove.push(handlers[i]);
                }
                if (!event.isImmediatePropagating) {
                    break;
                }
            }
            i = remove.length;
            while (--i >= 0) {
                this.handlers[event.type].splice(this.handlers[event.type].indexOf(remove[i]), 1);
            }
        }
    }

}