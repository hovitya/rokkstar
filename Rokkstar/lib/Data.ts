﻿///<reference path='Base.ts'/>
import core = require("./Base");
        export class FieldBase extends core.PropertyChangeNotifier {
            private _superGlobal: string = "";
            private storageEventHandler: (any) => void;
            isStorageChange: boolean = false;
            private _defaultValue: string = undefined;

            get defaultValue():string {
                return this._defaultValue;
            }

            set defaultValue(val: string) {
                if (this._defaultValue == val) return;
                var old = this._defaultValue;
                this._defaultValue = val;
                if (this.superGlobal !== "") {
                    var storedValue = window.localStorage.getItem(this.superGlobal);
                    if (storedValue === null) {
                        this.setSerializedValue(val);
                    }
                }
                this.notifyPropertyChanged("defaultValue", old, val);
            }

            constructor() {
                super();
                var that = this;
                this.storageEventHandler = function(event: any): void {
                    if (that.superGlobal !== "" && that.superGlobal === event.key) {
                        that.isStorageChange = true;
                        that.setSerializedValue(event.newValue);
                        that.isStorageChange = false;
                    }
                };
            }

            set superGlobal(value: string) {
                if (value == this._superGlobal) return;
                var old = this._superGlobal;
                this._superGlobal = value;
                window.removeEventListener("storage", this.storageEventHandler);
                if (value !== "") {
                    window.addEventListener("storage", this.storageEventHandler);
                    var storedValue = window.localStorage.getItem(value);
                    if (storedValue !== null) {
                        this.isStorageChange = true;
                        this.setSerializedValue(storedValue);
                        this.isStorageChange = false;
                    } else if(this.defaultValue !== undefined){
                        this.setSerializedValue(this.defaultValue);
                    }
                }
                this.notifyPropertyChanged("superGlobal", old, value);
            }

            get superGlobal() {
                return this._superGlobal;
            }

            storeValue() {
                if (this.superGlobal !== "" && !this.isStorageChange) {
                    window.localStorage.setItem(this.superGlobal,this.getSerializedValue());
                }
            } 

            getSerializedValue():string {
                return this['value'];
            }
            setSerializedValue(val:string) {
                this['value'] = val;
            }
        }

        export class BooleanField extends FieldBase{
            private _value:boolean;
            set value(val:boolean) {
                val = core.PrimitiveTypeTools.ensureBool(val);
                if (val === this._value) return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            }
            get value(): boolean {
                return this._value;
            }
        }

        export class IntegerField extends FieldBase {
            private _value: number;
            set value(val: number) {
                val = core.PrimitiveTypeTools.ensureInteger(val);
                if (val === this._value) return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            }
            get value(): number {
                return this._value;
            }
        }

        export class FloatField extends FieldBase {
            private _value: number;
            set value(val: number) {
                val = core.PrimitiveTypeTools.ensureFloat(val);
                if (val === this._value) return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            }
            get value(): number {
                return this._value;
            }
        }

        export class StringField extends FieldBase {
            private _value: number;
            set value(val: number) {
                if (val == this._value) return;
                var old = this._value;
                this._value = val;
                this.storeValue();
                this.notifyPropertyChanged("value", old, val);
            }
            get value(): number {
                return this._value;
            }
        }