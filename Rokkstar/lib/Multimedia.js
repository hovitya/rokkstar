﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
///<reference path='Base.ts'/>
///<reference path='Events.ts'/>
var core;
(function (core) {
    var Audio = (function (_super) {
        __extends(Audio, _super);
        function Audio() {
            _super.call(this);
            this.notifier = new core.PropertyChangeNotifier();
            this._volume = 100;
            this._masterVolume = 100;
            this._autoplay = false;
            this.fadeVolume = 100;
            this.audioObject = document.createElement('audio');
            var that = this;
            this.audioObject.addEventListener('durationchange', function (event) {
                var old = that._length;
                that._length = that.audioObject.duration;
                that.notifyPropertyChanged("length", old, that._length);
            });
            this.audioObject.addEventListener('play', function (event) {
                var e = new core.RokkEvent("play");
                e.originalEvent = event;
                that.dispatchEvent(e);
            });
        }

        Object.defineProperty(Audio.prototype, "volume", {
            get: function () {
                return this._volume;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (value > 100) {
                    core.DevConsole.warn("Audio volume has to be between 0 and 100");
                    value = 100;
                }
                if (value < 0) {
                    core.DevConsole.warn("Audio volume has to be between 0 and 100");
                    value = 0;
                }
                if (value === this._volume)
                    return;
                var old = this._volume;
                var oldReal = this.realVolume;
                this._volume = value;
                var newReal = this.realVolume;
                this.applyVolume();
                this.notifyPropertyChanged("volume", old, value);
                this.notifyPropertyChanged("realVolume", oldReal, newReal);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Audio.prototype, "masterVolume", {
            get: function () {
                return this._masterVolume;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureInteger(value);
                if (value > 100) {
                    core.DevConsole.warn("Audio master volume has to be between 0 and 100");
                    value = 100;
                }
                if (value < 0) {
                    core.DevConsole.warn("Audio master volume has to be between 0 and 100");
                    value = 0;
                }
                if (value === this._masterVolume)
                    return;
                var old = this._masterVolume;
                var oldReal = this.realVolume;
                this._masterVolume = value;
                var newReal = this.realVolume;
                this.applyVolume();
                this.notifyPropertyChanged("masterVolume", old, value);
                this.notifyPropertyChanged("realVolume", oldReal, newReal);
            },
            enumerable: true,
            configurable: true
        });


        Object.defineProperty(Audio.prototype, "autoplay", {
            get: function () {
                return this._autoplay;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureBool(value);
                if (value === this._autoplay)
                    return;
                this._autoplay = value;
                this.notifyPropertyChanged("autoplay", !value, value);
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Audio.prototype, "realVolume", {
            get: function () {
                return Math.round(this._volume * (this._masterVolume / 100) * (this.fadeVolume / 100));
            },
            enumerable: true,
            configurable: true
        });

        Object.defineProperty(Audio.prototype, "length", {
            get: function () {
                return this._length;
            },
            enumerable: true,
            configurable: true
        });

        Audio.prototype.applyVolume = function () {
            this.audioObject.volume = Math.round(this.realVolume / 100);
        };

        Audio.prototype._play = function () {
        };

        Audio.prototype._stop = function () {
        };

        Audio.prototype.play = function () {
            this._play();
        };

        Audio.prototype.attach = function (observer) {
            this.notifier.attach(observer);
        };

        Audio.prototype.detach = function (observer) {
            this.notifier.detach(observer);
        };

        Audio.prototype.notifyPropertyChanged = function (property, oldVal, newVal) {
            this.notifier.notifyPropertyChanged(property, oldVal, newVal);
        };
        return Audio;
    })(core.EventDispatcher);
    core.Audio = Audio;
})(core || (core = {}));
