///<reference path='Base.ts'/>
///<reference path='Events.ts'/>
import core = require("./Base");
import events = require("./Events"); 


        export interface ICursor {
            next(): boolean;
            prev(): boolean;
            current(): any;
            hasNext(): boolean;
            hasPrev(): boolean;
            rewind(): void;
            length: number;
            seek(index: number): boolean;
            remove(): ICursor;
            add(item: any): ICursor;
        }

        export class IICursor implements core.IInterfaceChecker {
            className: string = "IICursor";
            methodNames: string[] = ["next", "prev", "current", "hasNext", "hasPrev", "rewind", "seek", "remove", "add"];
            inheritsFrom: core.IInterfaceChecker[] = [];
        }

        export interface IList extends events.IEventDispatcher{
            indexOf(item: any, fromIndex?: number): number;
            push(item: any): IList;
            pop(): any;
            length: number;
            getItemAt(index: number):any;
            removeItemAt(index: number): IList;
            addItemAt(index: number, item: any): IList;
            toArray(): any[];
        }

        export class IIList implements core.IInterfaceChecker {
            className: string = "IIList";
            methodNames: string[] = ["indexOf", "push", "pop", "getItemAt", "removeItemAt", "addItemAt", "toArray"];
            inheritsFrom: core.IInterfaceChecker[] = [new events.IIEventDispatcher()];
        }

        export interface IIterable {
            createCursor(): ICursor;
        }

        export class IIIterable {
            className: string = "IIIterable";
            methodNames: string[] = ["createCursor"];
            inheritsFrom: core.IInterfaceChecker[] = [];
        }
        
        export interface ICollectionView extends IIterable, events.IEventDispatcher{
            filterFunction: (item: any) => boolean;
            sortFunction: (itemA: any, itemB: any) => number;
            length: number;
            contains(item: any): boolean;
            disableAutoUpdate();
            enableAutoUpdate();
            refresh();
        }

        export class IICollectionView implements core.IInterfaceChecker {
            className: string = "IICollectionView";
            methodNames: string[] = ["filterFunction", "sortFunction", "contains", "disableAutoUpdate", "enableAutoUpdate", "refresh"];
            inheritsFrom: core.IInterfaceChecker[] = [new IIIterable(), new events.IIEventDispatcher()];
        }



        export class CollectionEvent extends events.RokkEvent{
            static ITEM_ADDED = "item_added";
            static ITEM_REMOVED = "item_removed";
            static REFRESHED = "refreshed";
            position: number;
            affectedItem: any;

            constructor(type: string, bubbles: boolean, cancelable: boolean, position?: number, affectedItem?: any) {
                super(type, bubbles, cancelable);
                if (position) this.position = position;
                if (affectedItem) this.affectedItem = affectedItem;
            }
        }

        export class List extends events.EventDispatcher implements IList, core.IPropertyChangeNotifier {
            public objectId: number;
            private source: any[];
            private observers: core.IPropertyObserver[];
            constructor(source?: any[]) {
                super();
                if (source) {
                    var src = source.slice(0);
                    this.source = src.filter((el: any, i: number, a: any[]): boolean => {
                        return i == a.indexOf(el);
                    }, this);
                } else {
                    this.source = new Array();
                }
            }

            indexOf(item: any,fromIndex?:number): number {
                if (fromIndex) return this.source.indexOf(item);
                return this.source.indexOf(item, fromIndex);
            }

            lastIndexOf(item: any, fromIndex?: number): number {
                if (fromIndex) return this.source.lastIndexOf(item);
                return this.source.lastIndexOf(item, fromIndex);
            }

            push(item: any): IList {
                if (this.source.indexOf(item) != -1) {
                    //remove previous
                    this.removeItemAt(this.source.indexOf(item));
                }
                this.source.push(item);
                this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, this.source.length - 1, item));
                return this;
            }

            pop():any {
                var item: any = this.source.pop();
                this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, this.source.length, item));
                return item;
            }

            get length(): number {
                return this.source.length;
            }


            removeItemAt(index: number) :IList {
                if (!this.checkIndex(index)) throw new Error("Index out of bounds.");
                var item: any = this.source[index];
                this.source.splice(index, 1);
                this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, index, item));
                return this;
            }

            getItemAt(index: number): any {
                if (!this.checkIndex(index)) throw new Error("Index out of bounds.");
                return this.source[index];
            }

            private checkIndex(index: number) :boolean {
                if (index < 0 || index >= this.source.length) {
                    return false;
                }
                return true;
            }

            addItemAt(index: number, item: any): IList {
                if (this.source.indexOf(item) != -1) {
                    //remove previous
                    this.removeItemAt(this.source.indexOf(item));
                }
                if (index === this.source.length) {
                    this.source.push(item);
                    this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, index, item));
                    return;
                }
                if (!this.checkIndex(index)) throw new Error("Index out of bounds.");
                this.source.splice(index, 0, item);
                this.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, index, item));
                return this;
            }

            toArray(): any[] {
                return this.source.slice(0);
            }

            attach(observer: core.IPropertyObserver) {
                this.observers.push(observer);
            }

            detach(observer: core.IPropertyObserver) {
                if (this.observers.indexOf(observer) !== -1) {
                    this.observers.splice(this.observers.indexOf(observer), 1);
                }
            }

            notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
                for (var i = 0; i < this.observers.length; i++) {
                    this.observers[i].update(this, property, oldValue, newValue);
                }
            }

    
        }

        export class ListItemArrayAccess {
            items: any[] = [];
        }

        export class ListCollectionView extends events.EventDispatcher implements ICollectionView, core.IPropertyChangeNotifier {
            public objectId: number;
            private isAutoUpdate = true;
            private _items: any[];
            private set items(value: any[]) {
                this.itemsAccess.items = value;
                this._items = value;
            }
            private get items(): any[] {
                return this._items;
            }
            private itemsAccess: ListItemArrayAccess = new ListItemArrayAccess();
            private itemIndexes: any[] = [];
            private _source: IList;
            private observers: core.IPropertyObserver[] = [];
            private _filterFunction: (item: any) => boolean;
 
            get length(): number {
                return this.items.length;
            }
            get source(): IList {
                return this._source;
            }

            get filterFunction(): (item: any) => boolean {
                return this._filterFunction;
            }

            set filterFunction(value: (item: any) => boolean) {
                this._filterFunction = value;
                if (this.isAutoUpdate) this.refresh();
            }

            private _sortFunction: (itemA: any, itemB: any) => number;

            get sortFunction(): (itemA: any, itemB: any) => number {
                return this._sortFunction;
            }

            set sortFunction(value: (itemA: any, itemB: any) => number) {
                this._sortFunction = value;
                if (this.isAutoUpdate) this.refresh();
            }


            constructor(source?: IList) {
                super();
                this.items = [];
                if (source !== undefined) {
                    this._source = source;
                } else {
                    this._source = new List();
                }
                var that: ListCollectionView = this;
                
                this.source.addEventListener(CollectionEvent.ITEM_ADDED, (event: CollectionEvent) => {
                    if (that.filterFunction && !that.filterFunction(event.affectedItem)) {
                        return;
                    }
                    if (!that.sortFunction) {
                        that.items.splice(event.position, 0, event.affectedItem);
                        that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, event.position, event.affectedItem));
                    } else {
                        var i = 0;
                        while (i < that.items.length && that.sortFunction(event.affectedItem, that.items[i]) > -1) {
                            i++;
                        }
                        that.items.splice(i, 0, event.affectedItem);
                        that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_ADDED, false, true, i, event.affectedItem));
                    }
                    that.notifyPropertyChanged('length', that.items.length - 1, that.items.length);
                });
                this.source.addEventListener(CollectionEvent.ITEM_REMOVED, (event: CollectionEvent) => {
                    if (that.filterFunction && !that.filterFunction(event.affectedItem)) {
                        return;
                    }
                    for (var i: number = 0; i < that.items.length; i++) {
                        if (that.items[i] === event.affectedItem) {
                            that.items.splice(i, 1);
                            that.dispatchEvent(new CollectionEvent(CollectionEvent.ITEM_REMOVED, false, true, i, event.affectedItem));
                            that.notifyPropertyChanged('length', that.items.length + 1, that.items.length);
                            return;
                        }
                    }
                });
                this.refresh();
            }

            refresh() {
                var oldItemsLength = this.items.length;

                this.items = [];

                for (var i = 0; i < this.source.length; i++) {
                    if (!this.filterFunction || this.filterFunction(this.source.getItemAt(i))) {
                        this.items.push(this.source.getItemAt(i));
                    }
                }

                if (this.sortFunction) {
                    this.items.sort(this.sortFunction);
                }
                this.dispatchEvent(new CollectionEvent(CollectionEvent.REFRESHED, false, true, -1, null));
                if (oldItemsLength !== this.items.length) {
                    this.notifyPropertyChanged("length", oldItemsLength, this.items.length);
                }
            }

            contains(item: any): boolean {
                if (this.items.indexOf(item) === -1) return false;
                return true;
            }

            disableAutoUpdate() {
                this.isAutoUpdate = false;
            }

            enableAutoUpdate() {
                this.isAutoUpdate = true;
            }

            attach(observer: core.IPropertyObserver) {
                this.observers.push(observer);
            }

            detach(observer: core.IPropertyObserver) {
                if (this.observers.indexOf(observer) !== -1) {
                    this.observers.splice(this.observers.indexOf(observer), 1);
                }
            }

            notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
                for (var i = 0; i < this.observers.length; i++) {
                    this.observers[i].update(this, property, oldValue, newValue);
                }
            }

            createCursor(): ICursor {
                return new ListCollectionViewCursor(this, this.itemsAccess);
            }
        }

        export class ListCollectionViewCursor implements ICursor {
            private view: ListCollectionView;
            public currentPosition: number;
            private access: ListItemArrayAccess;
            constructor(list: ListCollectionView, access: ListItemArrayAccess) {
                this.view = list;
                this.access = access;
            }

            next(): boolean {
                this.currentPosition++;
                if (this.currentPosition < this.length) {
                    return true;
                }
                return false;
            }

            prev(): boolean {
                this.currentPosition--;
                if (this.currentPosition >= 0) {
                    return true;
                }
                return false;
            }

            seek(position: number): boolean {
                if (position >= 0 && position < this.length) {
                    this.currentPosition = position;
                    return true;
                }
                return false;
            }

            get length(): number {
                return this.view.length;
            }

            current(): any {
                return this.access.items[this.currentPosition];
            }

            hasNext(): boolean {
                return (this.currentPosition < this.length - 1);
            }

            hasPrev(): boolean {
                return (this.currentPosition > 0);
            }

            rewind(): void {
                this.currentPosition = 0;
            }

            remove(): ICursor {
                this.view.source.removeItemAt(this.currentPosition);
                return this;
            }

            add(item: any): ICursor {
                this.view.source.addItemAt(this.currentPosition, item);
                return this;
            }
        }
