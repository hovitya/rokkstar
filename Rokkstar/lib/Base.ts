
    export interface IInterfaceChecker {
        className: string;
        methodNames: string[];
        inheritsFrom: IInterfaceChecker[];
    }

    export interface IPropertyObserver {
        update(observable: IPropertyChangeNotifier, property: string, oldValue: any, newValue: any);
    }

    export class IIPropertyObserver implements IInterfaceChecker {
        className: string = "IIPropertyObserver";
        methodNames: string[] = ["update"];
        inheritsFrom: IInterfaceChecker[] = [];
    }


    export interface IPropertyChangeNotifier {
        objectId:number;
        attach(observer: IPropertyObserver): void;
        detach(observer: IPropertyObserver): void;
        notifyPropertyChanged(property: string, oldValue: any, newValue: any);
    }

    export class DevConsole {
        static warn(message: any) {
            if(!window['_rokkSilentMode']) console.warn(message);
        }
        static log(message: any) {
            if (!window['_rokkSilentMode']) console.log(message);
        }
    }

    
    
    export class DataBindingManager implements IPropertyObserver {
        private static _instance: DataBindingManager;
        private bindings: any[] = [];
        public static GetInstance():DataBindingManager {
            if (DataBindingManager._instance === undefined)
                DataBindingManager._instance = new DataBindingManager();
            return DataBindingManager._instance;
        }

        public bind(source: IPropertyChangeNotifier, property: string, target: any, targetProperty: string, filter: string[]= [], context:any = {}) {
            var parsedFilters = [];
            for (var i in filter) {
                var matches = filter[i].match(/^([^(]+)\(([^)]+)+\)$/);
                var func = ReflectionUtil.ObjectFactory(matches[1],context);
                var params = matches[2].split(',');
                parsedFilters.push({ f: func, p: params });
            }
            if (InterfaceChecker.implementsInterface(source, new InterfaceChecker(new IIPropertyChangeNotifier()))) {
                if (source.objectId === undefined) {
                    this.bindings.push({});
                    source.objectId = this.bindings.length - 1;
                }
                if (this.bindings[source.objectId][property] === undefined) this.bindings[source.objectId][property] = [];

                DevConsole.log(parsedFilters);
                this.bindings[source.objectId][property].push({t:target,tp:targetProperty,f:parsedFilters});
                source.attach(this);
            } else {
                DevConsole.warn("The following object is not an IPropertyChangeNotifier, so property binding won't work:");
                DevConsole.log(source);
            }
            target[targetProperty] = this.applyFilters(source[property],parsedFilters);
        }

        update(observable: IPropertyChangeNotifier, property: string, oldValue: any, newValue: any) {
            if (observable.objectId === undefined || this.bindings[observable.objectId] === undefined || this.bindings[observable.objectId][property] === undefined) return;
            for (var i in this.bindings[observable.objectId][property]) {
                this.bindings[observable.objectId][property][i].t[this.bindings[observable.objectId][property][i].tp] = this.applyFilters(newValue, this.bindings[observable.objectId][property][i].f);
            }
        }

        private applyFilters(value: any, filters: any[]):any {
            for (var i in filters) {
                value = filters[i].f.apply({},[value].concat(filters[i].p));
            }
            return value;
        }
    }

    export class MetaPropertyTypes {
        static INTEGER: string = "integer";
        static FLOAT: string = "float";
        static STRING: string = "string";
        static ARRAY: string = "array";
    }

    export class MetaProperty {
        type: string;
        name: string;
        listType: string;
        constructor(type: string, name: string, listType?: string) {
            this.type = type;
            this.name = name;
            if (listType !== null && listType !== undefined) {
                this.listType = listType;
            }
        }
    }

    export interface IMetadata {
        properties: Object;
        enabled: boolean;
    }

    export class PropertyChangeNotifier implements IPropertyChangeNotifier {
        objectId: number;
        private observers: IPropertyObserver[] = [];

        attach(observer: IPropertyObserver) {
            this.observers.push(observer);
        }

        detach(observer: IPropertyObserver) {
            if (this.observers.indexOf(observer) !== -1) {
                this.observers.splice(this.observers.indexOf(observer), 1);
            }
        }

        notifyPropertyChanged(property: string, oldValue: any, newValue: any) {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].update(this, property, oldValue, newValue);
            }
        }
    }

    export class IIPropertyChangeNotifier {
        className: string = "IIPropertyChangeNotifier";
        methodNames: string[] = ["attach", "detach", "notifyPropertyChanged"];
        inheritsFrom: IInterfaceChecker[] = [];
    }

    export interface IClass {
        new (param:any): any; 
    }

    export class ReflectionUtil {
        static ClassFactoryFromString(className: string, currentScope?: Object, parameter: any = undefined) {
            var currentObject: IClass = ReflectionUtil.ObjectFactory(className, currentScope);
            return new currentObject(parameter);
        }

        static ObjectFactory(className: string, currentScope?: Object): IClass {
            if (currentScope === null || currentScope === undefined) {
                currentScope = window;
            }
            var parts: string[] = className.split(".");
            var currentObject = currentScope;
            for (var i in parts) {
                if (currentObject[parts[i]] === undefined || currentObject[parts[i]] === undefined)
                    throw new Error("Class reflection error: " + className + " not found.");
                currentObject = currentObject[parts[i]];
            }
            return <IClass>currentObject;
        }

        static ClassExists(className: string, currentScope?: Object) {
            if (currentScope === null || currentScope === undefined) {
                currentScope = window;
            }
            var parts: string[] = className.split(".");
            var currentObject = currentScope;
            for (var i in parts) {
                if (currentObject[parts[i]] === undefined || currentObject[parts[i]] === undefined)
                    return false;
                currentObject = currentObject[parts[i]];
            }
            return true;
        }
    }

    export class InterfaceChecker {
        name: string;
        methods: string[];

        constructor(object: IInterfaceChecker) {
            this.name = object.className;
            this.methods = [];
            var i, len: number;
            for (i = 0, len = object.methodNames.length; i < len ; i++) {
                this.methods.push(object.methodNames[i]);
            };
        }

        static ensureImplements(object: any, targetInterface: InterfaceChecker) {
            var i, len: number;
            for (i = 0, len = targetInterface.methods.length; i < len; i++) {
                var method: string = targetInterface.methods[i];
                if (!object[method] || typeof object[method] !== 'function') {
                    throw new Error("Function InterfaceChecker.ensureImplements: ' + ' object does not implement the " + targetInterface.name + " interface. Method " + method + " was not found");
                }
            }
        }
        static implementsInterface(object: any, targetInterface: InterfaceChecker): boolean {
            var i, len: number;
            for (i = 0, len = targetInterface.methods.length; i < len; i++) {
                var method: string = targetInterface.methods[i];
                if (!object[method] || typeof object[method] !== 'function') {
                    return false;
                }
            }
            return true;
        }
    }


    export class PrimitiveTypeTools {
        static ensureInteger(value: any) {
            var ensured: number = parseInt(value);
            if (ensured === NaN) {
                throw new Error("Given value is not an integer.");
            }
            return ensured;
        }

        static ensureFloat(value: any) {
            var ensured: number = parseFloat(value);
            if (ensured === NaN) {
                throw new Error("Given value is not a float.");
            }
            return ensured;
        }

        static ensureBool(value: any) {
            if (value === "true" ||
                value === true ||
                value == 1) {
                return true;
            } else if (value === "false" ||
                value === false ||
                value == 0) {
                return false;
            }
            throw new Error("Given value is not a boolean.");
        }
    }

    export class Point {
        x: number;
        y: number;

        constructor(x: number, y: number) {
            this.x = x;
            this.y = y;
        }

        toJSON(): any {
            return {x:this.x,y:this.y};
        }

        static fromJSON(json:any):Point {
            return new Point(json.x,json.y);
        }
    }

    export class MathTools {
        static toDegree (radians: number): number {
            var pi = Math.PI;
            var deg = (radians) * (180 / pi);
            return Number(deg);
        }

        static toRadians (degree:number): number {
            var pi = Math.PI;
            var rad = degree * (pi / 180);
            return Number(rad);
        }
    }