﻿///<reference path='Base.ts'/>
///<reference path='Events.ts'/>
module core {
    export class Audio extends EventDispatcher implements IPropertyChangeNotifier {
        public objectId: number;
        private notifier: PropertyChangeNotifier = new PropertyChangeNotifier();
        private audioObject: HTMLAudioElement;
        private _length: number;

        private _volume: number = 100;

        public set volume(value: number) {
            value = PrimitiveTypeTools.ensureInteger(value);
            if (value > 100) {
                DevConsole.warn("Audio volume has to be between 0 and 100");
                value = 100;
            }
            if (value < 0) {
                DevConsole.warn("Audio volume has to be between 0 and 100");
                value = 0;
            }
            if (value === this._volume) return;
            var old = this._volume;
            var oldReal = this.realVolume;
            this._volume = value;
            var newReal = this.realVolume;
            this.applyVolume();
            this.notifyPropertyChanged("volume", old, value);
            this.notifyPropertyChanged("realVolume", oldReal, newReal);
        }

        public get volume():number {
            return this._volume;
        }

        private _masterVolume: number = 100;

        public set masterVolume(value: number) {
            value = PrimitiveTypeTools.ensureInteger(value);
            if (value > 100) {
                DevConsole.warn("Audio master volume has to be between 0 and 100");
                value = 100;
            }
            if (value < 0) {
                DevConsole.warn("Audio master volume has to be between 0 and 100");
                value = 0;
            }
            if (value === this._masterVolume) return;
            var old = this._masterVolume;
            var oldReal = this.realVolume;
            this._masterVolume = value;
            var newReal = this.realVolume;
            this.applyVolume();
            this.notifyPropertyChanged("masterVolume", old, value);
            this.notifyPropertyChanged("realVolume", oldReal, newReal);
        }

        public get masterVolume(): number {
            return this._masterVolume;
        }

        private _autoplay: boolean = false;

        public set autoplay(value: boolean) {
            value = PrimitiveTypeTools.ensureBool(value);
            if (value === this._autoplay) return;
            this._autoplay = value;
            this.notifyPropertyChanged("autoplay", !value, value);
        }

        public get autoplay(): boolean {
            return this._autoplay;
        }

        private fadeVolume: number = 100;

        public get realVolume():number {
            return Math.round(this._volume * (this._masterVolume / 100) * (this.fadeVolume / 100));
        }

        public get length(): number {
            return this._length;
        }

        private applyVolume() {
            this.audioObject.volume = Math.round(this.realVolume / 100);
        }

        private _play() {
            
        }

        private _stop() {
            
        }

        public play() {
            this._play();
        }
        

        constructor() {
            super();
            this.audioObject = document.createElement('audio');
            var that = this;
            this.audioObject.addEventListener('durationchange', function (event) {
                var old = that._length;
                that._length = that.audioObject.duration;
                that.notifyPropertyChanged("length", old, that._length);
            });
            this.audioObject.addEventListener('play', function (event) {
                var e = new RokkEvent("play");
                e.originalEvent = event;
                that.dispatchEvent(e);
            });
        }

        public attach(observer: IPropertyObserver) {
            this.notifier.attach(observer);
        }

        public detach(observer: IPropertyObserver) {
            this.notifier.detach(observer);
        }

        public notifyPropertyChanged(property: string, oldVal: any, newVal: any) {
            this.notifier.notifyPropertyChanged(property, oldVal, newVal);
        }
    }         
}