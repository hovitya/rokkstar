﻿///<reference path='Events.ts'/>
///<reference path='Collections.ts'/>
///<reference path='../third-party/definitions/Tween.d.ts'/>
module core {
    export module animations {
        export class SimplePropertyAnimation extends core.EventDispatcher {
            private tween: TWEEN.Tween;

            public propertyName: string = "";

            private _duration: number;

            public set duration(value: number) {
                value = PrimitiveTypeTools.ensureInteger(value);
                this._duration = value;
            }

            public get duration(): number {
                return this._duration;
            }

            private _target: any;

            public get target(): any {
                return this._target;
            }

            public set target(value: any) {
                this._target = value;
            }

            public get currentValue(): number {
                if (this.target !== undefined
                    && this.target !== null
                    && this.propertyName !== "") {
                        return this.target[this.propertyName];
                }
                return 0;
            }

            public set currentValue(value: number) {
                
                if (this.target !== undefined
                    && this.target !== null
                    && this.propertyName !== "") {
                        this.target[this.propertyName] = value;
                }
            }

            public from: number = undefined;

            public to: number = 0.0;
            //public 

            constructor(html?: HTMLElement) {
                super();
                this.tween = new TWEEN.Tween(this);
            }

            public run() {
                if (this.from !== undefined
                    && this.from !== null) {
                        this.currentValue = this.from;
                }
                var to = {};
                to[this.propertyName] = this.to;
                this.tween
                    .to(to, this.duration)
                    .start();
            }


            public stop() {
                this.tween.stop();
            }
            
        }
    }
}