///<reference path='ComponentBase.ts'/>
import core = require("./Base");
import components = require("./ComponentBase"); 
import events = require("./Events");
import collections = require("./Collections");
 
    export module filters { 
        export function Calculate(value: any, expr: string) {
            var expression = "var x = " + parseInt(value, 10) + "; return " + expr + ";";
            return new Function(expression)();
        }
    }

        export function run(modules:any) {
            return parseApplication(<HTMLElement>document.getElementsByTagName("body")[0], modules);
        }

        class DelayedProperty {
            context: Object;
            target: Object;
            propertyName: string;
            stringValue: string;
            listValue: any[];
            type: number = -1;
            idList: Object;
        }

        var reservedParameterNames: string[] = ["role", "class", "property", "context", "key", "id", "type"];



        function dashToCamel(str:string) {
            return str.replace(/\W+(.)/g, function (x, chr) {
                return chr.toUpperCase();
            })
        }

        function parseApplication(node: HTMLElement, modules:any = {}) {
            var app: components.IApplication;
            //Parsing application
            if (node.getAttribute("data-type") === undefined || node.getAttribute("data-type") === null) {
                //Using default class
                app = new components.Application(node);
            } else {
                var className = node.getAttribute("data-class");
                app = core.ReflectionUtil.ClassFactoryFromString(className);
                core.InterfaceChecker.ensureImplements(app, new core.InterfaceChecker(new components.IIAplication()));
            }

   
            var delayedProperties: DelayedProperty[] = [];
            var idList: Object = {};

            parseChildren(node, app, app, delayedProperties, idList, modules);

            //Parsing properties
            parseProperties(node, app, app, delayedProperties, idList);

            //Clear definition content
            //node.innerHTML = '';

            //Add real content
            //node.appendChild(app.domElement);

            //Apply delayed properties
            applyProperties(delayedProperties,app.resourceDictionary, modules);

            app.addToDisplayList(); 
            app.dispatchEvent(new events.RokkEvent("parsed"));
            return app;
        }

        export function parseResource(dom: HTMLElement, context: Object, activeResourceDictionary: components.ResourceDictionary,modules:any): any {
            var delayedProperties: DelayedProperty[] = [];
            var instance: any = parseInstance(dom, context, delayedProperties, {}, modules, false);
            applyProperties(delayedProperties,activeResourceDictionary, modules);
            return instance;
        }


        function parseInstance(node: HTMLElement, context: Object, delayedProperties: DelayedProperty[], idList: Object, modules:any, parseId: boolean = true): any {
            var instance: any;
            if (node.getAttribute("data-type") === undefined || node.getAttribute("data-type") === null) {
                //Using default class
                instance = new components.HTMLElementWrapper(node);
            } else {
                var className = node.getAttribute("data-type");
                instance = core.ReflectionUtil.ClassFactoryFromString(className,modules,node);
            }
            if (parseId) {
                var key = node.getAttribute("data-id");

                //Registering key
                if (key !== undefined && key !== null && key !== "") {
                    if (idList[key] !== undefined)
                        throw new Error("Duplicated id: " + key);
                    idList[key] = instance;
                    context[key] = instance;
                }
            }

            //Parsing properties
            parseProperties(node, instance, context, delayedProperties, idList);

            //Parsing children
            parseChildren(node, instance, context, delayedProperties, idList, modules);


            return instance;
        }

        function applyProperties(properties: DelayedProperty[], activeResourceDictionary: components.ResourceDictionary, modules:any): void {

            properties = properties.reverse();

            for (var i: number = 0; i < properties.length; i++) {
                var target: Object = properties[i].target;
                var propertyName: string = dashToCamel(properties[i].propertyName);
                console.log("apply" + propertyName);
                //if (!target.hasOwnProperty(propertyName))
                //    throw new Error(target + " has no property: " + propertyName);
                if (properties[i].type === 0) {
                    if (properties[i].stringValue[0] !== "$") {
                        target[propertyName] = properties[i].stringValue;
                    } else {
                        var property = properties[i].stringValue.substr(1);
                        var filter = property.split('|')
                        var parts = filter[0].split('.');
                        if (parts.length === 1) {
                            target[propertyName] = properties[i].context[parts[0]];
                        } else {
                            core.DataBindingManager.GetInstance().bind(properties[i].context[parts[0]], parts[1], target, propertyName,<string[]>filter.slice(1),modules);
                        }
                        
                    }
                    
                } else {
                    if (target[propertyName] instanceof Array) {
                        //Assign as array
                        target[propertyName] = properties[i].listValue;
                    } else if (target[propertyName] !== undefined && target[propertyName] !== null && core.InterfaceChecker.implementsInterface(target[propertyName], new core.InterfaceChecker(new collections.IIList()))) {
                        //Assign list
                        target[propertyName] = new collections.List(properties[i].listValue);
                        /*for (var j: number = 0; j < properties[i].listValue.length; j++) {
                            target[propertyName].push(properties[i].listValue[j]);
                        }*/
                    } else {
                        if (properties[i].listValue.length > 1)
                            throw new Error("Trying to add multiple value to " + target + " property " + propertyName + " but it supports only one.");
                        target[propertyName] = properties[i].listValue[0];
                    }
                }
            }
        }

        function parseResources(node: HTMLElement, context: Object, delayedProperties: DelayedProperty[], idList: Object,modules:any): components.ResourceDictionary {
            var dict: components.ResourceDictionary = new components.ResourceDictionary();
            for (var i: number = 0; i < node.childElementCount; i++) {
                var node: HTMLElement = <HTMLElement>node.children[i];
                var key: string = node.getAttribute("data-key");
                if (key === "" || key === undefined || key === null) {
                    throw new Error("Every resource item requires key.");
                }

                dict.addResource(key, parseInstance(<HTMLElement>node, context, delayedProperties, idList, modules));
            }
            return dict;
        }

        function parseProperties(node: HTMLElement, object: Object, context: Object, delayedProperties: DelayedProperty[], idList: Object) {
            for (var i: number = 0; i < node.attributes.length; i++) {
                if (node.attributes[i].name.search("data-when-") === 0) {
                    var eventName: string = node.attributes[i].name.replace("data-when-", "");
                    eventName = dashToCamel(eventName);
                    var func = core.ReflectionUtil.ObjectFactory(node.attributes[i].value, context);
                    if ((<any>object).addEventListener !== undefined) {
                        (<events.IEventDispatcher>object).addEventListener(eventName, function (e: any) {
                            func.apply(context,[e]);
                        });
                    } else {
                        core.DevConsole.warn("Trying to register event ("+eventName+") listener for a non event dispatcher.");
                    }
                }else if (node.attributes[i].name.search("data-") === 0) {
                    var parameterName: string = node.attributes[i].name.substring(5);
                    var parameterName: string = node.attributes[i].name.replace("data-", "");
                    if (reservedParameterNames.indexOf(parameterName) === -1) {
                        var delayedProperty: DelayedProperty = new DelayedProperty();
                        delayedProperty.context = context;
                        delayedProperty.idList = idList;
                        delayedProperty.target = object;
                        delayedProperty.type = 0;
                        delayedProperty.propertyName = parameterName;
                        delayedProperty.stringValue = node.attributes[i].value;
                        delayedProperties.push(delayedProperty);
                    }
                }
            }
        }

        function parseSetter(node: HTMLElement, object: Object, context: Object, delayedProperties: DelayedProperty[], idList: Object, modules:any) {
            var delayedProperty: DelayedProperty = new DelayedProperty();
            delayedProperty.context = context;
            delayedProperty.idList = idList;
            delayedProperty.target = object;
            delayedProperty.propertyName = node.getAttribute("data-property");
            if (delayedProperty.propertyName === null || delayedProperty.propertyName === undefined || delayedProperty.propertyName === "") {
                throw new Error("Invalid property name.");
            }
            if (node.children.length === 0) {
                delayedProperty.type = 0;
                delayedProperty.stringValue = node.textContent;
            } else if (node.children.length === 1) {
                delayedProperty.type = 1;
                delayedProperty.listValue = [];
                delayedProperty.listValue.push(parseInstance(<HTMLElement>node.children[0], context, delayedProperties, idList, modules));
            } else if (node.children.length > 1) {
                delayedProperty.type = 2;
                delayedProperty.listValue = [];
                for (var i: number = 0; i < node.childElementCount; i++) {
                    delayedProperty.listValue.push(parseInstance(<HTMLElement>node.children[i], context, delayedProperties, idList, modules));
                }
            }
            delayedProperties.push(delayedProperty);
        }

function parseScript(script: string, context: any, modules: any) {
    var moduleNames = "";
    for (var i in modules) {
        if (modules.hasOwnProperty(i)) {
            moduleNames += "var "+i+" = modules['"+i+"']; ";
        }
    }
    var func = eval("(function() { var func = function(modules) { " + moduleNames + script + "}; return func; }())");
    func.apply(context,[modules]);
}
        

        function parseChildren(node: HTMLElement, parentObject: Object, context: Object, delayedProperties: DelayedProperty[], idList: Object, modules:any) {
            for (var i: number = 0; i < node.children.length; i++) {
                var childNode: HTMLElement = <HTMLElement> node.children[i];
                var role = childNode.getAttribute("data-role");
                if (role === undefined || role === null || role === "") {

                    role = "instance";
                    if (childNode.nodeName.toLowerCase() === "script" && childNode.getAttribute("type") === "text/rokk") {
                        role = "script";
                    }    
                    
                }

                switch (role) {
                    case "script":
                        parseScript(childNode.innerHTML,context, modules);
                        break;
                    case "declaration":
                        var instance: any = parseInstance(childNode, context, delayedProperties, idList, modules);
                        break;
                    case "instance":
                        var instance: any = parseInstance(childNode, context, delayedProperties, idList, modules);
                        core.InterfaceChecker.ensureImplements(parentObject, new core.InterfaceChecker(new components.IIDisplayContainer()));
                        core.InterfaceChecker.ensureImplements(instance, new core.InterfaceChecker(new components.IIDisplayObject()));
                        (<components.IDisplayContainer>parentObject).addChild(instance);
                        break;
                    case "setter":
                        parseSetter(childNode, parentObject, context, delayedProperties, idList, modules);
                        break;
                    case "resources":
                        if (core.InterfaceChecker.implementsInterface(parentObject, new core.InterfaceChecker(new components.IIAplication()))) {
                            var resourceDict: components.ResourceDictionary = parseResources(childNode, context, delayedProperties, idList, modules);
                            var app: components.IApplication = <components.IApplication>parentObject;
                            app.resourceDictionary = resourceDict;
                        } else {
                            throw new Error("Resource tag is not allowed here.");
                        }

                        break;
                    case "template":
                        throw new Error("Template tag is not allowed here.");
                        break;

                }

            }

        } 

