﻿///<reference path='../lib/Base.ts'/>
///<reference path='../lib/Events.ts'/>
///<reference path='../lib/Collections.ts'/>
///<reference path='../lib/ComponentBase.ts'/>
///<reference path='../lib/Shapes.ts'/>
///<reference path='lib/Box2d.d.ts'/>
import core = require("./../lib/Base");
import components = require("./../lib/ComponentBase");
import events = require("./../lib/Events");
import shapes = require("./../lib/Shapes");
    export class GameField extends components.Group {
        public worker: Worker;
        public worldCreated: boolean = false;
        public interval;
        private mouseX: number;
        private mouseY: number;
        public simulation: boolean = true;
        private _mouseActive: boolean = true;
        public set mouseActive(value: boolean) {
            value = core.PrimitiveTypeTools.ensureBool(value);
            if (value === this._mouseActive) return;
            var old = this._mouseActive;
            this._mouseActive = value;
            if (!value) {
                this.worker.postMessage({
                    action: "dismissMouseJoint"
                });
            }
            this.notifyPropertyChanged("mouseActive", old, value);
        }
        public get mouseActive():boolean {
            return this._mouseActive;
        }
        constructor(node: HTMLElement) {
            super(node);
            this.worker = new Worker("BuildingBlockGame/PhysicsWorker.js"); 
            var that = this;

            this.worker.addEventListener("message", function (event) {
                for (var i in event.data) {
                    var item: components.IDisplayObject = that.getChildAt(i);
                    item.left = 0;
                    item.top = 0;
                    var rotate = new components.RotateTransform();
                    rotate.angle = core.MathTools.toDegree(event.data[i].angle);
                    var translate: components.TranslateTransform = new components.TranslateTransform();
                    translate.x = event.data[i].position.x - item.width / 2;
                    translate.y = event.data[i].position.y - item.height / 2;
                    item.renderTransformations = [
                        translate,
                        rotate
                    ];
                }
            });

            this.addEventListener("mousemove", function (event) {
                var localPoint: core.Point = that.globalToLocal(new core.Point(event.originalEvent.clientX, event.originalEvent.clientY));
                that.mouseX = localPoint.x;
                that.mouseY = localPoint.y;
                //console.log(that.mouseX,that.mouseY);
                that.worker.postMessage({
                    action: "mouseMove",
                    x: that.mouseX,
                    y: that.mouseY
                });
            });

            window.addEventListener("mousemove", function (event) {
                var p = that.globalToLocal(new core.Point(event.clientX, event.clientY));
                if (p.x < 0 || p.y < 0 || p.x > that.measuredWidth || p.y > that.measuredHeight) {
                    that.worker.postMessage({
                        action: "dismissMouseJoint"
                    });
                }
            });

            this.addEventListener("mouseup", function (event) {
                that.worker.postMessage({
                    action: "dismissMouseJoint"
                });
            });
        }

        earthquake() {
            this.worker.postMessage({
                action: "earthquake"
            });
        }


        updateSize(newWidth:number, newHeight:number) {
            super.updateSize(newWidth, newHeight);
            if(!this.worldCreated) {
                this.worker.postMessage({
                    action: 'createWorld',
                    width: newWidth,
                    height: newHeight
                });
                this.worldCreated = false;

                var that = this;
                this.addEventListener(components.DisplayContainerEvent.ITEM_ADDED, function (event: components.DisplayContainerEvent) {
                    (<GameFieldElement>item).elementId = event.index;
                    that.worker.postMessage({
                        action: "registerBody",
                        id: event.index,
                        x: event.item.left + event.item.width / 2,
                        y: event.item.top + event.item.height / 2,
                        width: event.item.width,
                        height: event.item.height,
                        isCircle: (<GameFieldElement>item).isCircle(),
                        edges: (<GameFieldElement>item).getEdges()
                    });
                });

                for (var i = 0; i < this.childrenNum; i++){
                    var item = this.getChildAt(i);
                    (<GameFieldElement>item).elementId = i;
                    this.worker.postMessage({
                        action: "registerBody",
                        id: i,
                        x: item.left + item.width / 2,
                        y: item.top + item.height / 2,
                        width: item.width,
                        height: item.height,
                        isCircle: (<GameFieldElement>item).isCircle(),
                        edges: (<GameFieldElement>item).getEdges()
                    });
                    (function (item) {
                        item.addEventListener("mousedown", function (event) {
                            if (!that.mouseActive) return;
                            that.worker.postMessage({
                                action: "createMouseJoint",
                                id: (<GameFieldElement>item).elementId,
                                x: that.mouseX,
                                y: that.mouseY
                            });
                        });
                        item.addEventListener("mouseup", function (event) {
                            
                            that.worker.postMessage({
                                action: "dismissMouseJoint"
                            });
                        });
                    })(item);
                }

                //Start rendering
                var that = this;
                this.interval = setInterval(function () {
                    if (that.simulation) {
                        that.worker.postMessage({
                            action: 'step',
                            dt: 1 / 60
                        });
                    }
                }, 1000 / 60);

            }
        }


    }

    export class DropshadowRenderer extends components.Group {
        private _gameField: GameField;
        public dx: number = 1;
        public dy: number = 3;
        public set gameField(gf: GameField) {
            if (this._gameField === gf) return;
            var old = this._gameField;
            this._gameField = gf;
            //Remove old shadows
            var i = this.childrenNum;
            for (var j = i - 1; j >= 0; j--) {
                this.removeChildAt(j);
            }
            //Create new shadows
            for (var i = 0; i < this.gameField.childrenNum; i++) {
                var child: GameFieldElement = <GameFieldElement>this.gameField.getChildAt(i);
                var clone = child.clone();
                clone.background = components.BackgroundParser.parseBackgroundString("black");
                clone.left = 0;
                clone.top = 0;
                clone.alpha = 0.5;
                this.addChild(clone);
                child.attach(this);
            }
            this.notifyPropertyChanged("gameField", old, gf);
            
        }

        public get gameField(): GameField {
            return this._gameField;
        }

        public update(item, property, oldVal, newVal) {
            if (item instanceof GameFieldElement && property === "renderTransformations" && this.gameField.getChildIndex(item)!==-1) {
                var shadow = <GameFieldElement>this.getChildAt(this.gameField.getChildIndex(item));
                var rotate = new components.RotateTransform();
                rotate.angle = (<components.RotateTransform>newVal[1]).angle;
                //rotate.x = (-1) * item.width / 2;
                //rotate.y = (-1) * item.height / 2;
                var translate: components.TranslateTransform = new components.TranslateTransform();
                translate.x = (<components.TranslateTransform>newVal[0]).x + this.dx;
                translate.y = (<components.TranslateTransform>newVal[0]).y + this.dy;
                var translate2: components.TranslateTransform = new components.TranslateTransform();
                translate2.x = this.dx;
                translate2.y = this.dy; 
                shadow.renderTransformations = [
                    translate,
                    rotate
                ];
            }
        }
    }

    export class GameFieldElement extends shapes.Rectangle {
        public elementId: number;
        public origin: GameFieldElement = null;
        public isCircle(): boolean {
            return false;
        }
        public getEdges(): any[]{
            return null;
        }
        constructor(node?: HTMLElement) {
            super(node);
            var that = this;

        }
        public clone(): GameFieldElement {
            var clone = new GameFieldElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        }
    }

    export class RectangleElement extends GameFieldElement {
        public clone(): GameFieldElement {
            var clone = new RectangleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        }
    }

    export class TriangleElement extends GameFieldElement {
        public getEdges(): any[] {
            var leftCorner = new core.Point(0,this.height);
            var rightCorner = new core.Point(this.width, this.height);
            var topCorner = new core.Point(this.width / 2.0, 0);
            return [
                { start: leftCorner.toJSON(), end: topCorner.toJSON() },
                { start: topCorner.toJSON(), end: rightCorner.toJSON() },
                { start: rightCorner.toJSON(), end: leftCorner.toJSON() }
            ];
        }
        draw(w: number, h: number, canvas: HTMLCanvasElement) {
            var edges: any[] = this.getEdges();
            var graphics: CanvasRenderingContext2D = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            graphics.beginPath();
            graphics.moveTo(edges[0].start.x, edges[0].start.y);
            for (var i in edges) {
                graphics.lineTo(edges[i].end.x, edges[i].end.y);
            }
            graphics.closePath();
            graphics.fill();
        }
        public clone(): GameFieldElement {
            var clone = new TriangleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        }
    }

    export class CircleElement extends GameFieldElement {
        public isCircle(): boolean {
            return true;
        }
        draw(w: number, h: number, canvas: HTMLCanvasElement) {
            var x: number = 0;
            var y: number = 0;

            var graphics: CanvasRenderingContext2D = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            var kappa = 0.5522848,
                ox = (w / 2) * kappa,
                oy = (h / 2) * kappa,
                xe = x + w,
                ye = y + h,
                xm = x + w / 2,
                ym = y + h / 2;

            graphics.beginPath();
            graphics.moveTo(x, ym);
            graphics.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
            graphics.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
            graphics.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
            graphics.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
            graphics.closePath();
            graphics.fill();
        }

        public clone(): GameFieldElement {
            var clone = new CircleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        }
    }

    class ElementDescriptor {
        public typeClass: string;
        public width: number;
        public height: number;
        public color: string;
    }

    class Building {
        public elements: ElementDescriptor[];
        public score: number;
        public preview: string;
    }

    class GameData {
        public buildings: Building[];
    }

  /*  export class GameLogic extends events.EventDispatcher {
        private gameData: GameData;
        private _gameDataUrl: string;

        public get gameDataUrl(): string {
            return this._gameDataUrl;
        }

        public set gameDataUrl(url: string) {
            if (this._gameDataUrl == url) return;
            var request = new XMLHttpRequest();
            var that = this;
            request.open("get", url, true);
            request.responseType = "json";
            request.onload = function (event) {
                that.gameData = request.response;
                that.dispatchEvent(new events.RokkEvent("gameDataUpdated"));
            };
            request.send();
            this._gameDataUrl = url;
        }

        constructor() {
            super();
        }

    }*/