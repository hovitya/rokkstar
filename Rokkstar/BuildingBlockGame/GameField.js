﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", "./../lib/Base", "./../lib/ComponentBase", "./../lib/Shapes"], function(require, exports, __core__, __components__, __shapes__) {
    ///<reference path='../lib/Base.ts'/>
    ///<reference path='../lib/Events.ts'/>
    ///<reference path='../lib/Collections.ts'/>
    ///<reference path='../lib/ComponentBase.ts'/>
    ///<reference path='../lib/Shapes.ts'/>
    ///<reference path='lib/Box2d.d.ts'/>
    var core = __core__;
    var components = __components__;
    
    var shapes = __shapes__;
    var GameField = (function (_super) {
        __extends(GameField, _super);
        function GameField(node) {
            _super.call(this, node);
            this.worldCreated = false;
            this.simulation = true;
            this._mouseActive = true;
            this.worker = new Worker("BuildingBlockGame/PhysicsWorker.js");
            var that = this;

            this.worker.addEventListener("message", function (event) {
                for (var i in event.data) {
                    var item = that.getChildAt(i);
                    item.left = 0;
                    item.top = 0;
                    var rotate = new components.RotateTransform();
                    rotate.angle = core.MathTools.toDegree(event.data[i].angle);
                    var translate = new components.TranslateTransform();
                    translate.x = event.data[i].position.x - item.width / 2;
                    translate.y = event.data[i].position.y - item.height / 2;
                    item.renderTransformations = [
                        translate,
                        rotate
                    ];
                }
            });

            this.addEventListener("mousemove", function (event) {
                var localPoint = that.globalToLocal(new core.Point(event.originalEvent.clientX, event.originalEvent.clientY));
                that.mouseX = localPoint.x;
                that.mouseY = localPoint.y;

                //console.log(that.mouseX,that.mouseY);
                that.worker.postMessage({
                    action: "mouseMove",
                    x: that.mouseX,
                    y: that.mouseY
                });
            });

            window.addEventListener("mousemove", function (event) {
                var p = that.globalToLocal(new core.Point(event.clientX, event.clientY));
                if (p.x < 0 || p.y < 0 || p.x > that.measuredWidth || p.y > that.measuredHeight) {
                    that.worker.postMessage({
                        action: "dismissMouseJoint"
                    });
                }
            });

            this.addEventListener("mouseup", function (event) {
                that.worker.postMessage({
                    action: "dismissMouseJoint"
                });
            });
        }
        Object.defineProperty(GameField.prototype, "mouseActive", {
            get: function () {
                return this._mouseActive;
            },
            set: function (value) {
                value = core.PrimitiveTypeTools.ensureBool(value);
                if (value === this._mouseActive)
                    return;
                var old = this._mouseActive;
                this._mouseActive = value;
                if (!value) {
                    this.worker.postMessage({
                        action: "dismissMouseJoint"
                    });
                }
                this.notifyPropertyChanged("mouseActive", old, value);
            },
            enumerable: true,
            configurable: true
        });

        GameField.prototype.earthquake = function () {
            this.worker.postMessage({
                action: "earthquake"
            });
        };

        GameField.prototype.updateSize = function (newWidth, newHeight) {
            _super.prototype.updateSize.call(this, newWidth, newHeight);
            if (!this.worldCreated) {
                this.worker.postMessage({
                    action: 'createWorld',
                    width: newWidth,
                    height: newHeight
                });
                this.worldCreated = false;

                var that = this;
                this.addEventListener(components.DisplayContainerEvent.ITEM_ADDED, function (event) {
                    (item).elementId = event.index;
                    that.worker.postMessage({
                        action: "registerBody",
                        id: event.index,
                        x: event.item.left + event.item.width / 2,
                        y: event.item.top + event.item.height / 2,
                        width: event.item.width,
                        height: event.item.height,
                        isCircle: (item).isCircle(),
                        edges: (item).getEdges()
                    });
                });

                for (var i = 0; i < this.childrenNum; i++) {
                    var item = this.getChildAt(i);
                    (item).elementId = i;
                    this.worker.postMessage({
                        action: "registerBody",
                        id: i,
                        x: item.left + item.width / 2,
                        y: item.top + item.height / 2,
                        width: item.width,
                        height: item.height,
                        isCircle: (item).isCircle(),
                        edges: (item).getEdges()
                    });
                    (function (item) {
                        item.addEventListener("mousedown", function (event) {
                            if (!that.mouseActive)
                                return;
                            that.worker.postMessage({
                                action: "createMouseJoint",
                                id: (item).elementId,
                                x: that.mouseX,
                                y: that.mouseY
                            });
                        });
                        item.addEventListener("mouseup", function (event) {
                            that.worker.postMessage({
                                action: "dismissMouseJoint"
                            });
                        });
                    })(item);
                }

                //Start rendering
                var that = this;
                this.interval = setInterval(function () {
                    if (that.simulation) {
                        that.worker.postMessage({
                            action: 'step',
                            dt: 1 / 60
                        });
                    }
                }, 1000 / 60);
            }
        };
        return GameField;
    })(components.Group);
    exports.GameField = GameField;

    var DropshadowRenderer = (function (_super) {
        __extends(DropshadowRenderer, _super);
        function DropshadowRenderer() {
            _super.apply(this, arguments);
            this.dx = 1;
            this.dy = 3;
        }

        Object.defineProperty(DropshadowRenderer.prototype, "gameField", {
            get: function () {
                return this._gameField;
            },
            set: function (gf) {
                if (this._gameField === gf)
                    return;
                var old = this._gameField;
                this._gameField = gf;

                //Remove old shadows
                var i = this.childrenNum;
                for (var j = i - 1; j >= 0; j--) {
                    this.removeChildAt(j);
                }

                for (var i = 0; i < this.gameField.childrenNum; i++) {
                    var child = this.gameField.getChildAt(i);
                    var clone = child.clone();
                    clone.background = components.BackgroundParser.parseBackgroundString("black");
                    clone.left = 0;
                    clone.top = 0;
                    clone.alpha = 0.5;
                    this.addChild(clone);
                    child.attach(this);
                }
                this.notifyPropertyChanged("gameField", old, gf);
            },
            enumerable: true,
            configurable: true
        });

        DropshadowRenderer.prototype.update = function (item, property, oldVal, newVal) {
            if (item instanceof GameFieldElement && property === "renderTransformations" && this.gameField.getChildIndex(item) !== -1) {
                var shadow = this.getChildAt(this.gameField.getChildIndex(item));
                var rotate = new components.RotateTransform();
                rotate.angle = (newVal[1]).angle;

                //rotate.x = (-1) * item.width / 2;
                //rotate.y = (-1) * item.height / 2;
                var translate = new components.TranslateTransform();
                translate.x = (newVal[0]).x + this.dx;
                translate.y = (newVal[0]).y + this.dy;
                var translate2 = new components.TranslateTransform();
                translate2.x = this.dx;
                translate2.y = this.dy;
                shadow.renderTransformations = [
                    translate,
                    rotate
                ];
            }
        };
        return DropshadowRenderer;
    })(components.Group);
    exports.DropshadowRenderer = DropshadowRenderer;

    var GameFieldElement = (function (_super) {
        __extends(GameFieldElement, _super);
        function GameFieldElement(node) {
            _super.call(this, node);
            this.origin = null;
            var that = this;
        }
        GameFieldElement.prototype.isCircle = function () {
            return false;
        };
        GameFieldElement.prototype.getEdges = function () {
            return null;
        };

        GameFieldElement.prototype.clone = function () {
            var clone = new GameFieldElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        };
        return GameFieldElement;
    })(shapes.Rectangle);
    exports.GameFieldElement = GameFieldElement;

    var RectangleElement = (function (_super) {
        __extends(RectangleElement, _super);
        function RectangleElement() {
            _super.apply(this, arguments);
        }
        RectangleElement.prototype.clone = function () {
            var clone = new RectangleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        };
        return RectangleElement;
    })(GameFieldElement);
    exports.RectangleElement = RectangleElement;

    var TriangleElement = (function (_super) {
        __extends(TriangleElement, _super);
        function TriangleElement() {
            _super.apply(this, arguments);
        }
        TriangleElement.prototype.getEdges = function () {
            var leftCorner = new core.Point(0, this.height);
            var rightCorner = new core.Point(this.width, this.height);
            var topCorner = new core.Point(this.width / 2.0, 0);
            return [
                { start: leftCorner.toJSON(), end: topCorner.toJSON() },
                { start: topCorner.toJSON(), end: rightCorner.toJSON() },
                { start: rightCorner.toJSON(), end: leftCorner.toJSON() }
            ];
        };
        TriangleElement.prototype.draw = function (w, h, canvas) {
            var edges = this.getEdges();
            var graphics = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            graphics.beginPath();
            graphics.moveTo(edges[0].start.x, edges[0].start.y);
            for (var i in edges) {
                graphics.lineTo(edges[i].end.x, edges[i].end.y);
            }
            graphics.closePath();
            graphics.fill();
        };
        TriangleElement.prototype.clone = function () {
            var clone = new TriangleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        };
        return TriangleElement;
    })(GameFieldElement);
    exports.TriangleElement = TriangleElement;

    var CircleElement = (function (_super) {
        __extends(CircleElement, _super);
        function CircleElement() {
            _super.apply(this, arguments);
        }
        CircleElement.prototype.isCircle = function () {
            return true;
        };
        CircleElement.prototype.draw = function (w, h, canvas) {
            var x = 0;
            var y = 0;

            var graphics = canvas.getContext("2d");
            graphics.clearRect(0, 0, w, h);
            if (this.background) {
                this.background.setFillColor(graphics, 0, 0, w, h);
            }
            var kappa = 0.5522848, ox = (w / 2) * kappa, oy = (h / 2) * kappa, xe = x + w, ye = y + h, xm = x + w / 2, ym = y + h / 2;

            graphics.beginPath();
            graphics.moveTo(x, ym);
            graphics.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
            graphics.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
            graphics.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
            graphics.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
            graphics.closePath();
            graphics.fill();
        };

        CircleElement.prototype.clone = function () {
            var clone = new CircleElement();
            clone.width = this.width;
            clone.height = this.height;
            clone.origin = this;
            return clone;
        };
        return CircleElement;
    })(GameFieldElement);
    exports.CircleElement = CircleElement;

    var ElementDescriptor = (function () {
        function ElementDescriptor() {
        }
        return ElementDescriptor;
    })();

    var Building = (function () {
        function Building() {
        }
        return Building;
    })();

    var GameData = (function () {
        function GameData() {
        }
        return GameData;
    })();
});
