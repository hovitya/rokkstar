﻿define(["require", "exports", "./../lib/ComponentBase", "./../lib/Data", "./GameField", "./../lib/Parser", "./../lib/Shapes"], function(require, exports, __components__, __data__, __imagix__, __parser__, __shapes__) {
    var components = __components__;
    var data = __data__;
    var imagix = __imagix__;
    var parser = __parser__;
    var shapes = __shapes__;
    function bootstrap() {
        //document.body.onload = function () {
        parser.run({ components: components, data: data, imagix: imagix, shapes: shapes, parser: parser });
        //}
    }
    exports.bootstrap = bootstrap;
});
