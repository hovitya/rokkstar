﻿///<reference path='PhysicsEngine.ts'/>
///<reference path='lib/Box2d.d.ts'/>

importScripts("lib/Box2dWeb-2.1.a.3.js", "PhysicsEngine.js");

var physicsEngine = new imagix.PhysicsEngine();
var worldCreated: boolean = false;
var needToRegister: any[] = [];
onmessage = function (event) {
    switch (event.data.action) {
        case "createWorld":
            physicsEngine.createWorld(parseInt(event.data.width), parseInt(event.data.height));
            worldCreated = true;
            for (var i in needToRegister) {
                var event = needToRegister[i];
                physicsEngine.registerBody(event.data.id, parseInt(event.data.x), parseInt(event.data.y), parseInt(event.data.width), parseInt(event.data.height), event.data.isCircle, event.data.edges);
            }
            break;
        case "registerBody":
            if (worldCreated) {
                physicsEngine.registerBody(event.data.id, parseInt(event.data.x), parseInt(event.data.y), parseInt(event.data.width), parseInt(event.data.height), event.data.isCircle, event.data.edges);
            } else {
                needToRegister.push(event);
            }
            break;
        case "step":
            var stepResult = physicsEngine.step(event.data.dt);
            postMessage(stepResult,"");
            break;
        case "createMouseJoint":
            physicsEngine.createMouseJoint(event.data.id, event.data.x, event.data.y);
            break;
        case "mouseMove":
            physicsEngine.mouseMove(parseInt(event.data.x),parseInt(event.data.y));
            break;
        case "dismissMouseJoint":
            physicsEngine.dismissMouseJoint();
            break;
        case "earthquake":
            physicsEngine.earthquake();
            break;

    }
};
