﻿import components = require("./../lib/ComponentBase");
import data = require("./../lib/Data");
import imagix = require("./GameField");
import parser = require("./../lib/Parser");
import shapes = require("./../lib/Shapes");
export function bootstrap() {
    //document.body.onload = function () {
        parser.run({ components: components, data: data, imagix: imagix, shapes: shapes, parser: parser });
    //}
}
  