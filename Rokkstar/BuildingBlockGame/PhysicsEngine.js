﻿///<reference path='lib/Box2d.d.ts'/>
var imagix;
(function (imagix) {
    var PhysicsEngine = (function () {
        function PhysicsEngine() {
            this.bodies = [];
        }
        PhysicsEngine.prototype.createWorld = function (width, height) {
            //Gravity
            var gravity = new Box2D.Common.Math.b2Vec2(0.0, 10);

            //World
            this.world = new Box2D.Dynamics.b2World(gravity, true);

            //Create ground
            var groundBodyDef = new Box2D.Dynamics.b2BodyDef();
            groundBodyDef.position.Set(0, height / 100 + 1);
            var groundBody = this.world.CreateBody(groundBodyDef);
            var groundBox = new Box2D.Collision.Shapes.b2PolygonShape();
            groundBox.SetAsBox(width / 100, 1);
            groundBody.CreateFixture2(groundBox, 0.0);

            //Create left wall
            var lwallBodyDef = new Box2D.Dynamics.b2BodyDef();
            lwallBodyDef.position.Set(-1, 0);
            var lwallBody = this.world.CreateBody(lwallBodyDef);
            var lwallBox = new Box2D.Collision.Shapes.b2PolygonShape();
            lwallBox.SetAsBox(1, height / 100);
            lwallBody.CreateFixture2(lwallBox, 0.0);

            //Create right wall
            var rwallBodyDef = new Box2D.Dynamics.b2BodyDef();
            rwallBodyDef.position.Set(width / 100 + 1, 0);
            var rwallBody = this.world.CreateBody(rwallBodyDef);
            var rwallBox = new Box2D.Collision.Shapes.b2PolygonShape();
            rwallBox.SetAsBox(1, height / 100);
            rwallBody.CreateFixture2(rwallBox, 0.0);

            //Create top
            var topBodyDef = new Box2D.Dynamics.b2BodyDef();
            topBodyDef.position.Set(0, -1);
            var topBody = this.world.CreateBody(topBodyDef);
            var topBox = new Box2D.Collision.Shapes.b2PolygonShape();
            topBox.SetAsBox(width / 100, 1);
            topBody.CreateFixture2(topBox, 0.0);
        };

        PhysicsEngine.prototype.randomFromInterval = function (from, to) {
            return Math.floor(Math.random() * (to - from + 1) + from);
        };

        PhysicsEngine.prototype.earthquake = function () {
            for (var i in this.bodies) {
                this.bodies[i].ApplyImpulse(new Box2D.Common.Math.b2Vec2(this.randomFromInterval(-50, 50), 0), this.bodies[i].GetPosition());
            }
        };

        PhysicsEngine.prototype.registerBody = function (id, x, y, w, h, isCircle, edges) {
            if (typeof isCircle === "undefined") { isCircle = false; }
            if (typeof edges === "undefined") { edges = null; }
            var bodyDef = new Box2D.Dynamics.b2BodyDef();
            bodyDef.type = Box2D.Dynamics.b2Body.b2_dynamicBody;
            bodyDef.position.Set(x / 100.0, y / 100.0);

            var body = this.world.CreateBody(bodyDef);

            if (isCircle) {
                var bodyBox = new Box2D.Collision.Shapes.b2CircleShape(w / 200.0);
            } else if (edges !== null) {
                var bodyBox = new Box2D.Collision.Shapes.b2PolygonShape();
                var vertices = [];

                //vertices.push(new Box2D.Common.Math.b2Vec2(edges[0].start.x / 100.0, edges[0].start.y / 100.0));
                var verticeCount = 0;
                for (var i in edges) {
                    verticeCount++;
                    vertices.push(new Box2D.Common.Math.b2Vec2(edges[i].start.x / 100.0 - w / 200.0, edges[i].start.y / 100.0 - h / 200.0));
                }
                (bodyBox).SetAsArray(vertices, verticeCount);
            } else {
                var bodyBox = new Box2D.Collision.Shapes.b2PolygonShape();
                (bodyBox).SetAsBox(w / 200.0, h / 200.0);
            }

            var fixtureDef = new Box2D.Dynamics.b2FixtureDef();
            fixtureDef.shape = bodyBox;
            fixtureDef.density = 10.0;
            fixtureDef.friction = 0.3;
            fixtureDef.restitution = 0.2;
            body.CreateFixture(fixtureDef);
            if (isCircle) {
                body.SetBullet(true);
            }

            body.ResetMassData();
            this.bodies[id] = body;
        };

        PhysicsEngine.prototype.step = function (time) {
            this.world.Step(time, 3, 3);
            var ret = {};
            for (var i in this.bodies) {
                var position = this.bodies[i].GetPosition();
                ret[i] = {
                    position: {
                        x: Math.round(position.x * 100),
                        y: Math.round(position.y * 100)
                    },
                    angle: this.bodies[i].GetAngle()
                };
            }
            return ret;
        };

        PhysicsEngine.prototype.createMouseJoint = function (id, x, y) {
            if (this.mouseJoint) {
                this.world.DestroyJoint(this.mouseJoint);
                this.mouseJoint = undefined;
            }
            var mdDef = new Box2D.Dynamics.Joints.b2MouseJointDef();
            mdDef.bodyA = this.world.GetGroundBody();
            mdDef.bodyB = this.bodies[id];
            mdDef.target.Set(x / 100.0, y / 100.0);
            mdDef.maxForce = 300.0 * this.bodies[id].GetMass();
            this.mouseJoint = this.world.CreateJoint(mdDef);
        };

        PhysicsEngine.prototype.dismissMouseJoint = function () {
            if (this.mouseJoint) {
                this.world.DestroyJoint(this.mouseJoint);
                this.mouseJoint = undefined;
            }
        };

        PhysicsEngine.prototype.mouseMove = function (x, y) {
            if (this.mouseJoint) {
                var p2 = new Box2D.Common.Math.b2Vec2(x / 100.0, y / 100.0);
                this.mouseJoint.SetTarget(p2);
            }
        };
        return PhysicsEngine;
    })();
    imagix.PhysicsEngine = PhysicsEngine;
})(imagix || (imagix = {}));
