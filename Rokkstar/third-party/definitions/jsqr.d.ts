declare class QRCode {
    typeNumber: number;
    errorCorrectLevel: number;
    modules: any[];
    moduleCount: number;
    constructor(typeNumber: number, errorCorrectLevel: number);
    isDark(row: number, col: number): boolean;
    addData(text: string);
    make();
}