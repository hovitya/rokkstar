﻿declare module TWEEN {
    class Tween {
        to(properties, duration?: number): Tween;
        start(time?: number): Tween;
        stop(): Tween;
        delay(amount: number): Tween;
        repeat(times: number): Tween;
        yoyo(yoyo: boolean): Tween;
        easing(easing: (k: number) => number): Tween;
        interpolation(interpolation: (end: number, value: number) => number): Tween;
        onStart(callback: () => void): Tween;
        onUpdate(callback: () => void): Tween;
        onComplete(callback: () => void): Tween;
        constructor(object: any);
    }

    var Easing: Map<string, Map<String, (k: number) => number>>;

    var Interpolation: Map<string, (v: number, k: number) => number>;
}

