import components = require("./lib/ComponentBase");
import data = require("./lib/Data");
import parser = require("./lib/Parser");
import shapes = require("./lib/Shapes");
import anim = require("./lib/Animation");
export function bootstrap() {
    //document.body.onload = function () {
    parser.run({ components: components, data: data, shapes: shapes, parser: parser, anim: anim  });
    //}
}
