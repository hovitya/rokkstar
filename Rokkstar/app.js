define(["require", "exports", "./lib/ComponentBase", "./lib/Data", "./lib/Parser", "./lib/Shapes", "./lib/Animation"], function(require, exports, __components__, __data__, __parser__, __shapes__, __anim__) {
    var components = __components__;
    var data = __data__;
    var parser = __parser__;
    var shapes = __shapes__;
    var anim = __anim__;
    function bootstrap() {
        //document.body.onload = function () {
        parser.run({ components: components, data: data, shapes: shapes, parser: parser, anim: anim });
        //}
    }
    exports.bootstrap = bootstrap;
});
//# sourceMappingURL=app.js.map
